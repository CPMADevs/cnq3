# Building CNQ3

## Toolchains

There are 3 supported build toolchains:

- Visual C++ on Windows x64
- GCC or Clang on Linux x64
- Clang or GCC on FreeBSD x64

## Directories

| Directory | Contains                                                      |
|:----------|:--------------------------------------------------------------|
| makefiles | premake script and pre-generated Visual C++ and GNU makefiles |
| .build    | intermediate build files                                      |
| .bin      | final executables (and symbol database files on Windows)      |

## Building on Windows with Visual C++ 2019 or later

**Requirements**

- The %QUAKE3DIR% environment variable must be defined to the absolute path to copy the .exe and .pdb files to
- nasm.exe (Netwide Assembler) is in your path for building the client
- One of the following is true for building the client:
  - The %DXCPATH% environment variable contains the full path to dxc.exe
  - dxc.exe is already in your %PATH%

**Options**

- The %CPMADIR% environment variable must be defined to the name of the mod directory for launching through the debugger

**Build steps**

- Set the %QUAKE3DIR% environment variable
- Open makefiles\windows_vs2019\cnq3.sln or makefiles\windows_vs2022\cnq3.sln
- Build

**Notes**

- You don't need to set environment variables globally
- Instead, we recomment you set them for the Visual Studio process only

Here's an example batch script for opening the Visual Studio solution:
```batch
cd cnq3\makefiles\windows_vs2022
set QUAKE3DIR=C:\Programs\Q3\Dev
set CPMADIR=cpma_dev
set DXCPATH=C:\Programs\dxc\bin\x64\dxc.exe
start "" cnq3.sln
exit
```
With this set-up, you can press F5 and run the engine through the Visual C++ debugger on the right q3 install and right mod folder immediately.

## Building on Linux / FreeBSD with GCC / Clang

**Requirements**

- On FreeBSD, we link against libexecinfo for the backtrace functions. We thus require FreeBSD 10.0 or later.

**Options**

- The $(QUAKE3DIR) environment variable can define the absolute path to copy the executables to

**Build steps**

- Navigate to the root of the repository
- Run `make [config=debug|release] all|server` to build on Linux  
  For FreeBSD, use `gmake` instead of `make`

**Notes**

- To create the QUAKE3DIR variable in the build shell, you can use `export QUAKE3DIR=~/games/q3`
- To delete the variable from the build shell, you can use `unset QUAKE3DIR`
- On FreeBSD, Clang is now the default compiler
- For most Linux distros, GCC is still the default compiler
- To force building with Clang, you can use `CC=clang CXX=clang++` as make/gmake arguments
- To force building with GCC, you can use `CC=gcc CXX=g++` as make/gmake arguments
- You don't have to specify $(CPP), it's not used by any of the makefiles

## Environment variables

There are 3 environment variables used for compiling and debugging:

| Env. Var. | Meaning                 | Example                         |
|:----------|:------------------------|:--------------------------------|
| QUAKE3DIR | absolute directory path | C:\Programs\Q3                  |
| CPMADIR   | mod folder name         | cpma                            |
| DXCPATH   | full dxc.exe file path  | C:\Programs\dxc\bin\x64\dxc.exe |

| Env. Var. | Windows                  | Linux / FreeBSD       |
|:----------|:-------------------------|:----------------------|
| QUAKE3DIR | required for building    | optional for building |
| CPMADIR   | required for debugging   | unused                |
| DXCPATH   | optional for building(1) | unused                |

1) DXCPATH is optional if you already have dxc.exe in your PATH
