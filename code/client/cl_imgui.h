/*
===========================================================================
Copyright (C) 2022-2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// Dear ImGui and Im3d client integration and utility functions


#pragma once


#include "../imgui/imgui.h"
#include "../implot/implot.h"


// global requires shift key down, local requires shift up
struct ShortcutOptions
{
	enum Flags
	{
		None = 0,
		Local = 0,
		Global = 1 << 0
	};
};

bool BeginTable(const char* name, int count);
void TableHeader(int count, ...);
void TableRow(int count, ...);
void TableRow2(const char* item0, bool item1);
void TableRow2(const char* item0, int item1);
void TableRow2(const char* item0, float item1, const char* format = "%g");
bool IsShortcutPressed(ImGuiKey key, ShortcutOptions::Flags flags);
void ToggleBooleanWithShortcut(bool& value, ImGuiKey key, ShortcutOptions::Flags flags);
void RadioButton(int* value, float titleWidth, const char* title, int count, ...); // count (const char* name, int value) pairs
void SaveFileDialog_Open(const char* folder, const char* extension);
bool SaveFileDialog_Do(); // true when the user clicked the save button
const char* SaveFileDialog_GetPath();
void OpenFileDialog_Open(const char* folder, const char* extension);
bool OpenFileDialog_Do(); // true when the user clicked the open button
const char* OpenFileDialog_GetPath();
void SaveFolderDialog_Open(const char* folder);
bool SaveFolderDialog_Do(); // true when the user clicked the save button
const char* SaveFolderDialog_GetPath();
void OpenFolderDialog_Open(const char* folder);
bool OpenFolderDialog_Do(); // true when the user clicked the open button
const char* OpenFolderDialog_GetPath();

#define MAIN_MENU_LIST(M) \
	M(Tools, "Tools") \
	M(Info, "Information") \
	M(Perf, "Performance") \
	M(Im3D, "Im3D")

#define M(Enum, Desc) Enum,
struct GUI_MainMenu
{
	enum Id
	{
		MAIN_MENU_LIST(M)
		Count
	};
};
#undef M

typedef void (*GUI_MainMenuCallback)();
void GUI_AddMainMenuItem(GUI_MainMenu::Id menu, const char* item, const char* shortcut, bool* selected, bool enabled = true);
void GUI_DrawMainMenu();
