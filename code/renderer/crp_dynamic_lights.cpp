/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// Cinematic Rendering Pipeline - direct lighting from dynamic lights


#include "crp_local.h"
#include "shaders/crp/scene_view.h.hlsli"
#include "compshaders/crp/fullscreen.h"
#include "compshaders/crp/dl_draw.h"
#include "compshaders/crp/dl_denoising.h"


#pragma pack(push, 4)

struct DynamicLightsRC
{
	DynamicLight light;
	uint32_t blueNoiseTextureIndex;
};


struct DenoiseRC
{
	vec3_t lightPosition;
	uint32_t textureIndex;
	uint32_t vshadowValid;
	uint32_t vshadowTextureIndex;
	uint32_t vshadowSamplerIndex;
	float vshadowWorldScale;
};

#pragma pack(pop)


void DynamicLights::Init()
{
	if(!rhiInfo.hasInlineRaytracing)
	{
		return;
	}

	{
		GraphicsPipelineDesc desc("Dynamic Lights");
		MakeFullScreenPipeline(desc, ShaderByteCode(g_dl_draw_ps));
		desc.AddRenderTarget(0, crp.renderTargetFormat);
		pipeline = CreateGraphicsPipeline(desc);
	}

	{
		GraphicsPipelineDesc desc("Dynamic Lights Denoising");
		MakeFullScreenPipeline(desc, ShaderByteCode(g_dl_denoising_ps));
		desc.AddRenderTarget(GLS_SRCBLEND_ONE | GLS_DSTBLEND_ONE, crp.renderTargetFormat);
		denoisingPipeline = CreateGraphicsPipeline(desc);
	}
}

void DynamicLights::DrawBegin()
{
	CmdBeginBarrier();
	CmdTextureBarrier(crp.lightTexture, ResourceStates::RenderTargetBit);
	CmdEndBarrier();
	CmdClearColorTarget(crp.lightTexture, colorBlack);
}

void DynamicLights::DrawPointLight(const dlight_t& light)
{
	if(r_dynamiclight->integer == 0 ||
		(backEnd.refdef.rdflags & RDF_NOWORLDMODEL) != 0 ||
		!IsViewportFullscreen(backEnd.viewParms) ||
		!crp.raytracing.CanRaytrace() ||
		backEnd.refdef.num_dlights <= 0)
	{
		return;
	}

	srp.renderMode = RenderMode::None;

	{
		SCOPED_DEBUG_LABEL("DL Draw", 1.0f, 1.0f, 1.0f);

		CmdSetViewportAndScissor(0, 0, glConfig.vidWidth, glConfig.vidHeight);

		CmdBeginBarrier();
		CmdTextureBarrier(crp.shadingPositionTexture, ResourceStates::PixelShaderAccessBit);
		CmdTextureBarrier(crp.normalTexture, ResourceStates::PixelShaderAccessBit);
		CmdTextureBarrier(crp.sunlightTexture, ResourceStates::RenderTargetBit);
		CmdEndBarrier();

		DynamicLightsRC rc = {};
		VectorCopy(light.origin, rc.light.position);
		VectorCopy(light.color, rc.light.color);
		rc.light.radius = light.radius;
		rc.blueNoiseTextureIndex = GetTextureIndexSRV(crp.blueNoise2D);
		CmdBindRenderTargets(1, &crp.sunlightTexture, NULL);
		CmdBindPipeline(pipeline);
		CmdSetGraphicsRootConstants(0, sizeof(rc), &rc);
		CmdDraw(3, 0);
	}

	{
		SCOPED_DEBUG_LABEL("DL Denoise", 1.0f, 1.0f, 1.0f);

		CmdSetViewportAndScissor(0, 0, glConfig.vidWidth, glConfig.vidHeight);

		CmdBeginBarrier();
		CmdTextureBarrier(crp.shadingPositionTexture, ResourceStates::PixelShaderAccessBit);
		CmdTextureBarrier(crp.sunlightTexture, ResourceStates::PixelShaderAccessBit);
		CmdTextureBarrier(crp.volumetricLight.pointShadowTexture, ResourceStates::PixelShaderAccessBit);
		CmdTextureBarrier(crp.lightTexture, ResourceStates::RenderTargetBit);
		CmdEndBarrier();

		DenoiseRC rc = {};
		VectorCopy(light.origin, rc.lightPosition);
		rc.textureIndex = GetTextureIndexSRV(crp.sunlightTexture);
		rc.vshadowValid = crp_volLight->integer != 0 ? 1 : 0;
		rc.vshadowTextureIndex = GetTextureIndexSRV(crp.volumetricLight.pointShadowTexture);
		rc.vshadowSamplerIndex = GetSamplerIndex(TW_CLAMP_TO_EDGE, TextureFilter::Linear);
		rc.vshadowWorldScale = crp.volumetricLight.pointShadowVolumeScale;
		CmdBindRenderTargets(1, &crp.lightTexture, NULL);
		CmdBindPipeline(denoisingPipeline);
		CmdSetGraphicsRootConstants(0, sizeof(rc), &rc);
		CmdDraw(3, 0);
	}
}

bool DynamicLights::WantRTASUpdate(const trRefdef_t& scene)
{
	return scene.num_dlights > 0;
}
