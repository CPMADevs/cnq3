/*
===========================================================================
Copyright (C) 2023-2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// Main renderer GUI tools


#include "tr_local.h"
#include "../client/cl_imgui.h"
#include "../imgui/imgui_internal.h" // to get the clip rect of a InputTextMultiline


qbool CL_IMGUI_IsCustomFontLoaded(const char** customFontName);


#define IMAGE_WINDOW_NAME  "Image Details"
#define SHADER_WINDOW_NAME "Shader Details"
#define EDIT_WINDOW_NAME   "Shader Editor"

#define SHADER_CODE_BUFFER_SIZE (16 * 1024)


struct ImageWindow
{
	const image_t* image;
	bool active;
	int mip;
};

struct ShaderWindow
{
	char formattedCode[SHADER_CODE_BUFFER_SIZE];
	const image_t* displayImages[MAX_IMAGE_ANIMATIONS * MAX_SHADER_STAGES + 6]; // +6 for sky box
	shader_t* shader;
	int displayImageCount;
	bool active;
};

struct ShaderReplacement
{
	shader_t original;
	int index;
};

struct ShaderReplacements
{
	ShaderReplacement shaders[16];
	int count;
};

struct ImageReplacement
{
	image_t original;
	int index;
};

struct ImageReplacements
{
	ImageReplacement images[16];
	int count;
};

struct ShaderEditWindow
{
	char fileContent[SHADER_CODE_BUFFER_SIZE + MAX_QPATH * 2]; // code + header line
	char code[SHADER_CODE_BUFFER_SIZE];
	char originalCode[SHADER_CODE_BUFFER_SIZE];
	char chars[SHADER_CODE_BUFFER_SIZE];
	char currentLine[1024];
	char toolTip[1024];
	char shaderFilePath[MAX_QPATH];
	vec2_t cursorCoords;
	shader_t originalShader;
	shader_t* shader;
	int charCount;
	int currentLineCursor;
	int currentLineX;
	int cursor;
	int delayedShaderOpen = -1;
	bool active;
	bool validCursorCoords;
};

static ImageWindow imageWindow;
static ShaderWindow shaderWindow;
static ShaderReplacements shaderReplacements;
static ImageReplacements imageReplacements;
static ShaderEditWindow shaderEditWindow;
static char cvarFilter[256];

static const char* mipNames[16] =
{
	"Mip 0",
	"Mip 1",
	"Mip 2",
	"Mip 3",
	"Mip 4",
	"Mip 5",
	"Mip 6",
	"Mip 7",
	"Mip 8",
	"Mip 9",
	"Mip 10",
	"Mip 11",
	"Mip 12",
	"Mip 13",
	"Mip 14",
	"Mip 15"
};

struct ImageFlag
{
	int flag;
	const char* description;
};

static const ImageFlag imageFlags[] =
{
	{ IMG_NOPICMIP, "'nopicmip'" },
	{ IMG_NOMIPMAP, "'nomipmap'" },
	{ IMG_NOIMANIP, "'noimanip'" },
	{ IMG_LMATLAS, "int. LM" },
	{ IMG_EXTLMATLAS, "ext. LM" },
	{ IMG_NOAF, "no AF" }
};

// RGB triplets for ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789
static const float cpmaColorCodes[] =
{
	1.000000f, 0.000000f, 0.000000f,
	1.000000f, 0.267949f, 0.000000f,
	1.000000f, 0.500000f, 0.000000f,
	1.000000f, 0.732051f, 0.000000f,
	1.000000f, 1.000000f, 0.000000f,
	0.732051f, 1.000000f, 0.000000f,
	0.500000f, 1.000000f, 0.000000f,
	0.267949f, 1.000000f, 0.000000f,
	0.000000f, 1.000000f, 0.000000f,
	0.000000f, 1.000000f, 0.267949f,
	0.000000f, 1.000000f, 0.500000f,
	0.000000f, 1.000000f, 0.732051f,
	0.000000f, 1.000000f, 1.000000f,
	0.000000f, 0.732051f, 1.000000f,
	0.000000f, 0.500000f, 1.000000f,
	0.000000f, 0.267949f, 1.000000f,
	0.000000f, 0.000000f, 1.000000f,
	0.267949f, 0.000000f, 1.000000f,
	0.500000f, 0.000000f, 1.000000f,
	0.732051f, 0.000000f, 1.000000f,
	1.000000f, 0.000000f, 1.000000f,
	1.000000f, 0.000000f, 0.732051f,
	1.000000f, 0.000000f, 0.500000f,
	1.000000f, 0.000000f, 0.267949f,
	0.250000f, 0.250000f, 0.250000f,
	0.500000f, 0.500000f, 0.500000f,
	0.000000f, 0.000000f, 0.000000f,
	1.000000f, 0.000000f, 0.000000f,
	0.000000f, 1.000000f, 0.000000f,
	1.000000f, 1.000000f, 0.000000f,
	0.200000f, 0.200000f, 1.000000f,
	0.000000f, 1.000000f, 1.000000f,
	1.000000f, 0.000000f, 1.000000f,
	1.000000f, 1.000000f, 1.000000f,
	0.750000f, 0.750000f, 0.750000f,
	0.600000f, 0.600000f, 1.000000f
};


#if 0
static void SelectableText(const char* text)
{
	char buffer[256];
	Q_strncpyz(buffer, text, sizeof(buffer));

	ImGui::PushID(text);
	ImGui::PushItemWidth(ImGui::GetWindowSize().x);
	ImGui::InputText("", buffer, sizeof(buffer), ImGuiInputTextFlags_ReadOnly);
	ImGui::PopItemWidth();
	ImGui::PopID();
}
#endif

static bool IsNullOrEmpty(const char* string)
{
	return string == NULL || string[0] == '\0';
}

static bool IsNonEmpty(const char* string)
{
	return string != NULL && string[0] != '\0';
}

static void Checkbox(const char* label, bool value)
{
	ImGui::Checkbox(label, &value);
}

static void SetTooltipIfValid(const char* text)
{
	if(IsNonEmpty(text) && ImGui::IsItemHovered(ImGuiHoveredFlags_DelayNormal))
	{
		ImGui::SetTooltip(text);
	}
}

static ImVec4 GetColorFromCPMACode(char colorCode)
{
	int colorIndex = 0;
	if(colorCode >= 'A' && colorCode <= 'Z')
	{
		colorIndex = colorCode - 'A';
	}
	else if(colorCode >= 'a' && colorCode <= 'z')
	{
		colorIndex = colorCode - 'a';
	}
	else if(colorCode >= '0' && colorCode <= '9')
	{
		colorIndex = 26 + (colorCode - '0');
	}

	const float* colorPointer = cpmaColorCodes + colorIndex * 3;
	const ImVec4 color(colorPointer[0], colorPointer[1], colorPointer[2], 1.0f);

	return color;
}

static bool CPMAColorCodeButton(const char* title, const char* id, char& currentColor)
{
	const char* const popupId = va("##color_popup_%s", id);
	const ImGuiColorEditFlags previewFlags = ImGuiColorEditFlags_DisplayRGB | ImGuiColorEditFlags_NoAlpha;
	bool clicked = false;

	ImGui::Text(va("%c", currentColor));
	ImGui::SameLine();

	if(ImGui::ColorButton(va("##%s", id), GetColorFromCPMACode(currentColor), previewFlags))
	{
		ImGui::OpenPopup(popupId);
	}

	if(ImGui::BeginPopup(popupId))
	{
		if(title != NULL && title[0] != '\0')
		{
			ImGui::Text(title);
		}

		const char* rows[] =
		{
			"ABCDEF",
			"GHIJKL",
			"MNOPQR",
			"STUVWX",
			"YZ0123",
			"456789"
		};
		for(int y = 0; y < ARRAY_LEN(rows); ++y)
		{
			const char* row = rows[y];
			for(int x = 0; row[x] != '\0'; ++x)
			{
				if(x > 0)
				{
					ImGui::SameLine();
				}

				const char colorCode = row[x];
				const ImGuiColorEditFlags buttonFlags = ImGuiColorEditFlags_InputRGB | ImGuiColorEditFlags_NoAlpha;
				const ImVec4 color = GetColorFromCPMACode(colorCode);
				if(ImGui::ColorButton(va("%c", colorCode), color, buttonFlags))
				{
					currentColor = colorCode;
					clicked = true;
				}
			}
		}

		ImGui::EndPopup();
	}

	return clicked;
}

static const char* RemoveColorCodes(const char* text)
{
	static char buffer[1024];
	Q_strncpyz(buffer, text, sizeof(buffer));

	char* d = buffer;
	char* s = buffer;
	int c;
	while((c = *s) != 0)
	{
		if(Q_IsColorString(s))
		{
			s++;
		}
		else
		{
			*d++ = c;
		}
		s++;
	}
	*d = '\0';

	return buffer;
}

static void FormatShaderCode(char* dest, int destSize, const shader_t* shader)
{
#define Append(Text) Q_strcat(dest, destSize, Text)

	dest[0] = '\0';
	if(shader->text == NULL)
	{
		return;
	}

	const char* s = shader->text;
	int tabs = 0;
	for(;;)
	{
		const char c0 = s[0];
		const char c1 = s[1];
		if(c0 == '{')
		{
			tabs++;
			Append("{");
		}
		else if(c0 == '\n')
		{
			Append("\n");
			if(c1 == '}')
			{
				tabs--;
				if(tabs == 0)
				{
					Append("}\n");
					return;
				}
			}
			for(int i = 0; i < tabs; i++)
			{
				Append("    ");
			}
		}
		else
		{
			Append(va("%c", c0));
		}
		s++;
	}

#undef Append
}

static void TitleText(const char* text)
{
	ImGui::TextColored(ImVec4(1.0f, 0.25f, 0.0f, 1.0f), text);
}

static void OpenImageDetails(const image_t* image)
{
	ImGui::SetWindowFocus(IMAGE_WINDOW_NAME);

	imageWindow.active = true;
	imageWindow.image = image;
	imageWindow.mip = 0;
}

static void OpenShaderDetails(shader_t* shader)
{
	ImGui::SetWindowFocus(SHADER_WINDOW_NAME);

	shaderWindow.active = true;
	shaderWindow.shader = shader;
	FormatShaderCode(shaderWindow.formattedCode, sizeof(shaderWindow.formattedCode), shader);
}

static void CloseShaderEdit(bool force = false)
{
	ShaderEditWindow& window = shaderEditWindow;
	if(window.active || force)
	{
		R_SetShaderData(window.shader, &window.originalShader);
		window.active = false;
		window.delayedShaderOpen = -1;
	}
}

static void OpenShaderEdit(shader_t* shader)
{
	CloseShaderEdit();

	ImGui::SetWindowFocus(EDIT_WINDOW_NAME);

	ShaderEditWindow& window = shaderEditWindow;
	window.active = true;
	window.shader = shader;
	window.originalShader = *shader;
	window.shaderFilePath[0] = '\0';
	window.delayedShaderOpen = -1;
	FormatShaderCode(window.code, sizeof(window.code), shader);
	Q_strncpyz(window.originalCode, window.code, sizeof(window.originalCode));
	tr.shaderParseFailed = qfalse;
	tr.shaderParseNumWarnings = 0;
}

static bool IsEditingAllowed()
{
	return
		ri.Cvar_Get("sv_cheats", "0", 0)->integer != 0 || // cheats enabled
		tr.world == NULL || // menu only
		ri.Cvar_Get("sv_running", "0", 0)->integer != 0; // listen server
}

static void RemoveShaderReplacement(int shaderIndex)
{
	for(int i = 0; i < shaderReplacements.count; ++i)
	{
		const ShaderReplacement& repl = shaderReplacements.shaders[i];
		if(shaderIndex == repl.index && shaderIndex >= 0 && shaderIndex < tr.numShaders)
		{
			*tr.shaders[repl.index] = repl.original;
			if(i < shaderReplacements.count - 1)
			{
				shaderReplacements.shaders[i] = shaderReplacements.shaders[shaderReplacements.count - 1];
			}
			shaderReplacements.count--;
			break;
		}
	}
}

static void AddShaderReplacement(int shaderIndex)
{
	if(shaderReplacements.count >= ARRAY_LEN(shaderReplacements.shaders))
	{
		return;
	}

	if(shaderIndex < 0 || shaderIndex >= tr.numShaders)
	{
		return;
	}

	for(int i = 0; i < shaderReplacements.count; ++i)
	{
		if(shaderReplacements.shaders[i].index == shaderIndex)
		{
			return;
		}
	}

	ShaderEditWindow& edit = shaderEditWindow;
	if(edit.active && edit.shader->index == shaderIndex)
	{
		RemoveShaderReplacement(shaderIndex);
		R_SetShaderData(edit.shader, &edit.originalShader);
		edit.active = false;
	}

	ShaderReplacement& repl = shaderReplacements.shaders[shaderReplacements.count++];
	repl.index = shaderIndex;
	repl.original = *tr.shaders[shaderIndex];
	*tr.shaders[shaderIndex] = *tr.defaultShader;
	Q_strncpyz(tr.shaders[shaderIndex]->name, repl.original.name, sizeof(tr.shaders[shaderIndex]->name));
	tr.shaders[shaderIndex]->index = repl.original.index;
	tr.shaders[shaderIndex]->sortedIndex = repl.original.sortedIndex;
}

static bool IsReplacedShader(int shaderIndex)
{
	for(int i = 0; i < shaderReplacements.count; ++i)
	{
		const ShaderReplacement& repl = shaderReplacements.shaders[i];
		if(shaderIndex == repl.index)
		{
			return true;
		}
	}

	return false;
}

static void ClearShaderReplacements()
{
	for(int i = 0; i < shaderReplacements.count; ++i)
	{
		const ShaderReplacement& sr = shaderReplacements.shaders[i];
		if(sr.index >= 0 && sr.index < tr.numShaders)
		{
			*tr.shaders[sr.index] = sr.original;
		}
	}
	shaderReplacements.count = 0;
}

static void AddImageReplacement(int imageIndex)
{
	if(imageReplacements.count >= ARRAY_LEN(imageReplacements.images))
	{
		return;
	}

	if(imageIndex < 0 || imageIndex >= tr.numImages)
	{
		return;
	}

	for(int i = 0; i < imageReplacements.count; ++i)
	{
		if(imageReplacements.images[i].index == imageIndex)
		{
			return;
		}
	}

	ImageReplacement& repl = imageReplacements.images[imageReplacements.count++];
	repl.index = imageIndex;
	repl.original = *tr.images[imageIndex];
	*tr.images[imageIndex] = *tr.defaultImage;
	Q_strncpyz(tr.images[imageIndex]->name, repl.original.name, sizeof(tr.images[imageIndex]->name));
	tr.images[imageIndex]->index = repl.original.index;
}

static void RemoveImageReplacement(int imageIndex)
{
	for(int i = 0; i < imageReplacements.count; ++i)
	{
		const ImageReplacement& repl = imageReplacements.images[i];
		if(imageIndex == repl.index && imageIndex >= 0 && imageIndex < tr.numImages)
		{
			*tr.images[repl.index] = repl.original;
			if(i < imageReplacements.count - 1)
			{
				imageReplacements.images[i] = imageReplacements.images[imageReplacements.count - 1];
			}
			imageReplacements.count--;
			break;
		}
	}
}

static bool IsReplacedImage(int imageIndex)
{
	for(int i = 0; i < imageReplacements.count; ++i)
	{
		const ImageReplacement& repl = imageReplacements.images[i];
		if(imageIndex == repl.index)
		{
			return true;
		}
	}

	return false;
}

static void ClearImageReplacements()
{
	for(int i = 0; i < imageReplacements.count; ++i)
	{
		const ImageReplacement& repl = imageReplacements.images[i];
		if(repl.index >= 0 && repl.index < tr.numImages)
		{
			*tr.images[repl.index] = repl.original;
		}
	}
	imageReplacements.count = 0;
}

static void DrawFilter(char* filter, size_t filterSize)
{
	if(ImGui::Button("Clear filter"))
	{
		filter[0] = '\0';
	}
	ImGui::SameLine();
	if(ImGui::IsWindowAppearing())
	{
		ImGui::SetKeyboardFocusHere();
	}
	ImGui::InputText(" ", filter, filterSize);
	if(ImGui::IsItemHovered(ImGuiHoveredFlags_DelayNormal))
	{
		ImGui::SetTooltip("Use * to match any character any amount of times.");
	}
}

static void DrawImageToolTip(const image_t* image)
{
	const float scaleX = 128.0f / image->width;
	const float scaleY = 128.0f / image->height;
	const float scale = min(scaleX, scaleY);
	const float w = max(1.0f, scale * (float)image->width);
	const float h = max(1.0f, scale * (float)image->height);
	ImGui::BeginTooltip();
	ImGui::Image((ImTextureID)image->textureIndex, ImVec2(w, h));
	ImGui::EndTooltip();
}

static void DrawImageList()
{
	static bool listActive = false;
	ToggleBooleanWithShortcut(listActive, ImGuiKey_I, ShortcutOptions::Global);
	GUI_AddMainMenuItem(GUI_MainMenu::Tools, "Image Explorer", "Ctrl+Shift+I", &listActive);
	if(listActive)
	{
		if(ImGui::Begin("Image Explorer", &listActive))
		{
			if(imageReplacements.count > 0 && ImGui::Button("Restore Images"))
			{
				ClearImageReplacements();
			}

			static char rawFilter[256];
			DrawFilter(rawFilter, sizeof(rawFilter));

			struct ImageFilter
			{
				enum Id
				{
					None,
					World,
					NonWorld,
					Count
				};
			};

			static int imageFilter = ImageFilter::None;
			if(tr.world == NULL && imageFilter != ImageFilter::None)
			{
				imageFilter = ImageFilter::None;
			}
			if(tr.world != NULL)
			{
				ImGui::Text("Surface type");
				ImGui::RadioButton("All surfaces", &imageFilter, ImageFilter::None);
				ImGui::RadioButton("World only", &imageFilter, ImageFilter::World);
				ImGui::RadioButton("Non-world only", &imageFilter, ImageFilter::NonWorld);
			}

			if(BeginTable("Images", 1))
			{
				for(int i = 0; i < tr.numImages; ++i)
				{
					const image_t* image = tr.images[i];
					if((imageFilter == ImageFilter::World && image->worldSurfaceRefCount <= 0) ||
						(imageFilter == ImageFilter::NonWorld && image->worldSurfaceRefCount > 0))
					{
						continue;
					}

					char nameFilter[256];
					Com_sprintf(nameFilter, sizeof(nameFilter), rawFilter[0] == '*' ? "%s" : "*%s", rawFilter);
					if(nameFilter[0] != '\0' && !Com_Filter(nameFilter, image->name))
					{
						continue;
					}

					ImGui::TableNextRow();
					ImGui::TableSetColumnIndex(0);
					if(ImGui::Selectable(va("%s##%d", image->name, i), false))
					{
						OpenImageDetails(image);
					}
					else if(ImGui::IsItemHovered())
					{
						DrawImageToolTip(image);
					}
				}

				ImGui::EndTable();
			}
		}

		ImGui::End();
	}
}

// flp2 function from "Hacker's Delight"
static int LowerOrEqualPOT(int x)
{
	x = x | (x >> 1);
	x = x | (x >> 2);
	x = x | (x >> 4);
	x = x | (x >> 8);
	x = x | (x >> 16);

	return x - (x >> 1);
}

static void DrawShaderReferenceListForImage(const image_t* image)
{
	for(int is = 0; is < ARRAY_LEN(tr.imageShaders); ++is)
	{
		const int i = tr.imageShaders[is] & 0xFFFF;
		if(i != image->index)
		{
			continue;
		}

		const int s = (tr.imageShaders[is] >> 16) & (MAX_SHADERS - 1);
		const shader_t* const shader = tr.shaders[s];
		if(shader == NULL)
		{
			continue;
		}

		if(ImGui::Selectable(va("%s (LM %d)##%d", shader->name, shader->lightmapIndex, is), false))
		{
			OpenShaderDetails((shader_t*)shader);
		}
		else if(ImGui::IsItemHovered())
		{
			const char* const shaderPath = R_GetShaderPath(shader);
			if(shaderPath != NULL)
			{
				ImGui::BeginTooltip();
				ImGui::Text(shaderPath);
				ImGui::EndTooltip();
			}
		}
	}
}

static void DrawImageWindow()
{
	ImageWindow& window = imageWindow;
	if(window.active)
	{
		if(ImGui::Begin(IMAGE_WINDOW_NAME, &window.active, ImGuiWindowFlags_AlwaysAutoResize))
		{
			const image_t* const image = window.image;

			TitleText(image->name);

			if(ImGui::Button("Copy Name"))
			{
				ImGui::SetClipboardText(image->name);
			}

			char pakName[256];
			if(FS_GetPakPath(pakName, sizeof(pakName), image->pakChecksum))
			{
				ImGui::Text(pakName);
			}

			ImGui::Text("%dx%d", image->width, image->height);
			if(image->wrapClampMode == TW_CLAMP_TO_EDGE)
			{
				ImGui::SameLine();
				ImGui::Text("'clampMap'");
			}
			for(int f = 0; f < ARRAY_LEN(imageFlags); ++f)
			{
				if(image->flags & imageFlags[f].flag)
				{
					ImGui::SameLine();
					ImGui::Text(imageFlags[f].description);
				}
			}

			if(IsEditingAllowed())
			{
				if(IsReplacedImage(image->index))
				{
					if(ImGui::Button("Restore Image"))
					{
						RemoveImageReplacement(image->index);
					}
				}
				else
				{
					if(ImGui::Button("Replace Image"))
					{
						AddImageReplacement(image->index);
					}
				}
			}

			int shaderCount = 0;
			for(int is = 0; is < ARRAY_LEN(tr.imageShaders); ++is)
			{
				const int i = tr.imageShaders[is] & 0xFFFF;
				if(i == image->index)
				{
					shaderCount++;
				}
			}

			ImGui::NewLine();
			const char* const shaderListTitle = va("%d Shader Reference%s",
				shaderCount, shaderCount > 1 ? "s" : "");
			if(shaderCount > 3)
			{
				if(ImGui::CollapsingHeader(shaderListTitle, NULL, ImGuiTreeNodeFlags_None))
				{
					DrawShaderReferenceListForImage(image);
				}
			}
			else
			{
				ImGui::Text(shaderListTitle);
				DrawShaderReferenceListForImage(image);
			}
			ImGui::NewLine();

			static int manualScaleLog = 0;
			static bool autoScale = true;
			static bool autoScaleDownOnly = false;
			static bool autoScalePOT = false;
			bool usePointFilter = false;

			ImGui::Checkbox("Auto-scale", &autoScale);
			if(autoScale)
			{
				ImGui::Checkbox("Auto-scale down only", &autoScaleDownOnly);
				ImGui::Checkbox("Auto-scale to power of 2 dimensions", &autoScalePOT);
			}
			else
			{
				ImGui::SliderInt("Manual scale", &manualScaleLog, -10, 10, "%d", ImGuiSliderFlags_AlwaysClamp);
			}

			int width = image->width;
			int height = image->height;
			if((image->flags & IMG_NOMIPMAP) == 0)
			{
				ImGui::Combo("Mip", &window.mip, mipNames, R_ComputeMipCount(width, height));
			}
			for(int m = 0; m < window.mip; ++m)
			{
				width = max(width / 2, 1);
				height = max(height / 2, 1);
			}
			if(autoScale && autoScalePOT)
			{
				const ImVec2 workSize = ImGui::GetMainViewport()->WorkSize;
				const int maxWidth = LowerOrEqualPOT((int)workSize.x) / 2;
				const int maxHeight = LowerOrEqualPOT((int)workSize.y) / 2;
				if(width > maxWidth || height > maxHeight)
				{
					for(int i = 0; width > maxWidth || height > maxHeight; i++)
					{
						if(width == 1 || height == 1)
						{
							break;
						}
						width /= 2;
						height /= 2;
					}
				}
				else if(!autoScaleDownOnly)
				{
					usePointFilter = true;
					for(int i = 0; width * 2 <= maxWidth && height * 2 <= maxHeight; i++)
					{
						width *= 2;
						height *= 2;
					}
				}
			}
			else if(autoScale && !autoScalePOT)
			{
				const ImVec2 workSize = ImGui::GetMainViewport()->WorkSize;
				const int maxWidth = (int)(workSize.x * 0.75f);
				const int maxHeight = (int)(workSize.y * 0.75f);
				const bool downScale = width > maxWidth || height > maxHeight;
				const bool upScale = !downScale && !autoScaleDownOnly;
				if(downScale || upScale)
				{
					usePointFilter = upScale;
					const float scaleX = (float)width / (float)maxWidth;
					const float scaleY = (float)height / (float)maxHeight;
					const float scale = max(scaleX, scaleY);
					width = (int)((float)width / scale);
					height = (int)((float)height / scale);
				}
			}
			else if(manualScaleLog > 0)
			{
				usePointFilter = true;
				for(int i = 0; i < manualScaleLog; i++)
				{
					width *= 2;
					height *= 2;
				}
			}
			else if(manualScaleLog < 0)
			{
				for(int i = 0; i < -manualScaleLog; i++)
				{
					if(width == 1 || height == 1)
					{
						break;
					}
					width /= 2;
					height /= 2;
				}
			}

			const ImTextureID textureId =
				(ImTextureID)image->textureIndex |
				(ImTextureID)(window.mip << 16) |
				(usePointFilter ? ImTextureID(1 << 31) : ImTextureID(0));
			ImGui::Image(textureId, ImVec2(width, height));
		}

		ImGui::End();
	}
}

static void DrawShaderList()
{
	static bool listActive = false;
	ToggleBooleanWithShortcut(listActive, ImGuiKey_S, ShortcutOptions::Global);
	GUI_AddMainMenuItem(GUI_MainMenu::Tools, "Shader Explorer", "Ctrl+Shift+S", &listActive);
	if(listActive)
	{
		if(ImGui::Begin("Shader Explorer", &listActive))
		{
			if(shaderReplacements.count > 0 && ImGui::Button("Restore Shaders"))
			{
				ClearShaderReplacements();
			}

			if(tr.world != NULL)
			{
				if(tr.traceWorldShader)
				{
					if(ImGui::Button("Disable world shader tracing"))
					{
						tr.traceWorldShader = qfalse;
					}
					if((uint32_t)tr.tracedWorldShaderIndex < (uint32_t)tr.numShaders)
					{
						shader_t* shader = tr.shaders[tr.tracedWorldShaderIndex];
						if(ImGui::Selectable(va("%s##world_shader_trace", shader->name), false))
						{
							OpenShaderDetails(shader);
						}
					}
				}
				else
				{
					if(ImGui::Button("Enable world shader tracing"))
					{
						tr.traceWorldShader = qtrue;
					}
				}
			}

			static char rawFilter[256];
			DrawFilter(rawFilter, sizeof(rawFilter));

			struct ShaderFilter
			{
				enum Id
				{
					None,
					World,
					NonWorld,
					NonLightmappedWorld,
					Skybox,
					Fog,
					Count
				};
			};

			static int shaderFilter = ShaderFilter::None;
			if(tr.world == NULL && shaderFilter != ShaderFilter::None)
			{
				shaderFilter = ShaderFilter::None;
			}
			if(tr.world != NULL)
			{
				ImGui::Text("Surface type");
				ImGui::RadioButton("All surfaces", &shaderFilter, ShaderFilter::None);
				ImGui::RadioButton("World only", &shaderFilter, ShaderFilter::World);
				ImGui::RadioButton("Non-lightmapped world", &shaderFilter, ShaderFilter::NonLightmappedWorld);
				ImGui::RadioButton("Non-world only", &shaderFilter, ShaderFilter::NonWorld);
				ImGui::RadioButton("Skybox only", &shaderFilter, ShaderFilter::Skybox);
				ImGui::RadioButton("Fog only", &shaderFilter, ShaderFilter::Fog);
			}

			if(BeginTable("Shaders", 1))
			{
				for(int s = 0; s < tr.numShaders; ++s)
				{
					shader_t* shader = tr.shaders[s];
					if((shaderFilter == ShaderFilter::World && shader->worldSurfaceRefCount <= 0) ||
						(shaderFilter == ShaderFilter::NonWorld && shader->worldSurfaceRefCount > 0) ||
						(shaderFilter == ShaderFilter::Skybox && !shader->isSky) ||
						(shaderFilter == ShaderFilter::Fog && !shader->isFog) ||
						(shaderFilter == ShaderFilter::NonLightmappedWorld && (shader->hasLightmapStage || shader->worldSurfaceRefCount <= 0)))
					{
						continue;
					}

					char nameFilter[256];
					Com_sprintf(nameFilter, sizeof(nameFilter), rawFilter[0] == '*' ? "%s" : "*%s", rawFilter);
					if(nameFilter[0] != '\0' && !Com_Filter(nameFilter, shader->name))
					{
						continue;
					}

					ImGui::TableNextRow();
					ImGui::TableSetColumnIndex(0);
					if(ImGui::Selectable(va("%s##%d", shader->name, s), false))
					{
						OpenShaderDetails(shader);
					}
				}

				ImGui::EndTable();
			}
		}

		ImGui::End();
	}
}

static void BuildImageReferenceListForShader(const shader_t* shader)
{
	ShaderWindow& window = shaderWindow;
	window.displayImageCount = 0;

	if(shader->isSky)
	{
		for(int i = 0; i < 6; ++i)
		{
			const image_t* image = shader->sky.outerbox[i];
			if(image != NULL)
			{
				window.displayImages[window.displayImageCount++] = image;
			}
		}
	}

	for(int s = 0; s < shader->numStages; ++s)
	{
		const textureBundle_t& bundle = shader->stages[s]->bundle;
		const int imageCount = max(bundle.numImageAnimations, 1);
		for(int i = 0; i < imageCount; ++i)
		{
			const image_t* const image = bundle.image[i];
			bool found = false;
			for(int di = 0; di < window.displayImageCount; ++di)
			{
				if(window.displayImages[di] == image)
				{
					found = true;
					break;
				}
			}
			if(!found && window.displayImageCount < ARRAY_LEN(window.displayImages))
			{
				window.displayImages[window.displayImageCount++] = image;
			}
		}
	}
}

static void DrawImageReferenceList()
{
	const ShaderWindow& window = shaderWindow;
	for(int i = 0; i < window.displayImageCount; i++)
	{
		const image_t* const image = window.displayImages[i];
		if(ImGui::Selectable(va("%s", image->name), false))
		{
			OpenImageDetails(image);
		}
		else if(ImGui::IsItemHovered())
		{
			DrawImageToolTip(image);
		}
	}
}

static void DrawShaderWindow()
{
	ShaderWindow& window = shaderWindow;
	if(window.active)
	{
		if(ImGui::Begin(SHADER_WINDOW_NAME, &window.active, ImGuiWindowFlags_AlwaysAutoResize))
		{
			shader_t* const shader = window.shader;
			TitleText(shader->name);

			if(ImGui::Button("Copy Name"))
			{
				ImGui::SetClipboardText(shader->name);
			}

			const char* const shaderPath = R_GetShaderPath(shader);
			if(shaderPath != NULL)
			{
				ImGui::Text(shaderPath);
			}

			const bool isReplaced = IsReplacedShader(shader->index);
			const bool isCheating = IsEditingAllowed();

			if(isCheating)
			{
				if(isReplaced)
				{
					if(ImGui::Button("Restore Shader"))
					{
						RemoveShaderReplacement(shader->index);
					}
				}
				else
				{
					if(ImGui::Button("Replace Shader"))
					{
						AddShaderReplacement(shader->index);
					}
				}
			}

			ImGui::NewLine();
			ImGui::Text("Sort key: %g", shader->sort);
			ImGui::Text("Lightmap index: %d", shader->lightmapIndex);
			ImGui::BeginDisabled();
			Checkbox("Opaque", shader->isOpaque);
			Checkbox("Alpha-tested opaque", shader->isAlphaTestedOpaque);
			Checkbox("Lightmap", shader->hasLightmapStage);
			Checkbox("Dynamic vertex attribute(s)", shader->isDynamic);
			ImGui::EndDisabled();

			BuildImageReferenceListForShader(shader);
			const int imageCount = window.displayImageCount;
			ImGui::NewLine();
			const char* const imageListTitle = va("%d Image Reference%s",
				imageCount, imageCount > 1 ? "s" : "");
			if(imageCount > 3)
			{
				if(ImGui::CollapsingHeader(imageListTitle, NULL, ImGuiTreeNodeFlags_None))
				{
					DrawImageReferenceList();
				}
			}
			else
			{
				ImGui::Text(imageListTitle);
				DrawImageReferenceList();
			}
			ImGui::NewLine();

			if(isReplaced)
			{
				ImGui::Text("No code available (using default shader)");
			}
			else if(shaderEditWindow.active && window.shader == shaderEditWindow.shader)
			{
				ImGui::Text("Shader is being edited");
				if(ImGui::CollapsingHeader("Show Code##edit", NULL, ImGuiTreeNodeFlags_None))
				{
					ImGui::Separator();
					ImGui::Text("Original shader code shown here");
					ImGui::TextUnformatted(shaderEditWindow.originalCode);
					ImGui::Separator();
					ImGui::NewLine();
				}
				if(ImGui::Button("Copy Code"))
				{
					ImGui::SetClipboardText(shaderEditWindow.originalCode);
				}
			}
			else if(window.formattedCode[0] != '\0')
			{
				if(ImGui::CollapsingHeader("Show Code##normal", NULL, ImGuiTreeNodeFlags_None))
				{
					ImGui::Separator();
					ImGui::TextUnformatted(window.formattedCode);
					ImGui::Separator();
					ImGui::NewLine();
				}
				if(ImGui::Button("Copy Code"))
				{
					ImGui::SetClipboardText(window.formattedCode);
				}
			}
			else
			{
				ImGui::Text("No code available");
			}

			if(isCheating)
			{
				if(!shaderEditWindow.active || window.shader != shaderEditWindow.shader)
				{
					ImGui::NewLine();
					if(ImGui::Button("Edit Shader"))
					{
						if(isReplaced)
						{
							RemoveShaderReplacement(shader->index);
						}
						OpenShaderEdit(shader);
					}
					ImGui::Text("Use Ctrl+Shift+E to edit the shader under the crosshair.");
				}
			}
		}

		ImGui::End();
	}
}

struct ShaderEditToken
{
	bool HasValidDisplay() const
	{
		return display != NULL && display[0] != '\0';
	}

	bool HasExtraLevel() const
	{
		return tokens != NULL && tokenCount > 0;
	}

	const char* pattern;
	const char* display;
	const ShaderEditToken* tokens;
	int tokenCount;
};

#define T(Pattern, Display) { Pattern, Display, NULL, 0 }
#define T2(Pattern, Display, Tokens) { Pattern, Display, Tokens, ARRAY_LEN(Tokens) }

static const ShaderEditToken waveModes[] =
{
	T("sin", "<base> <amplitude> <phase> <freq>\nUnscaled range: [-1;1]"),
	T("triangle", "<base> <amplitude> <phase> <freq>\nUnscaled range: [-1;1]"),
	T("square", "<base> <amplitude> <phase> <freq>\nUnscaled range: [-1;1]"),
	T("noise", "<base> <amplitude> <phase> <freq>\nUnscaled range: [-1;1]"),
	T("sawtooth", "<base> <amplitude> <phase> <freq>\nUnscaled range: [0;1]"),
	T("inverseSawtooth", "<base> <amplitude> <phase> <freq>\nUnscaled range: [0;1]")
};

static const ShaderEditToken deformVertexesModes[] =
{
	T("wave", "<div> <func> <base> <amplitude> <phase> <freq>"),
	T("normal", "<amplitude> <freq>"),
	T("bulge", "<div> <ampl> <freq>"),
	T("move", "<x> <y> <z> <func> <base> <amplitude> <phase> <freq>"),
	T("autosprite", ""),
	T("autosprite2", "")
};

static const ShaderEditToken cullModes[] =
{
	T("back", ""),
	T("backside", ""),
	T("backsided", ""),
	T("none", "(two-sided)"),
	T("twosided", ""),
	T("disable", "(two-sided)")
};

static const ShaderEditToken sortKeys[] =
{
	T("portal", "(1)"),
	T("sky", "(2)"),
	T("opaque", "(3)"),
	T("decal", "(4)"),
	T("seeThrough", "(5)"),
	T("banner", "(6)"),
	T("additive", "(10)"),
	T("nearest", "(16)"),
	T("underwater", "(8)")
};

static const ShaderEditToken surfaceParms[] =
{
	T("water", ""),
	T("slime", ""),
	T("lava", ""),
	T("playerclip", ""),
	T("monsterclip", ""),
	T("nodrop", ""),
	T("nonsolid", ""),
	T("origin", ""),
	T("trans", ""),
	T("detail", ""),
	T("structural", ""),
	T("areaportal", ""),
	T("clusterportal", ""),
	T("donotenter", ""),
	T("fog", ""),
	T("sky", ""),
	T("lightfilter", ""),
	T("alphashadow", ""),
	T("hint", ""),
	T("slick", ""),
	T("noimpact", ""),
	T("nomarks", ""),
	T("ladder", ""),
	T("nodamage", ""),
	T("metalsteps", ""),
	T("nosteps", ""),
	T("nodraw", ""),
	T("pointlight", ""),
	T("nolightmap", ""),
	T("nodlight", ""),
	T("dust", "")
};

static const ShaderEditToken globalDirectives[] =
{
	T("skyParms", "<farbox> <cloudheight> <nearbox>"),
	T2("cull", "<side>", cullModes),
	T2("deformVertexes", "<mode>", deformVertexesModes),
	T("fogParms", "( <r> <g> <b> ) <distToOpaque>"),
	T("noPicMip", ""),
	T("noMipMaps", ""),
	T("polygonOffset", ""),
	T("portal", ""),
	T2("sort", "<value>\n"
		"1: portal      |  7: default fog\n"
		"2: sky         |  8: underwater\n"
		"3: opaque      |  9: default transparency\n"
		"4: decal       | 10: additive FX\n"
		"5: see-through | 14: stencil shadows\n"
		"6: banner      | 16: nearest",
		sortKeys),
	T2("surfaceParm", "<paramater>", surfaceParms),
	T("q3map_cnq3_depthFade", "<scale> <bias>")
};

static const ShaderEditToken rgbGenModes[] =
{
	T("identity", ""),
	T("identityLighting", ""),
	T("vertex", ""),
	T("oneMinusVertex", ""),
	T("exactVertex", ""),
	T("entity", ""),
	T("oneMinusEntity", ""),
	T("lightingDiffuse", ""),
	T2("wave", "<func> <base> <amplitude> <phase> <freq>", waveModes),
	T("const", "( <R> <G> <B> )")
};

static const ShaderEditToken alphaGenModes[] =
{
	T("lightingSpecular", ""),
	T2("wave", "<func> <base> <amplitude> <phase> <freq>", waveModes),
	T("entity", ""),
	T("oneMinusEntity", ""),
	T("vertex", ""),
	T("oneMinusVertex", ""),
	T("const", "<alphaValue>"),
	T("portal", "<distance>"),
};

static const ShaderEditToken tcGenSources[] =
{
	T("base", ""),
	T("lightmap", ""),
	T("environment", ""),
	T("vector", "( sx sy sz ) ( tx ty tz )")
};

static const ShaderEditToken tcModFuncs[] =
{
	T("rotate", "<degreesPerSecond>"),
	T("scale", "<x> <y>"),
	T("scroll", "<x> <y>"),
	T2("stretch", "<func> <base> <amplitude> <phase> <frequency>", waveModes),
	T("transform", "<m00> <m01> <m10> <m11> <t0> <t1>"),
	T("turb", "<base> <amplitude> <phase> <frequency>")
};

static const ShaderEditToken specialTextureNames[] =
{
	T("$lightmap", ""),
	T("$whiteimage", "")
};

static const ShaderEditToken depthComparisons[] =
{
	T("lequal", ""),
	T("equal", "")
};

static const ShaderEditToken alphaTests[] =
{
	T("GT0", ""),
	T("LT128", ""),
	T("GE128", "")
};

static const ShaderEditToken blendFuncDest[] =
{
	T("GL_ONE", ""),
	T("GL_ZERO", ""),
	T("GL_SRC_ALPHA", ""),
	T("GL_ONE_MINUS_SRC_ALPHA", ""),
	T("GL_SRC_COLOR", ""),
	T("GL_ONE_MINUS_SRC_COLOR", ""),
	T("GL_DST_ALPHA", ""),
	T("GL_ONE_MINUS_DST_ALPHA", "")
};

static const ShaderEditToken blendFuncSrc[] =
{
	T("add", ""),
	T("filter", ""),
	T("blend", ""),
	T2("GL_ONE", "", blendFuncDest),
	T2("GL_ZERO", "", blendFuncDest),
	T2("GL_DST_COLOR", "", blendFuncDest),
	T2("GL_ONE_MINUS_DST_COLOR", "", blendFuncDest),
	T2("GL_SRC_ALPHA", "", blendFuncDest),
	T2("GL_ONE_MINUS_SRC_ALPHA", "", blendFuncDest),
	T2("GL_DST_ALPHA", "", blendFuncDest),
	T2("GL_ONE_MINUS_DST_ALPHA", "", blendFuncDest),
	T2("GL_SRC_ALPHA_SATURATE", "", blendFuncDest)
};

static const ShaderEditToken stageDirectives[] =
{
	T2("map", "<texturePath>", specialTextureNames),
	T("clampMap", "<texturePath>"),
	T("animMap", "<frequency> <texture1> [texture2 ... texture8]"),
	T2("blendFunc", "<func> OR <source> <dest>", blendFuncSrc),
	T2("rgbGen", "<func>", rgbGenModes),
	T2("alphaGen", "<func>", alphaGenModes),
	T2("tcGen", "<source>", tcGenSources),
	T2("tcMod", "<func>", tcModFuncs),
	T2("depthFunc", "<depthComparison>", depthComparisons),
	T("depthWrite", ""),
	T("detail", ""),
	T2("alphaFunc", "<alphaTest>", alphaTests)
};

#undef T
#undef T2

enum ShaderEditChar : char
{
	Comment = -128,
	Uncomment,
	Home
};

static bool IsValidShaderEditChar(char c)
{
	return
		(c >= 32 && c <= 126) ||
		c == '\n' ||
		c == '\t';
}

static bool IsNonSpaceShaderEditChar(char c)
{
	return c >= 33 && c <= 126;
}

static int GetIndentation(const char* buffer, int bufferSize, int cursorPos)
{
	int indent = 0;
	for(int i = 0; i < cursorPos; i++)
	{
		const char c = buffer[i];
		if(c == '{')
		{
			indent++;
		}
		else if(c == '}')
		{
			indent--;
		}
	}

	return indent;
}

static int GetLineStartIndex(const char* buffer, int i)
{
	for(; i >= 0; i--)
	{
		if(buffer[i] == '\n')
		{
			return i + 1;
		}
	}

	return 0;
}

static void GetTextCursorCoords(vec2_t coords, int* outX, const char* buffer, int bufferSize, int cursorPos)
{
	const int l = (int)strlen(buffer);
	if(l == 0)
	{
		coords[0] = 0.0f;
		coords[1] = 0.0f;
		return;
	}

	cursorPos = max(cursorPos, 0);
	cursorPos = min(cursorPos, l - 1);
	cursorPos = min(cursorPos, bufferSize - 1);

	bool leadingSpace = true;
	int leadingSpaceCount = 0;
	int x = 0;
	int y = 0;
	for(int i = 0;; i++)
	{
		if(i >= cursorPos && !leadingSpace)
		{
			break;
		}

		const char c = buffer[i];
		if(c == '\n')
		{
			leadingSpace = true;
			leadingSpaceCount = 0;
			x = 0;
			y++;
		}
		else
		{
			if(leadingSpace)
			{
				if(c == ' ')
				{
					leadingSpaceCount++;
				}
				else
				{
					leadingSpace = false;
				}
			}
			x++;
		}
	}

	*outX = leadingSpaceCount;
	coords[0] = x;
	coords[1] = y;
}

static void CommentCode(ImGuiInputTextCallbackData* data)
{
	const int l = strlen(data->Buf);
	int start = min(data->SelectionStart, data->SelectionEnd);
	int end = max(data->SelectionStart, data->SelectionEnd);
	if(data->SelectionStart == data->SelectionEnd)
	{
		start = data->CursorPos;
		end = data->CursorPos;
	}
	if(end >= l)
	{
		end = l - 1;
	}

	for(int i = end; i >= start;)
	{
		const int prev = i;
		i = GetLineStartIndex(data->Buf, i - 1);
		if(data->Buf[i] != '/' || data->Buf[i + 1] != '/')
		{
			data->InsertChars(i, "//");
		}
		if(i == prev)
		{
			i--;
		}
		if(i <= 0)
		{
			break;
		}
	}
}

static void UncommentCode(ImGuiInputTextCallbackData* data)
{
	const int l = strlen(data->Buf);
	int start = min(data->SelectionStart, data->SelectionEnd);
	int end = max(data->SelectionStart, data->SelectionEnd);
	if(data->SelectionStart == data->SelectionEnd)
	{
		start = data->CursorPos;
		end = data->CursorPos;
	}
	if(end >= l)
	{
		end = l - 1;
	}

	for(int i = end; i >= start;)
	{
		const int prev = i;
		i = GetLineStartIndex(data->Buf, i - 1);
		if(data->Buf[i] == '/' && data->Buf[i + 1] == '/')
		{
			data->DeleteChars(i, 2);
		}
		if(i == prev)
		{
			i--;
		}
		if(i <= 0)
		{
			break;
		}
	}
}

static void ExtractShaderEditLine(const char* buffer, int bufferSize, int cursorPos)
{
	ShaderEditWindow& window = shaderEditWindow;

	cursorPos = max(cursorPos, 0);
	cursorPos = min(cursorPos, bufferSize - 1);
	const int cursor = cursorPos;
	int start = cursorPos - 1;
	for(; start >= 0; start--)
	{
		const char c = buffer[start];
		if(c == '\n')
		{
			start++;
			break;
		}
	}
	start = max(start, 0);
	int end = cursorPos;
	for(;; end++)
	{
		const char c = buffer[end];
		if(c == '\n' || c == '\0')
		{
			end--;
			break;
		}
	}

	const int length = end - start + 1;
	memcpy(window.currentLine, buffer + start, (size_t)length);
	window.currentLine[length] = '\0';
	window.currentLineCursor = cursor - start;
}

static int LengthOfCurrentLine(const char* string)
{
	int l = 0;
	const char* s = string;
	while(*s != '\0')
	{
		const char c = *s++;
		if(c == '\n')
		{
			l = 0;
		}
		else
		{
			l++;
		}
	}

	return l;
}

// returns true when handled
static bool AutoCompleteShaderKeyword(ShaderEditWindow& window, ImGuiInputTextCallbackData* data)
{
	if(data->SelectionStart == data->SelectionEnd)
	{
		GetTextCursorCoords(window.cursorCoords, &window.currentLineX, data->Buf, data->BufSize, data->CursorPos);
		window.validCursorCoords = true;
	}
	ExtractShaderEditLine(data->Buf, data->BufSize, data->CursorPos);
	window.cursor = data->CursorPos;

	const int indent = GetIndentation(window.code, sizeof(window.code), window.cursor);
	if(indent < 1 || indent > 2)
	{
		return false;
	}

	Cmd_TokenizeString(window.currentLine);
	const int tokenIndex = Cmd_ArgIndexFromOffset(window.currentLineCursor);
	const int tokenCount = Cmd_Argc();
	if(tokenCount <= 0 ||
		tokenIndex < 0 ||
		tokenIndex >= tokenCount)
	{
		return false;
	}

	const ShaderEditToken* patterns = globalDirectives;
	int patternCount = ARRAY_LEN(globalDirectives);
	if(indent == 2)
	{
		patterns = stageDirectives;
		patternCount = ARRAY_LEN(stageDirectives);
	}

	struct CompletionMatch
	{
		const char* string;
		int length;
	};

	CompletionMatch matches[64];
	int matchCount = 0;
	int minMatchLength = INT_MAX;
	char patternString[64];
	patternString[0] = '\0';
	for(int t = 0; t <= tokenIndex; t++)
	{
		if(patterns == NULL || patternCount == 0)
		{
			break;
		}

		const char* const token = Cmd_Argv(t);

		if(t < tokenIndex)
		{
			// when not processing the selected token, we only accept exact matches
			for(int p = 0; p < patternCount; p++)
			{
				const ShaderEditToken& pattern = patterns[p];
				if(!Q_stricmp(pattern.pattern, token))
				{
					patterns = pattern.tokens;
					patternCount = pattern.tokenCount;
					break;
				}
			}
			continue;
		}

		// we're processing the selected token,
		// which can either have an exact match or any number of partial matches
		bool fullMatch = false;
		for(int p = 0; p < patternCount; p++)
		{
			const ShaderEditToken& pattern = patterns[p];
			if(!Q_stricmp(token, pattern.pattern))
			{
				Q_strncpyz(patternString, pattern.pattern, sizeof(patternString));
				matchCount = 1;
				fullMatch = true;
				break;
			}
			if(matchCount < ARRAY_LEN(matches) &&
				Com_Filter(token, pattern.pattern))
			{
				const int l = strlen(pattern.pattern);
				CompletionMatch* match = &matches[matchCount++];
				match->string = pattern.pattern;
				match->length = l;
				minMatchLength = min(minMatchLength, l);
			}
		}

		if(!fullMatch && matchCount > 0)
		{
			int finalLength = minMatchLength;
			for(int c = 0; c < minMatchLength; c++)
			{
				const char candidate = matches[0].string[c];
				bool candidateValid = true;
				for(int m = 1; m < matchCount; m++)
				{
					const CompletionMatch& match = matches[m];
					if(match.string[c] != candidate)
					{
						candidateValid = false;
						finalLength = c;
						break;
					}
				}
				if(!candidateValid)
				{
					break;
				}
			}

			if(finalLength > 0)
			{
				finalLength = min(finalLength, (int)ARRAY_LEN(patternString) - 1);
				for(int i = 0; i < finalLength; i++)
				{
					patternString[i] = matches[0].string[i];
				}
				patternString[finalLength] = '\0';
			}
		}
	}

	if(patternString[0] == '\0')
	{
		// we found nothing to replace the selected token with
		return false;
	}

	const int wordOffset = window.currentLineCursor - Cmd_ArgOffset(tokenIndex);
	const int tokenLength = strlen(Cmd_Argv(tokenIndex));
	if(wordOffset >= 0 && wordOffset <= tokenLength)
	{
		// replace token with completion string (which might not be a full keyword)
		data->CursorPos -= wordOffset;
		data->DeleteChars(data->CursorPos, tokenLength);
		data->InsertChars(data->CursorPos, patternString);

		// insert a space if needed and move the cursor appropriately
		if(matchCount == 1)
		{
			if(tokenIndex == tokenCount - 1)
			{
				if(data->Buf[data->CursorPos] == ' ')
				{
					data->CursorPos++;
				}
				else
				{
					data->InsertChars(data->CursorPos, " ");
				}
			}
			else
			{
				const int wordStartDistance = Cmd_ArgOffset(tokenIndex + 1) - Cmd_ArgOffset(tokenIndex);
				const int nextWordOffset = wordStartDistance - tokenLength;
				if(nextWordOffset >= 0 &&
					data->CursorPos + nextWordOffset <= data->BufSize)
				{
					data->CursorPos += nextWordOffset;
				}
			}
		}
	}

	window.charCount = 0;

	return true;
}

static int ShaderEditCallback(ImGuiInputTextCallbackData* data)
{
	ShaderEditWindow& window = shaderEditWindow;

	if(data->EventFlag == ImGuiInputTextFlags_CallbackCompletion)
	{
		if(window.charCount < ARRAY_LEN(window.chars) - 1)
		{
			window.chars[window.charCount++] = '\t';
		}
		return 1;
	};

	if(data->EventFlag == ImGuiInputTextFlags_CallbackCharFilter)
	{
		// CTRL-V passes all characters through this too
		if(IsValidShaderEditChar(data->EventChar) &&
			window.charCount < ARRAY_LEN(window.chars) - 1)
		{
			window.chars[window.charCount++] = (char)data->EventChar;
		}
		return 1;
	}

	Q_assert(data->EventFlag == ImGuiInputTextFlags_CallbackAlways);
	window.validCursorCoords = false;

	// apply auto-completion
	if(window.charCount == 1 && window.chars[0] == '\t')
	{
		if(AutoCompleteShaderKeyword(window, data))
		{
			return 0;
		}
	}

	// move the caret to the first word after pressing home
	if(window.charCount == 1 && window.chars[0] == ShaderEditChar::Home)
	{
		ExtractShaderEditLine(data->Buf, data->BufSize, data->CursorPos);
		const char* s = window.currentLine;
		int skipCount = 0;
		while(*s != '\0')
		{
			const char c = *s++;
			if(c == ' ')
			{
				skipCount++;
			}
			else
			{
				break;
			}
		}
		data->CursorPos += skipCount;
	}

	// delete the selection if we started writing some text
	if(window.charCount > 0 &&
		data->SelectionStart != data->SelectionEnd)
	{
		int realCharCount = 0;
		int commandCharCount = 0;
		for(int i = 0; i < window.charCount; i++)
		{
			const char c = window.chars[i];
			if(c < 0)
			{
				commandCharCount++;
			}
			else
			{
				realCharCount++;
			}
		}

		if(realCharCount > 0 && commandCharCount == 0)
		{
			const int start = min(data->SelectionStart, data->SelectionEnd);
			const int end = max(data->SelectionStart, data->SelectionEnd);
			data->DeleteChars(start, end - start);
		}
	}

	// insert characters and handle special commands
	if(window.charCount > 0)
	{
		// analyze current line
		int lineCharCount = 0;
		int leadingSpaceCount = 0;
		bool lineStarted = false;
		for(int i = data->CursorPos - 1; i >= 0; i--)
		{
			const char c = window.code[i];
			if(c == '\n')
			{
				break;
			}
			if(c == ' ')
			{
				leadingSpaceCount++;
			}
			else
			{
				lineStarted = true;
				leadingSpaceCount = 0;
			}
			lineCharCount++;
		}

		char c[2] = { '\0', '\0' };
		for(int i = 0; i < window.charCount; i++)
		{
			c[0] = window.chars[i];

			// apply auto-formatting
			if(!lineStarted && IsNonSpaceShaderEditChar(c[0]))
			{
				lineStarted = true;
				const int spaceOffset = c[0] == '}' ? -4 : 0;
				const int spaceCount = spaceOffset + GetIndentation(data->Buf, data->BufSize, data->CursorPos) * 4;
				if(leadingSpaceCount > spaceCount)
				{
					const int count = leadingSpaceCount - spaceCount;
					data->DeleteChars(data->CursorPos - count, count);
				}
				else if(leadingSpaceCount < spaceCount)
				{
					const int count = spaceCount - leadingSpaceCount;
					for(int s = 0; s < count; s++)
					{
						data->InsertChars(data->CursorPos, " ");
					}
				}
			}

			if(c[0] == ShaderEditChar::Comment)
			{
				CommentCode(data);
			}
			else if(c[0] == ShaderEditChar::Uncomment)
			{
				UncommentCode(data);
			}
			else if(c[0] < 0)
			{
				// the remaining commands are not processed in this loop
			}
			else if(c[0] == '\n')
			{
				data->InsertChars(data->CursorPos, c);
				const int tabCount = GetIndentation(data->Buf, data->BufSize, data->CursorPos);
				for(int s = 0; s < tabCount; s++)
				{
					data->InsertChars(data->CursorPos, "    ");
				}
				const int spaceCount = tabCount * 4;
				leadingSpaceCount = spaceCount;
				lineCharCount = spaceCount;
				lineStarted = false;
			}
			else if(c[0] == '\t')
			{
				const int targetLineCharCount = (lineCharCount & (~3)) + 4;
				const int addCount = targetLineCharCount - lineCharCount;
				for(int s = 0; s < addCount; s++)
				{
					data->InsertChars(data->CursorPos, " ");
				}
				if(!lineStarted && addCount > 0)
				{
					leadingSpaceCount += addCount;
					lineCharCount = targetLineCharCount;
				}
			}
			else if(c[0] == ' ')
			{
				if(lineStarted)
				{
					data->InsertChars(data->CursorPos, c);
					lineCharCount++;
				}
			}
			else
			{
				data->InsertChars(data->CursorPos, c);
				lineCharCount++;
			}
		}
		window.charCount = 0;
	}

	if(data->SelectionStart == data->SelectionEnd)
	{
		GetTextCursorCoords(window.cursorCoords, &window.currentLineX, data->Buf, data->BufSize, data->CursorPos);
		window.validCursorCoords = true;
	}

	ExtractShaderEditLine(data->Buf, data->BufSize, data->CursorPos);

	window.cursor = data->CursorPos;

	return 0;
}

static void SaveShaderCodeToFile()
{
	ShaderEditWindow& window = shaderEditWindow;
	if(window.shader == NULL ||
		window.shader->name == NULL ||
		window.shaderFilePath[0] == '\0' ||
		window.code[0] == '\0')
	{
		return;
	}

	// only skip newlines and not anything that isn't '{' since we might skip valuable data
	Com_sprintf(window.fileContent, sizeof(window.fileContent), "%s\n", window.shader->name);
	const char* code = window.code;
	while(*code == '\n')
	{
		code++;
	}
	Q_strcat(window.fileContent, sizeof(window.fileContent), code);
	ri.FS_WriteFile(window.shaderFilePath, window.fileContent, strlen(window.fileContent));
}

static void DrawShaderEdit()
{
	ShaderEditWindow& window = shaderEditWindow;

	// handle the keyboard shortcut and delayed opening
	if(window.delayedShaderOpen >= 0)
	{
		window.delayedShaderOpen--;
	}
	const bool mapLoaded = tr.world != NULL;
	if((mapLoaded && IsShortcutPressed(ImGuiKey_E, ShortcutOptions::Global)) || window.delayedShaderOpen == 0)
	{
		const bool isCheating = IsEditingAllowed();
		if(isCheating && !tr.traceWorldShader)
		{
			tr.traceWorldShader = qtrue;
			window.delayedShaderOpen = 4;
		}
		else if(
			isCheating &&
			tr.traceWorldShader &&
			tr.tracedWorldShaderIndex >= 0 &&
			tr.tracedWorldShaderIndex < tr.numShaders)
		{
			OpenShaderEdit(tr.shaders[tr.tracedWorldShaderIndex]);
		}
	}

	// handle the window itself
	const bool wasActive = window.active;
	shader_t* const shader = window.shader;
	if(window.active && shader != NULL)
	{
		bool editing = false;
		ImVec2 cursorPos;

		ImRect hintClipRect;
		bool hintClipRectValid = false;

		if(ImGui::Begin(EDIT_WINDOW_NAME, &window.active, ImGuiWindowFlags_AlwaysAutoResize))
		{
			if(IsEditingAllowed())
			{
				static bool showOptions = false;
				static bool showShortcuts = false;
				static float scaleX = 0.0f;
				static float scaleY = 0.0f;

				const ImVec2 xSize = ImGui::CalcTextSize("X");
				const ImVec2 minSize = ImVec2(xSize.x * 60.0f, xSize.y * 30.0f);
				const ImVec2 maxSize = ImVec2(glConfig.vidWidth - 75.0f, glConfig.vidHeight - 250.0f);
				const ImVec2 inputSize = ImVec2(lerp(minSize.x, maxSize.x, scaleX), lerp(minSize.y, maxSize.y, scaleY));

				if(ImGui::CollapsingHeader("Show Options and Keyboard Shortcuts", NULL, ImGuiTreeNodeFlags_None))
				{
					ImGui::Separator();
					ImGui::SliderFloat("Window size X", &scaleX, 0.0f, 1.0f, "%g", ImGuiSliderFlags_AlwaysClamp);
					ImGui::SliderFloat("Window size Y", &scaleY, 0.0f, 1.0f, "%g", ImGuiSliderFlags_AlwaysClamp);
					if(ImGui::Button("Maximize"))
					{
						ImGui::SetWindowPos(EDIT_WINDOW_NAME, ImVec2(25, 25), ImGuiCond_None);
						scaleX = 1.0f;
						scaleY = 1.0f;
					}
					ImGui::SameLine();
					if(ImGui::Button("Minimize"))
					{
						scaleX = 0.0f;
						scaleY = 0.0f;
					}

					ImGui::NewLine();
					ImGui::Text("Ctrl+K/L: Comment/uncomment code");
					ImGui::Text("Ctrl+H  : Toggle hinting");
					ImGui::Text("Ctrl+S  : Save to shader file");
					ImGui::Text("     F5 : Apply code changes");
					ImGui::Separator();
				}

				ImGui::NewLine();
				TitleText(shader->name);
				ImGui::SameLine(inputSize.x - ImGui::CalcTextSize("Open Details").x);
				if(ImGui::Button("Open Details"))
				{
					OpenShaderDetails(shader);
				}

				if(ImGui::IsWindowAppearing())
				{
					ImGui::SetKeyboardFocusHere();
				}

				// need to get the cursor position before drawing the text edit
				// so we know where its top left corner is
				cursorPos = ImGui::GetWindowPos();
				cursorPos.x += ImGui::GetCursorPos().x;
				cursorPos.y += ImGui::GetCursorPos().y;

				const char* const editBoxLabel = va("##Shader Code: %s", shader->name);
				const int callbackFlags =
					ImGuiInputTextFlags_CallbackCompletion |
					ImGuiInputTextFlags_CallbackCharFilter |
					ImGuiInputTextFlags_CallbackAlways;
				ImGui::InputTextMultiline(
					editBoxLabel, window.code, sizeof(window.code), inputSize,
					callbackFlags, &ShaderEditCallback);

				ImGuiContext& imguiContext = *GImGui;
				const char* const multiLineName = va("%s/%s_%08X", imguiContext.CurrentWindow->Name, editBoxLabel, ImGui::GetID(editBoxLabel));
				ImGuiWindow* const multiLineWindow = ImGui::FindWindowByName(multiLineName);
				if(multiLineWindow != NULL)
				{
					cursorPos.y -= multiLineWindow->Scroll.y;
					hintClipRect = multiLineWindow->InnerClipRect;
					hintClipRectValid = true;
				}

				if(window.validCursorCoords)
				{
					// now we can apply the offset within the text
					cursorPos.x += ImGui::GetFont()->CalcTextSizeA(ImGui::GetFontSize(), FLT_MAX, 0.0f,
						window.currentLine, window.currentLine + window.currentLineX).x;
					cursorPos.y += (window.cursorCoords[1] + 1.5f) * ImGui::GetFontSize();
					editing = true;
				}

				bool saveFile = false;
				if(IsShortcutPressed(ImGuiKey_H, ShortcutOptions::Local))
				{
					ri.Cvar_Set(r_guiShaderEditHints->name, r_guiShaderEditHints->integer != 0 ? "0" : "1");
				}
				else if(IsShortcutPressed(ImGuiKey_S, ShortcutOptions::Local))
				{
					saveFile = true;
				}
				else if(ImGui::IsKeyPressed(ImGuiKey_F5, false))
				{
					R_EditShader(window.shader, &window.originalShader, window.code);
				}
				else if(window.charCount < ARRAY_LEN(window.chars) - 1)
				{
					const bool ctrlDown = ImGui::IsKeyDown(ImGuiMod_Ctrl);
					const bool shiftDown = ImGui::IsKeyDown(ImGuiMod_Shift);
					if(IsShortcutPressed(ImGuiKey_K, ShortcutOptions::Local))
					{
						window.chars[window.charCount++] = ShaderEditChar::Comment;
					}
					else if(IsShortcutPressed(ImGuiKey_L, ShortcutOptions::Local))
					{
						window.chars[window.charCount++] = ShaderEditChar::Uncomment;
					}
					else if(!ctrlDown && !shiftDown && ImGui::IsKeyPressed(ImGuiKey_Home, false))
					{
						window.chars[window.charCount++] = ShaderEditChar::Home;
					}
				}

				ImGui::NewLine();
				if(ImGui::Button("Apply Code"))
				{
					R_EditShader(window.shader, &window.originalShader, window.code);
				}
				ImGui::SameLine();
				if(ImGui::Button("Save to File"))
				{
					saveFile = true;
				}
				ImGui::SameLine(inputSize.x - ImGui::CalcTextSize("Close and Discard").x);
				if(ImGui::Button("Close and Discard"))
				{
					window.active = false;
				}

				if(tr.shaderParseFailed || tr.shaderParseNumWarnings > 0)
				{
					ImGui::NewLine();
				}
				if(tr.shaderParseFailed)
				{
					ImGui::TextColored(ImVec4(1.0f, 0.25f, 0.0f, 1.0f), "Error: %s", tr.shaderParseError);
				}
				for(int i = 0; i < tr.shaderParseNumWarnings; ++i)
				{
					ImGui::TextColored(ImVec4(1.0f, 0.5f, 0.0f, 1.0f), "Warning: %s", tr.shaderParseWarnings[i]);
				}

				if(saveFile)
				{
					if(window.shaderFilePath[0] == '\0')
					{
						SaveFileDialog_Open("scripts", ".shader");
					}
					else
					{
						SaveShaderCodeToFile();
					}
				}

				if(SaveFileDialog_Do())
				{
					Q_strncpyz(window.shaderFilePath, SaveFileDialog_GetPath(), sizeof(window.shaderFilePath));
					SaveShaderCodeToFile();
				}
			}
			else
			{
				TitleText(shader->name);
				ImGui::Text("Enable cheats to use");
			}
		}

		ImGui::End();

		if(editing && r_guiShaderEditHints->integer != 0)
		{
			const int indent = GetIndentation(window.code, sizeof(window.code), window.cursor);
			Cmd_TokenizeString(window.currentLine);
			const int tokenIndex = Cmd_ContinuousArgIndexFromOffset(window.currentLineCursor);
			const int tokenCount = Cmd_Argc();
			if((indent == 1 || indent == 2) && tokenCount > 0)
			{
				window.toolTip[0] = '\0';

				const ShaderEditToken* patterns = globalDirectives;
				int patternCount = ARRAY_LEN(globalDirectives);
				if(indent == 2)
				{
					patterns = stageDirectives;
					patternCount = ARRAY_LEN(stageDirectives);
				}
				int printedArgCount = 0;
				for(int t = 0; t < tokenCount; t++)
				{
					if(patterns == NULL || patternCount == 0)
					{
						break;
					}

					const char* token = Cmd_Argv(t);
					if(t == tokenIndex)
					{
						int partialMatchCount = 0;
						for(int p = 0; p < patternCount; p++)
						{
							const ShaderEditToken& pattern = patterns[p];
							if(!Q_stricmp(token, pattern.pattern))
							{
								if(printedArgCount > 0)
								{
									Q_strcat(window.toolTip, sizeof(window.toolTip), " ");
								}
								Q_strcat(window.toolTip, sizeof(window.toolTip), pattern.pattern);
								if(pattern.HasValidDisplay())
								{
									Q_strcat(window.toolTip, sizeof(window.toolTip), " ");
									Q_strcat(window.toolTip, sizeof(window.toolTip), pattern.display);
								}
								printedArgCount++;
								if(pattern.HasExtraLevel())
								{
									Q_strcat(window.toolTip, sizeof(window.toolTip), "\n");
									Q_strcat(window.toolTip, sizeof(window.toolTip), pattern.tokens[0].pattern);
									for(int t2 = 1; t2 < pattern.tokenCount; t2++)
									{
										Q_strcat(window.toolTip, sizeof(window.toolTip), ",");
										if(LengthOfCurrentLine(window.toolTip) >= 64)
										{
											Q_strcat(window.toolTip, sizeof(window.toolTip), "\n");
										}
										Q_strcat(window.toolTip, sizeof(window.toolTip), pattern.tokens[t2].pattern);
									}
								}
								break;
							}
							if(Com_Filter(token, pattern.pattern))
							{
								if(printedArgCount > 0 && partialMatchCount == 0)
								{
									Q_strcat(window.toolTip, sizeof(window.toolTip), " ");
								}
								if(partialMatchCount > 0)
								{
									Q_strcat(window.toolTip, sizeof(window.toolTip), ",");
									if(LengthOfCurrentLine(window.toolTip) >= 64)
									{
										Q_strcat(window.toolTip, sizeof(window.toolTip), "\n");
									}
								}
								Q_strcat(window.toolTip, sizeof(window.toolTip), pattern.pattern);
								partialMatchCount++;
								printedArgCount++;
							}
						}
					}
					else
					{
						for(int p = 0; p < patternCount; p++)
						{
							const ShaderEditToken& pattern = patterns[p];
							if(!Q_stricmp(pattern.pattern, token))
							{
								if(printedArgCount > 0)
								{
									Q_strcat(window.toolTip, sizeof(window.toolTip), " ");
								}
								Q_strcat(window.toolTip, sizeof(window.toolTip), pattern.pattern);
								if(!pattern.HasExtraLevel() && pattern.HasValidDisplay())
								{
									Q_strcat(window.toolTip, sizeof(window.toolTip), " ");
									Q_strcat(window.toolTip, sizeof(window.toolTip), pattern.display);
								}
								patterns = pattern.tokens;
								patternCount = pattern.tokenCount;
								printedArgCount++;
								break;
							}
						}
					}
				}

				if(window.toolTip[0] != '\0')
				{
					bool draw = true;
					if(hintClipRectValid)
					{
						if(cursorPos.y < hintClipRect.Min.y ||
							cursorPos.y > hintClipRect.Max.y)
						{
							draw = false;
						}
					}

					if(draw)
					{
						ImGui::SetNextWindowPos(cursorPos, ImGuiCond_Always);
						ImGui::BeginTooltip();
						ImGui::TextColored(ImVec4(1, 0.5f, 0, 1), window.toolTip);
						ImGui::EndTooltip();
					}
				}
			}
		}
	}

	if(wasActive && !window.active)
	{
		CloseShaderEdit(true);
	}
}

static void DrawFrontEndStats()
{
	static bool active = false;
	GUI_AddMainMenuItem(GUI_MainMenu::Perf, "Front-end stats", "", &active);
	if(active)
	{
		if(ImGui::Begin("Front-end stats", &active, ImGuiWindowFlags_AlwaysAutoResize))
		{
			if(BeginTable("General stats", 2))
			{
				struct Item
				{
					int index;
					const char* title;
				};
				const Item items[] =
				{
					{ RF_LEAF_CLUSTER, "PVS cluster" },
					{ RF_LEAF_AREA, "PVS area" },
					{ RF_LEAFS, "Leaves" },
					{ RF_LIT_LEAFS, "Lit leaves" },
					{ RF_LIT_SURFS, "Lit surfaces" },
					{ RF_LIT_CULLS, "Lit surfaces culled" },
					{ RF_LIGHT_CULL_IN, "Lights kept" },
					{ RF_LIGHT_CULL_OUT, "Lights rejected" }
				};

				for(int i = 0; i < ARRAY_LEN(items); ++i)
				{
					const Item& item = items[i];
					TableRow(2, item.title, va("%d", tr.pc[item.index]));
				}

				ImGui::EndTable();
			}

			if(BeginTable("Culling stats", 4))
			{
				struct Item
				{
					int indexIn;
					int indexClip;
					int indexOut;
					const char* title;
				};
				const Item items[] =
				{
					{ RF_MD3_CULL_S_IN, RF_MD3_CULL_S_CLIP, RF_MD3_CULL_S_OUT, "MD3 vs sphere" },
					{ RF_MD3_CULL_B_IN, RF_MD3_CULL_B_CLIP, RF_MD3_CULL_B_OUT, "MD3 vs box" },
					{ RF_BEZ_CULL_S_IN, RF_BEZ_CULL_S_CLIP, RF_BEZ_CULL_S_OUT, "Grid vs sphere" },
					{ RF_BEZ_CULL_B_IN, RF_BEZ_CULL_B_CLIP, RF_BEZ_CULL_B_OUT, "Grid vs box" }
				};

				ImGui::TableSetupColumn("Surface");
				ImGui::TableSetupColumn("In", ImGuiTableColumnFlags_WidthFixed, ImGui::GetFont()->FontSize * 4.0f);
				ImGui::TableSetupColumn("Clip", ImGuiTableColumnFlags_WidthFixed, ImGui::GetFont()->FontSize * 4.0f);
				ImGui::TableSetupColumn("Out", ImGuiTableColumnFlags_WidthFixed, ImGui::GetFont()->FontSize * 4.0f);
				ImGui::TableHeadersRow();
				for(int i = 0; i < ARRAY_LEN(items); ++i)
				{
					const Item& item = items[i];
					TableRow(4, item.title,
						va("%d", tr.pc[item.indexIn]),
						va("%d", tr.pc[item.indexClip]),
						va("%d", tr.pc[item.indexOut]));
				}

				ImGui::EndTable();
			}
		}

		ImGui::End();
	}
}

static ImVec4 ParseHexRGB(const char* text)
{
	float c[4];
	Com_ParseHexColor(c, text, qfalse);

	return ImVec4(c[0], c[1], c[2], 1.0f);
}

static ImVec4 ParseHexRGBA(const char* text)
{
	float c[4];
	Com_ParseHexColor(c, text, qtrue);

	return ImVec4(c[0], c[1], c[2], c[3]);
}

static float GetVerticalFov(float hfov)
{
	const float w = 1.0f;
	const float h = (float)glConfig.vidHeight / (float)glConfig.vidWidth;
	const float adj = w / tanf(DEG2RAD(0.5f * hfov));
	const float vfov = 2.0f * RAD2DEG(atanf(h / adj));

	return vfov;
}

static float GetHorizontalFov(float vfov)
{
	const float w = 1.0f;
	const float h = (float)glConfig.vidHeight / (float)glConfig.vidWidth;
	const float adj = h / tanf(DEG2RAD(0.5f * vfov));
	const float hfov = 2.0f * RAD2DEG(atanf(w / adj));

	return hfov;
}

static void DrawCVarValue(cvar_t* cvar)
{
	if(cvar->flags & (CVAR_ROM | CVAR_INIT))
	{
		ImGui::Text(cvar->string);
		return;
	}

	if(cvar->type == CVART_COLOR_RGB)
	{
		const char* const id = cvar->name;
		const char* const oldValue = cvar->latchedString != NULL ? cvar->latchedString : cvar->string;
		const ImVec4 color = ParseHexRGB(oldValue);
		const ImGuiColorEditFlags flags =
			ImGuiColorEditFlags_NoLabel |
			ImGuiColorEditFlags_NoInputs |
			ImGuiColorEditFlags_InputRGB |
			ImGuiColorEditFlags_NoAlpha |
			ImGuiColorEditFlags_PickerHueWheel;
		float outColor[4] = { color.x, color.y, color.z, 1.0f };

		if(ImGui::ColorEdit3(va("%s##picker_%s", cvar->gui.title, id), outColor, flags))
		{
			const unsigned int r = outColor[0] * 255.0f;
			const unsigned int g = outColor[1] * 255.0f;
			const unsigned int b = outColor[2] * 255.0f;
			Cvar_Set2(cvar->name, va("%02X%02X%02X", r, g, b), 0);
		}
	}
	else if(cvar->type == CVART_COLOR_RGBA)
	{
		const char* const id = cvar->name;
		const char* const oldValue = cvar->latchedString != NULL ? cvar->latchedString : cvar->string;
		const ImVec4 color = ParseHexRGBA(oldValue);
		const ImGuiColorEditFlags flags =
			ImGuiColorEditFlags_NoLabel |
			ImGuiColorEditFlags_NoInputs |
			ImGuiColorEditFlags_InputRGB |
			ImGuiColorEditFlags_AlphaPreview |
			ImGuiColorEditFlags_AlphaBar |
			ImGuiColorEditFlags_PickerHueWheel;
		float outColor[4] = { color.x, color.y, color.z, color.w };

		if(ImGui::ColorEdit4(va("%s##picker_%s", cvar->gui.title, id), outColor, flags))
		{
			const unsigned int r = outColor[0] * 255.0f;
			const unsigned int g = outColor[1] * 255.0f;
			const unsigned int b = outColor[2] * 255.0f;
			const unsigned int a = outColor[3] * 255.0f;
			Cvar_Set2(cvar->name, va("%02X%02X%02X%02X", r, g, b, a), 0);
		}
	}
	else if(cvar->type == CVART_COLOR_CPMA || !Q_stricmp(cvar->name, "ch_eventOwnColor") || !Q_stricmp(cvar->name, "ch_eventEnemyColor"))
	{
		const char* const oldValue = cvar->latchedString != NULL ? cvar->latchedString : cvar->string;
		char colorCode = oldValue[0];
		if(CPMAColorCodeButton(NULL, cvar->name, colorCode))
		{
			Cvar_Set2(cvar->name, va("%c", colorCode), 0);
		}
	}
	else if(cvar->type == CVART_COLOR_CHBLS || !Q_stricmp(cvar->name, "color"))
	{
		const char* titles[5] =
		{
			"Rail core",
			"Head / helmet / visor",
			"Body / shirt",
			"Legs",
			"Rail spiral"
		};

		const char* const oldValue = cvar->latchedString != NULL ? cvar->latchedString : cvar->string;
		char colorCodes[6] = { '7', '7', '7', '7', '7', '\0' };
		for(int i = 0; i < 5; ++i)
		{
			if(oldValue[i] == '\0')
			{
				break;
			}
			colorCodes[i] = (char)toupper(oldValue[i]);
		}

		bool clicked = false;
		for(int i = 0; i < 5; ++i)
		{
			if(CPMAColorCodeButton(titles[i], va("%s_%d", cvar->name, i), colorCodes[i]))
			{
				clicked = true;
			}
			ImGui::SameLine();
			ImGui::Text(titles[i]);
		}
		Cvar_Set2(cvar->name, colorCodes, 0);
	}
	else if(cvar->gui.numValues > 0 && cvar->type == CVART_BITMASK)
	{
		const int oldValue = cvar->latchedString != NULL ? atoi(cvar->latchedString) : cvar->integer;
		for(int i = 0; i < cvar->gui.numValues; ++i)
		{
			cvarGuiValue_t* const value = &cvar->gui.values[i];
			const int bitIndex = atoi(value->value);
			const int bitMask = 1 << bitIndex;
			bool bitValue = (oldValue & bitMask) != 0;
			if(ImGui::Checkbox(value->title, &bitValue))
			{
				int newValue = oldValue;
				if(bitValue)
				{
					newValue |= bitMask;
				}
				else
				{
					newValue &= ~bitMask;
				}
				Cvar_Set2(cvar->name, va("%d", newValue), 0);
			}
			SetTooltipIfValid(value->desc);
		}
	}
	else if(cvar->gui.numValues == 2 && cvar->type == CVART_BOOL)
	{
		// keep the value order: the value for 1 can come before the value for 0
		// this is on purpose because there are CVars phrased as a negative
		const bool oldValue = cvar->latchedString ? (atoi(cvar->latchedString) != 0) : (cvar->integer != 0);
		const cvarGuiValue_t* const values = cvar->gui.values;
		const int i0 = atoi(values[0].value);
		const int i1 = atoi(values[1].value);
		const cvarGuiValue_t* const value0 = &values[0];
		const cvarGuiValue_t* const value1 = &values[1];
		int currValue = oldValue ? 1 : 0;

		if(ImGui::RadioButton(value0->title, &currValue, i0))
		{
			Cvar_Set2(cvar->name, values[0].value, 0);
		}
		SetTooltipIfValid(value0->desc);

		ImGui::SameLine();

		if(ImGui::RadioButton(value1->title, &currValue, i1))
		{
			Cvar_Set2(cvar->name, values[1].value, 0);
		}
		SetTooltipIfValid(value1->desc);
	}
	else if(cvar->gui.numValues == 2 && cvar->type == CVART_INTEGER &&
		cvar->validator.i.max == cvar->validator.i.min + 1)
	{
		const int oldValue = cvar->latchedString ? atoi(cvar->latchedString) : cvar->integer;
		const cvarGuiValue_t* const values = cvar->gui.values;
		const cvarGuiValue_t* const value0 = &values[0];
		const cvarGuiValue_t* const value1 = &values[1];
		const int i0 = atoi(value0->value);
		const int i1 = atoi(value1->value);
		int currValue = oldValue;

		if(ImGui::RadioButton(value0->title, &currValue, i0))
		{
			Cvar_Set2(cvar->name, value0->value, 0);
		}
		SetTooltipIfValid(value0->desc);

		ImGui::SameLine();

		if(ImGui::RadioButton(value1->title, &currValue, i1))
		{
			Cvar_Set2(cvar->name, value1->value, 0);
		}
		SetTooltipIfValid(value1->desc);
	}
	else if(cvar->gui.numValues > 0)
	{
		const char* const oldValue = cvar->latchedString != NULL ? cvar->latchedString : cvar->string;
		const char* previewString = NULL;
		for(int i = 0; i < cvar->gui.numValues; ++i)
		{
			cvarGuiValue_t* const value = &cvar->gui.values[i];
			if(strcmp(oldValue, value->value) == 0)
			{
				previewString = value->title;
				break;
			}
		}
		if(previewString == NULL)
		{
			previewString = oldValue;
		}

		ImGui::SetNextItemWidth(ImGui::GetIO().FontDefault->FontSize * 15.0f);
		if(ImGui::BeginCombo(va("##%s", cvar->name), previewString, ImGuiComboFlags_None))
		{
			for(int i = 0; i < cvar->gui.numValues; ++i)
			{
				cvarGuiValue_t* const value = &cvar->gui.values[i];
				bool selected = false;
				if(ImGui::Selectable(value->title, &selected, ImGuiSelectableFlags_None))
				{
					Cvar_Set2(cvar->name, value->value, 0);
				}
				SetTooltipIfValid(value->desc);
			}

			ImGui::EndCombo();
		}
	}
	else if(cvar->type == CVART_STRING)
	{
		if(!Q_stricmp(cvar->name, "mvw_DM") ||
			!Q_stricmp(cvar->name, "mvw_TDM1") ||
			!Q_stricmp(cvar->name, "mvw_TDM2") ||
			!Q_stricmp(cvar->name, "mvw_TDM3") ||
			!Q_stricmp(cvar->name, "mvw_TDM4"))
		{
			int v[4] = {};
			const char* const oldValue = cvar->latchedString != NULL ? cvar->latchedString : cvar->string;
			sscanf(oldValue, "%d %d %d %d", &v[0], &v[1], &v[2], &v[3]);
			if(ImGui::SliderInt4(va("##%s", cvar->name), v, 0, 640, "%d", ImGuiSliderFlags_AlwaysClamp))
			{
				Cvar_Set2(cvar->name, va("%d %d %d %d", v[0], v[1], v[2], v[3]), 0);
			}
		}
		else
		{
			static char text[256];
			Q_strncpyz(text, cvar->string, sizeof(text));
			if(ImGui::InputText(va("##%s", cvar->name), text, sizeof(text)))
			{
				Cvar_Set2(cvar->name, text, 0);
			}
		}
	}
	else if(cvar->type == CVART_BOOL)
	{
		const bool oldValue = cvar->latchedString ? (atoi(cvar->latchedString) != 0) : (cvar->integer != 0);
		bool curValue = oldValue;
		ImGui::Checkbox(va("##%s", cvar->name), &curValue);
		if(curValue != oldValue)
		{
			Cvar_Set2(cvar->name, va("%d", (int)curValue), 0);
		}
	}
	else if(cvar->type == CVART_INTEGER || cvar->type == CVART_BITMASK)
	{
		const int oldValue = cvar->latchedString ? atoi(cvar->latchedString) : cvar->integer;
		int curValue = oldValue;
		int min = cvar->validator.i.min;
		int max = cvar->validator.i.max;
		min = (min == INT32_MIN) ? -(1 << 20) : min;
		max = (max == INT32_MAX) ?  (1 << 20) : max;
		ImGui::SetNextItemWidth(ImGui::GetIO().FontDefault->FontSize * 15.0f);
		ImGui::SliderInt(va("##%s", cvar->name), &curValue, min, max,
			"%d", ImGuiSliderFlags_AlwaysClamp);
		if(curValue != oldValue)
		{
			Cvar_Set2(cvar->name, va("%d", curValue), 0);
		}
	}
	else if(cvar->type == CVART_FLOAT && (!Q_stricmp(cvar->name, "cg_fov") || !Q_stricmp(cvar->name, "cg_zoomFov")))
	{
		const float oldValue = cvar->latchedString ? atof(cvar->latchedString) : cvar->value;
		const float hMin = cvar->validator.f.min;
		const float hMax = cvar->validator.f.max;
		const float vMin = GetVerticalFov(hMin);
		const float vMax = GetVerticalFov(hMax);
		float curValue = oldValue;
		ImGui::SetNextItemWidth(ImGui::GetIO().FontDefault->FontSize * 10.0f);
		const bool hChange = ImGui::SliderFloat(va("Horizontal##%s", cvar->name), &curValue, hMin, hMax,
			"%g", ImGuiSliderFlags_AlwaysClamp);
		curValue = GetVerticalFov(curValue);
		ImGui::SetNextItemWidth(ImGui::GetIO().FontDefault->FontSize * 10.0f);
		const bool vChange = ImGui::SliderFloat(va("Vertical##%s", cvar->name), &curValue, vMin, vMax,
			"%g", ImGuiSliderFlags_AlwaysClamp);
		curValue = GetHorizontalFov(curValue);
		if(hChange || vChange)
		{
			Cvar_Set2(cvar->name, va("%g", curValue), 0);
		}
	}
	else if(cvar->type == CVART_FLOAT)
	{
		const float oldValue = cvar->latchedString ? atof(cvar->latchedString) : cvar->value;
		float curValue = oldValue;
		float min = cvar->validator.f.min;
		float max = cvar->validator.f.max;
		min = (min == -FLT_MAX) ? -1048576.0f : min;
		max = (max ==  FLT_MAX) ?  1048576.0f : max;
		ImGui::SetNextItemWidth(ImGui::GetIO().FontDefault->FontSize * 15.0f);
		ImGui::SliderFloat(va("##%s", cvar->name), &curValue, min, max,
			"%g", ImGuiSliderFlags_AlwaysClamp);
		if(curValue != oldValue)
		{
			Cvar_Set2(cvar->name, va("%g", curValue), 0);
		}
	}
}

static void DrawCVarToolTip(cvar_t* cvar)
{
	if(!ImGui::IsItemHovered(ImGuiHoveredFlags_DelayNormal))
	{
		return;
	}

	const char* const tooltipTextRaw = IsNonEmpty(cvar->gui.help) ? cvar->gui.help : "";
	const char* const tooltipTextClean = RemoveColorCodes(tooltipTextRaw);
	char help[1024];
	if(IsNullOrEmpty(tooltipTextClean))
	{
		Com_sprintf(help, sizeof(help), "CVar: %s", cvar->name);
	}
	else
	{
		Com_sprintf(help, sizeof(help), "CVar: %s\n\n%s", cvar->name, tooltipTextClean);
	}

	ImGui::SetTooltip(help);
}

static void DrawCVarNoValue(cvar_t* cvar)
{
	Q_assert(cvar != NULL);
	if((cvar->gui.categories & CVARCAT_DEBUGGING) == 0)
	{
		Q_assert(IsNonEmpty(cvar->gui.title));
	}

	ImGui::TableNextRow();
	ImGui::TableSetColumnIndex(0);
	ImGui::Text(cvar->gui.title);
	DrawCVarToolTip(cvar);
	if((cvar->flags & (CVAR_ROM | CVAR_INIT)) == 0)
	{
		ImGui::TableSetColumnIndex(2);
		if(ImGui::Button(va("R##%s", cvar->name)))
		{
			Cvar_Set2(cvar->name, cvar->resetString, 0);
		}
	}
	ImGui::TableSetColumnIndex(3);
	ImGui::Text(cvar->latchedString != NULL ? cvar->latchedString : "");
	ImGui::TableSetColumnIndex(4);
	const char* const desc = IsNonEmpty(cvar->gui.desc) ? cvar->gui.desc : "";
	ImGui::TextWrapped(RemoveColorCodes(desc));
	DrawCVarToolTip(cvar);
}

static void DrawCVar(const char* cvarName)
{
	cvar_t* cvar = Cvar_FindVar(cvarName);
	if(cvar == NULL)
	{
		return;
	}

	DrawCVarNoValue(cvar);
	ImGui::TableSetColumnIndex(1);
	DrawCVarValue(cvar);
}

static void DrawCVarValueCombo(const char* cvarName, int count, ...)
{
	cvar_t* cvar = Cvar_FindVar(cvarName);
	if(cvar == NULL)
	{
		return;
	}

#if defined(_DEBUG)
	if(Q_stricmp(cvarName, "r_guiFont"))
	{
		Q_assert(count == cvar->validator.i.max - cvar->validator.i.min + 1);
	}
#endif

	const int oldValue = cvar->latchedString != NULL ? atoi(cvar->latchedString) : cvar->integer;

	const char* previewString = NULL;
	va_list args;
	va_start(args, count);
	for(int i = 0; i < count; ++i)
	{
		const char* value = va_arg(args, const char*);
		if(oldValue == cvar->validator.i.min + i)
		{
			previewString = value;
			break;
		}
	}
	va_end(args);
	Q_assert(previewString != NULL);

	ImGui::SetNextItemWidth(ImGui::GetIO().FontDefault->FontSize * 15.0f);
	if(ImGui::BeginCombo(va("##%s", cvar->name), previewString, ImGuiComboFlags_None))
	{
		va_start(args, count);
		for(int i = 0; i < count; ++i)
		{
			const char* value = va_arg(args, const char*);
			bool selected;
			if(ImGui::Selectable(value, &selected, ImGuiSelectableFlags_None))
			{
				Cvar_Set2(cvar->name, va("%d", cvar->validator.i.min + i), 0);
			}
		}
		va_end(args);

		ImGui::EndCombo();
	}
}

#if 0
static void DrawCVarValueStringCombo(const char* cvarName, int count, ...)
{
	cvar_t* cvar = Cvar_FindVar(cvarName);
	if(cvar == NULL)
	{
		return;
	}

	const char* const currValue = cvar->latchedString != NULL ? cvar->latchedString : cvar->string;

	const char* previewString = NULL;
	va_list args;
	va_start(args, count);
	for(int i = 0; i < count; ++i)
	{
		const char* value = va_arg(args, const char*);
		const char* title = va_arg(args, const char*);
		if(strcmp(currValue, value) == 0)
		{
			previewString = title;
			break;
		}
	}
	va_end(args);

	if(previewString == NULL)
	{
		previewString = "???";
	}

	ImGui::SetNextItemWidth(ImGui::GetIO().FontDefault->FontSize * 15.0f);
	if(ImGui::BeginCombo(va("##%s", cvar->name), previewString, ImGuiComboFlags_None))
	{
		va_start(args, count);
		for(int i = 0; i < count; ++i)
		{
			const char* value = va_arg(args, const char*);
			const char* title = va_arg(args, const char*);
			bool selected;
			if(ImGui::Selectable(title, &selected, ImGuiSelectableFlags_None))
			{
				Cvar_Set2(cvar->name, value, 0);
			}
		}
		va_end(args);

		ImGui::EndCombo();
	}
}

static void DrawCVarValueBitmask(const char* cvarName, int count, ...)
{
	cvar_t* cvar = Cvar_FindVar(cvarName);
	if(cvar == NULL)
	{
		return;
	}

	Q_assert(cvar->validator.i.min == 0);
	Q_assert(IsPowerOfTwo(cvar->validator.i.max + 1));
	Q_assert(count == log2(cvar->validator.i.max + 1));

	ImGui::TableSetColumnIndex(1);

	const int oldValue = cvar->latchedString != NULL ? atoi(cvar->latchedString) : cvar->integer;

	va_list args;
	va_start(args, count);
	for(int i = 0; i < count; ++i)
	{
		const char* title = va_arg(args, const char*);
		bool bitValue = (oldValue & (1 << i)) != 0;
		if(ImGui::Checkbox(title, &bitValue))
		{
			int newValue = oldValue;
			if(bitValue)
			{
				newValue |= (1 << i);
			}
			else
			{
				newValue &= ~(1 << i);
			}
			Cvar_Set2(cvar->name, va("%d", newValue), 0);
		}
	}
	va_end(args);
}
#endif

static void DrawFontSelection()
{
#if 0
	ImGuiIO& io = ImGui::GetIO();
	ImFont* currentFont = ImGui::GetFont();
	if(ImGui::BeginCombo("Font Selection", currentFont->GetDebugName()))
	{
		for(int i = 0; i < io.Fonts->Fonts.Size; ++i)
		{
			ImFont* font = io.Fonts->Fonts[i];
			ImGui::PushID((void*)font);
			if(ImGui::Selectable(font->GetDebugName(), font == currentFont))
			{
				io.FontDefault = font;
			}
			ImGui::PopID();
		}

		ImGui::EndCombo();
	}
#endif

	ImGui::AlignTextToFramePadding();
	ImGui::Text("Font");
	ImGui::SameLine();
	const char* customFontName = NULL;
	if(CL_IMGUI_IsCustomFontLoaded(&customFontName))
	{
		DrawCVarValueCombo("r_guiFont", 3,
			"Proggy Clean (13px)",
			"Sweet16 Mono (16px)",
			customFontName);
	}
	else
	{
		DrawCVarValueCombo("r_guiFont", 2,
			"Proggy Clean (13px)",
			"Sweet16 Mono (16px)");
	}
	const int fontIndex = Cvar_VariableIntegerValue("r_guiFont");
	ImGuiIO& io = ImGui::GetIO();
	if(fontIndex >= 0 && fontIndex < io.Fonts->Fonts.Size)
	{
		io.FontDefault = io.Fonts->Fonts[fontIndex];
	}
}

static void SetTableColumns()
{
	ImGui::TableSetupColumn("Name", ImGuiTableColumnFlags_WidthFixed, ImGui::GetFont()->FontSize * 16.0f);
	ImGui::TableSetupColumn("Value", ImGuiTableColumnFlags_WidthFixed, ImGui::GetFont()->FontSize * 20.0f);
	ImGui::TableSetupColumn("Reset");
	ImGui::TableSetupColumn("Latched");
	ImGui::TableSetupColumn("Description", ImGuiTableColumnFlags_WidthFixed, ImGui::GetFont()->FontSize * 32.0f);
	ImGui::TableHeadersRow();
}

static void DrawVideoRestart(bool restartNeeded)
{
	if(ImGui::Button("Video restart"))
	{
		Cbuf_AddText("vid_restart\n");
	}
	if(restartNeeded)
	{
		ImGui::SameLine();
		ImGui::Text("Some pending changes need a restart to take effect.");
	}
}

static bool DrawCVarTable(bool* restartNeeded, const char* title, int categoryMask)
{
	if(BeginTable(title, 5))
	{
		SetTableColumns();

		for(cvar_t* var = Cvar_GetFirst(); var != NULL; var = var->next)
		{
#if !defined(_DEBUG)
			if(var->gui.categories & CVARCAT_DEBUGGING)
			{
				continue;
			}
#endif

			if(var->gui.categories & categoryMask)
			{
				if(cvarFilter[0] != '\0')
				{
					char filter[256];
					Com_sprintf(filter, sizeof(filter), cvarFilter[0] == '*' ? "%s" : "*%s", cvarFilter);
					const cvarGui_t* const gui = &var->gui;
					const bool matchName = !!Com_Filter(filter, var->name);
					const bool matchTitle = gui->title != NULL && !!Com_Filter(filter, gui->title);
					const bool matchDesc = gui->desc != NULL && !!Com_Filter(filter, gui->desc);
					const bool matchHelp = gui->help != NULL && !!Com_Filter(filter, gui->help);
					bool matchValues = false;
					for(int v = 0; v < gui->numValues; ++v)
					{
						const cvarGuiValue_t* const value = &gui->values[v];
						if(!!Com_Filter(filter, value->title) ||
							!!Com_Filter(filter, value->desc))
						{
							matchValues = true;
							break;
						}
					}
					if(!matchName && !matchTitle && !matchDesc && !matchHelp && !matchValues)
					{
						continue;
					}
				}

				DrawCVar(var->name);
				if(var->latchedString != NULL)
				{
					*restartNeeded = true;
				}
			}
		}

		ImGui::EndTable();
	}

	return restartNeeded;
}

static void DrawSettings()
{
	static float opacity = 1.0f;
	ImVec4 bgColor = ImGui::GetStyleColorVec4(ImGuiCol_WindowBg);
	bgColor.w *= opacity;
	ImGui::PushStyleColor(ImGuiCol_WindowBg, bgColor);

	static bool active = false;
	static bool restartNeeded = false;
	ToggleBooleanWithShortcut(active, ImGuiKey_O, ShortcutOptions::Global);
	GUI_AddMainMenuItem(GUI_MainMenu::Tools, "Client Settings", "Ctrl+Shift+O", &active);
	if(active)
	{
		const ImGuiWindowFlags flags =
			ImGuiWindowFlags_AlwaysAutoResize |
			(restartNeeded ? ImGuiWindowFlags_UnsavedDocument : 0);
		restartNeeded = false;
		if(ImGui::Begin("Client Settings", &active, flags))
		{
			DrawFontSelection();
			ImGui::SliderFloat("Dialog opacity", &opacity, 0.5f, 1.0f, "%g", ImGuiSliderFlags_AlwaysClamp);

			ImGui::NewLine();
			DrawFilter(cvarFilter, sizeof(cvarFilter));
			ImGui::NewLine();

			if(ImGui::BeginTabBar("Tabs#ClientSettings"))
			{
				if(ImGui::BeginTabItem("All"))
				{
					DrawCVarTable(&restartNeeded, "All settings", -1);
					ImGui::EndTabItem();
				}
				if(ImGui::BeginTabItem("General"))
				{
					DrawCVarTable(&restartNeeded, "General settings", CVARCAT_GENERAL);
					ImGui::EndTabItem();
				}
				if(ImGui::BeginTabItem("Gameplay"))
				{
					DrawCVarTable(&restartNeeded, "Gameplay settings", CVARCAT_GAMEPLAY);
					ImGui::EndTabItem();
				}
				if(ImGui::BeginTabItem("HUD"))
				{
					DrawCVarTable(&restartNeeded, "HUD settings", CVARCAT_HUD);
					ImGui::EndTabItem();
				}
				if(ImGui::BeginTabItem("Video"))
				{
					DrawCVarTable(&restartNeeded, "Display settings", CVARCAT_DISPLAY);
					DrawCVarTable(&restartNeeded, "Graphics settings", CVARCAT_GRAPHICS);
					ImGui::EndTabItem();
				}
				if(ImGui::BeginTabItem("Audio"))
				{
					DrawCVarTable(&restartNeeded, "Sound settings", CVARCAT_SOUND);
					ImGui::EndTabItem();
				}
				if(ImGui::BeginTabItem("Input"))
				{
					DrawCVarTable(&restartNeeded, "Input settings", CVARCAT_INPUT);
					ImGui::EndTabItem();
				}
				if(ImGui::BeginTabItem("Network"))
				{
					DrawCVarTable(&restartNeeded, "Network and client-side prediction settings", CVARCAT_NETWORK);
					ImGui::EndTabItem();
				}
				if(ImGui::BeginTabItem("Console"))
				{
					DrawCVarTable(&restartNeeded, "Console settings", CVARCAT_CONSOLE);
					ImGui::EndTabItem();
				}
				if(ImGui::BeginTabItem("Demo"))
				{
					DrawCVarTable(&restartNeeded, "Demo playback settings", CVARCAT_DEMO);
					ImGui::EndTabItem();
				}
				if(ImGui::BeginTabItem("Performance"))
				{
					DrawCVarTable(&restartNeeded, "Performance settings", CVARCAT_PERFORMANCE);
					ImGui::EndTabItem();
				}
				ImGui::EndTabBar();
			}

			DrawVideoRestart(restartNeeded);
		}

		ImGui::End();
	}

	ImGui::PopStyleColor(1);
}

void R_DrawGUI()
{
	DrawImageList();
	DrawImageWindow();
	DrawShaderList();
	DrawShaderWindow();
	DrawShaderEdit();
	DrawFrontEndStats();
	DrawSettings();
}

void R_ShutDownGUI()
{
	// this is necessary to avoid crashes in the detail windows
	// following a map change or video restart:
	// the renderer is shut down and the pointers become stale
	CloseShaderEdit();
	memset(&imageWindow, 0, sizeof(imageWindow));
	memset(&shaderWindow, 0, sizeof(shaderWindow));
	memset(&shaderEditWindow, 0, sizeof(shaderEditWindow));
	ClearShaderReplacements();
	ClearImageReplacements();
}

void RE_DrawMainMenuBarInfo()
{
	static uint32_t frameTimes[16];
	static uint32_t frameCount = 0;
	static int displayedFPS = 0;

	// frame times can be 0 in some cases
	if(rhie.presentToPresentUS > 0)
	{
		frameTimes[frameCount++] = rhie.presentToPresentUS;
	}
	else
	{
		frameCount = 0;
	}
	if(frameCount == ARRAY_LEN(frameTimes))
	{
		uint32_t sum = 0;
		for(uint32_t i = 0; i < ARRAY_LEN(frameTimes); ++i)
		{
			sum += frameTimes[i];
		}
		sum /= ARRAY_LEN(frameTimes);
		displayedFPS = (int)((1000000 + (sum >> 1)) / sum);
		frameCount = 0;
	}

	const char* const pipeline = r_pipeline->integer == PIPELINE_CINEMATIC ? "CRP" : "GRP";
	const char* const info = va(
		"%s | %s | %s | %3d FPS",
		pipeline, rhiInfo.adapter, Com_FormatBytes(rhiInfo.allocatedByteCount), displayedFPS);
	const float offset = ImGui::GetWindowWidth() - ImGui::CalcTextSize("___").x - ImGui::CalcTextSize(info).x;
	ImGui::SameLine(offset);
	ImGui::Text(info);
}
