/*
===========================================================================
Copyright (C) 2023 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// mip-map generation: 8-tap 1D filter


#include "../common/mip_gen.hlsli"


cbuffer RootConstants
{
	float4 weights;
	int2 maxSize;
	int2 scale;
	int2 offset;
	uint clampMode; // 0 = repeat
	uint srcMip;
	uint dstMip;
}

RWTexture2D<float4> mips[2] : register(u0);

[numthreads(8, 8, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture2D<float4> src = mips[srcMip];
	RWTexture2D<float4> dst = mips[dstMip];
	MipGen_DownSample(dst, src, id, maxSize, clampMode, scale, offset, weights);
}
