/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// Im3d integration


#include "../common/im3d.hlsli"


#if VERTEX_SHADER

cbuffer RootConstants
{
	matrix viewMatrix;
	matrix projectionMatrix;
	float2 viewport;
	uint vertexOffset;
};

StructuredBuffer<Vertex> vertexBuffer : register(t0, space1);

VOut vs(uint vertexId : SV_VertexID)
{
	Im3dVertexShaderInput input;
	input.viewMatrix = viewMatrix;
	input.projectionMatrix = projectionMatrix;
	input.viewport = viewport;
	input.vertexOffset = vertexOffset;
	input.vertexBuffer = vertexBuffer;
	input.vertexId = vertexId;

	return VertexShader(input);
}

#endif


#if PIXEL_SHADER

float4 ps(VOut input) : SV_Target
{
	return PixelShader(input);
}

#endif
