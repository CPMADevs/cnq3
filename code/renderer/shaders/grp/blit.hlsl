/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// blit shader - unlike texture copies, it doesn't care about the specific formats used


#include "shared.hlsli"


struct VOut
{
	float4 position : SV_Position;
	float2 texCoords : TEXCOORD0;
};


#if VERTEX_SHADER

cbuffer RootConstants
{
	float scaleX;
	float scaleY;
};

VOut vs(uint id : SV_VertexID)
{
	VOut output;
	output.position.x = scaleX * ((float)(id / 2) * 4.0 - 1.0);
	output.position.y = scaleY * ((float)(id % 2) * 4.0 - 1.0);
	output.position.z = 0.0;
	output.position.w = 1.0;
	output.texCoords.x = (float)(id / 2) * 2.0;
	output.texCoords.y = 1.0 - (float)(id % 2) * 2.0;

	return output;
}

#endif


#if PIXEL_SHADER

Texture2D texture0 : register(t0);
SamplerState sampler0 : register(s0);

float4 ps(VOut input) : SV_Target
{
	float3 color = texture0.Sample(sampler0, input.texCoords).rgb;
	float4 result = float4(color, 1.0);

	return result;
}

#endif
