/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: accumulates in-scattered light from a local point light


#include "common.hlsli"
#include "scene_view.h.hlsli"
#include "raytracing.h.hlsli"


cbuffer RootConstants
{
	DynamicLight light;
	uint materialTextureAIndex;
	uint materialTextureBIndex;
	uint scatterExtTextureIndex;
	uint transmittanceTextureIndex;
	uint transmittanceSamplerIndex;
	float shadowWorldScale;
}

[numthreads(4, 4, 4)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<float4> scatterExtTexture = ResourceDescriptorHeap[scatterExtTextureIndex];
	uint3 textureSize = GetTextureSize(scatterExtTexture);
	if(any(id >= textureSize))
	{
		return;
	}

	RWTexture3D<float4> materialTextureA = ResourceDescriptorHeap[materialTextureAIndex];
	RWTexture3D<float4> materialTextureB = ResourceDescriptorHeap[materialTextureBIndex];
	Texture3D<float> transmittanceTexture = ResourceDescriptorHeap[transmittanceTextureIndex];
	SamplerState transmittanceSampler = SamplerDescriptorHeap[transmittanceSamplerIndex];
	SceneView scene = GetSceneView();

	float3 froxelPosition = scene.FroxelIndexToWorldSpace(id, float3(textureSize));
	float3 lightPosition = light.position;
	float dist = distance(froxelPosition, lightPosition);
	float radius = light.radius;
	if(dist >= radius)
	{
		return;
	}

	float3 scattering = materialTextureA[id].rgb;
	float anisotropy = materialTextureB[id].a;
	RTAS rtas = ResourceDescriptorHeap[scene.tlasBufferIndex];
	float3 lightDir = normalize(lightPosition - froxelPosition);
	float vis = TraceVisibilityWithoutAT(rtas, froxelPosition, lightDir, dist);
	float3 shadowTC = AABoxWorldSpaceToTC(froxelPosition, lightPosition, GetTextureSize(transmittanceTexture), shadowWorldScale);
	float trans = transmittanceTexture.SampleLevel(transmittanceSampler, shadowTC, 0);
	float intensity = saturate(1.0 - dist / radius);
	float3 lightRaw = light.color * intensity * scene.pointLightIntensityVL;
	float2 froxelTC = (float2(id.xy) + float2(0.5, 0.5)) / float2(textureSize.xy);
	float2 froxelNDC = TCToNDC(froxelTC);
	float3 cameraRay = scene.CamerayRay(froxelNDC);
	float cosTheta = dot(-lightDir, -cameraRay);
	float phase = HenyeyGreenstein(cosTheta, anisotropy);
	float3 inScattering = vis * lightRaw * trans * scattering * phase;

	scatterExtTexture[id].rgb += inScattering;
}
