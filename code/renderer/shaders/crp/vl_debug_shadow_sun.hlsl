/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: visualize the sun shadow volume as little cubes


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	float3 color;
	float worldScale;
	float3 cameraPosition;
	float boxScale;
	uint shadowTextureIndex;
}

struct VOut
{
	float4 position : SV_Position;
	float3 positionInCube : POSITIONINCUBE;
	int3 voxelIndex : VOXELINDEX;
};

VOut vs(uint vertexId : SV_VertexID)
{
	RWTexture3D<float> shadowTexture = ResourceDescriptorHeap[shadowTextureIndex];
	SceneView scene = GetSceneView();

	uint3 shadowSize = GetTextureSize(shadowTexture);
	uint flatVoxelIndex = vertexId / 36;
	uint vertexIndex = vertexId % 36;
	int3 voxelIndex = int3(UnflattenIndex(flatVoxelIndex, shadowSize));
	float3 voxelCenterSS = AABoxIndexToWorldSpace(voxelIndex, cameraPosition, float3(shadowSize), worldScale);
	float3 positionInCube = CubeFromVertexID(vertexIndex);
	float3 positionSS = voxelCenterSS + 0.5 * boxScale * worldScale * positionInCube;
	float3 positionWS = cameraPosition + mul(scene.sunToZMatrix, positionSS - cameraPosition);
	float4 positionVS = mul(scene.viewMatrix, float4(positionWS, 1));
	float4 position = mul(scene.projectionMatrix, positionVS);

	VOut output;
	output.position = position;
	output.voxelIndex = voxelIndex;
	output.positionInCube = positionInCube;

	return output;
}

float4 ps(VOut input) : SV_Target
{
	RWTexture3D<float> shadowTexture = ResourceDescriptorHeap[shadowTextureIndex];

	float transmittance = saturate(shadowTexture[input.voxelIndex]);
	if(transmittance == 1.0)
	{
		discard;
	}

	float threshold = 0.9;
	float3 position = abs(input.positionInCube);
	bool3 limits = position >= float3(threshold, threshold, threshold);
	float edge = 0.0;
	if(dot(uint3(limits), uint3(1, 1, 1)) >= 2)
	{
		edge = 1.0;
	}
	float4 result = float4(saturate(0.5 * color * transmittance + edge.xxx), 1);

	return result;
}
