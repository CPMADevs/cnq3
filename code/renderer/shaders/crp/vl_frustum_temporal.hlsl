/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: temporal reprojection


#include "common.hlsli"
#include "scene_view.h.hlsli"


#if defined(TYPE_FLOAT4)
typedef float4 Type;
#elif defined(TYPE_FLOAT)
typedef float Type;
#else
#pragma message "define TYPE_FLOAT4 or TYPE_FLOAT"
#endif


cbuffer RootConstants
{
	uint currTextureIndex;
	uint prevTextureIndex;
	uint prevTextureSamplerIndex;
	float alpha;
}

[numthreads(4, 4, 4)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<Type> currTexture = ResourceDescriptorHeap[currTextureIndex];
	uint3 textureSize = GetTextureSize(currTexture);
	if(any(id >= textureSize))
	{
		return;
	}

	SceneView scene = GetSceneView();
	Texture3D<Type> prevTexture = ResourceDescriptorHeap[prevTextureIndex];
	SamplerState prevTextureSampler = SamplerDescriptorHeap[prevTextureSamplerIndex];
	
	float3 tc = scene.FroxelReproject01(id, float3(textureSize));
	Type currValue = currTexture[id];
	float3 halfPixelSize = float3(0.5, 0.5, 0.5) / float3(textureSize);
	if(IsInRange(tc, halfPixelSize, float3(1, 1, 1) - halfPixelSize))
	{
		Type prevValue = prevTexture.SampleLevel(prevTextureSampler, tc, 0);
		Type finalValue = lerp(currValue, prevValue, alpha);
		if(any(finalValue != currValue))
		{
			currTexture[id] = finalValue;
		}
	}
}
