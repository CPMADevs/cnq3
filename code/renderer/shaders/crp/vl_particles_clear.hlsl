/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting particles: clear all froxel tiles and global counters


#include "common.hlsli"
#include "vl_common.h.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint counterBufferIndex;
	uint tileBufferIndex;
	uint tileCount;
}

[numthreads(64, 1, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	if(id.x >= tileCount)
	{
		return;
	}

	RWStructuredBuffer<Tile> tileBuffer= ResourceDescriptorHeap[tileBufferIndex];
	if(id.x == 0)
	{
		RWStructuredBuffer<Counters> counterBuffer = ResourceDescriptorHeap[counterBufferIndex];
		Counters counters;
		counters.particleCount = 0;
		counters.tileCount = 0;
		counterBuffer[0] = counters;
	}
	Tile tile;
	tile.firstParticle = 0;
	tile.particleCount = 0;
	tile.particleIndex = 0;
	tile.pad0 = 0;
	tileBuffer[id.x] = tile;
}
