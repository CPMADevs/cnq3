/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: opaque sunlight visibility


#include "common.hlsli"
#include "scene_view.h.hlsli"
#include "raytracing.h.hlsli"


cbuffer RootConstants
{
	float3 jitter;
	uint visTextureIndex;
	uint frustumVisTextureIndex;
	uint depthMip;
}

[numthreads(4, 4, 4)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<float> visTexture = ResourceDescriptorHeap[visTextureIndex];
	uint3 textureSize = GetTextureSize(visTexture);
	if(any(id >= textureSize))
	{
		return;
	}

	RWTexture2D<uint> frustumVisTexture = ResourceDescriptorHeap[frustumVisTextureIndex];
	uint furthestVisibleFroxelZIndex = frustumVisTexture[id.xy];
	if(id.z > furthestVisibleFroxelZIndex)
	{
		// this helps fix issues like dark spots around opaque geometry set against the skybox
		visTexture[id] = 0.0;
		return;
	}

	SceneView scene = GetSceneView();
	RTAS rtas = ResourceDescriptorHeap[scene.tlasBufferIndex];

	float2 tc = (float2(id.xy) + float2(0.5, 0.5)) / float2(textureSize.xy);
	float2 ndc = TCToNDC(tc);
	float3 cameraRay = scene.CamerayRay(ndc);
	float3 froxelPosition =
		scene.FroxelIndexToWorldSpace(id, float3(textureSize)) +
		jitter.x * scene.cameraLeft +
		jitter.y * scene.cameraUp +
		jitter.z * cameraRay;
	float vis = TraceVisibilityWithoutAT(rtas, froxelPosition, scene.sunDirection, 10000.0);

	visTexture[id] = vis;
}
