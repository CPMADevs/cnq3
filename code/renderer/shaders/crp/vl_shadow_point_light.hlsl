/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: point light shadow volume


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	float3 lightPosition;
	float extinctionWorldScale;
	float shadowWorldScale;
	uint shadowTextureIndex;
}

[numthreads(4, 4, 4)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<float> shadowTexture = ResourceDescriptorHeap[shadowTextureIndex];
	uint3 shadowSize = GetTextureSize(shadowTexture);
	if(any(id >= shadowSize))
	{
		return;
	}

	SceneView scene = GetSceneView();
	float3 voxelPosition = AABoxIndexToWorldSpace(id, lightPosition, shadowSize, shadowWorldScale);
	float dist = distance(voxelPosition, lightPosition);
	float stepDist = extinctionWorldScale;
	uint stepCount = uint(dist / stepDist);
	float3 step = normalize(lightPosition - voxelPosition) * stepDist;
	ExtinctionCascade cascade = scene.GetExtinctionCascade(stepDist);

	float transmittance = 1.0;
	float3 extinctionPositionWS = voxelPosition;
	for(uint i = 0; i < stepCount; i++)
	{
		float extinction = cascade.ExtinctionAt(extinctionPositionWS);
		float trans = Transmittance(stepDist, extinction);
		transmittance *= saturate(trans);
		extinctionPositionWS += step;
	}

	shadowTexture[id] = transmittance;
}
