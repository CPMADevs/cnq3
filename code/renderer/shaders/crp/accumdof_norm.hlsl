/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// accumulation depth of field: normalization pass


#include "common.hlsli"
#include "fullscreen.hlsli"


cbuffer RootConstants
{
	uint textureIndex;
};

float4 ps(VOut input) : SV_Target
{
	Texture2D texture0 = ResourceDescriptorHeap[textureIndex];

	int2 tc = int2(input.position.xy);
	float4 sum = texture0.Load(int3(tc.x, tc.y, 0));
	float3 color = saturate(sum.rgb / sum.a);
	float4 result = float4(color, 1);

	return result;
}
