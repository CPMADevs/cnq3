/*
===========================================================================
Copyright (C) 2023-2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// shared structures and constants used to implement order-independent transparency


#pragma once


#if defined(__cplusplus)
	#pragma pack(push, 4)
	typedef uint32_t uint;
#endif

#define OIT_MAX_FRAGMENTS_PER_PIXEL 32
#define OIT_AVG_FRAGMENTS_PER_PIXEL 16

struct OIT_Counter
{
	uint fragmentCount;
	uint maxFragmentCount;
	uint overflowCount;
};

struct OIT_Fragment
{
	uint color;
	float depth; // higher is further away from the camera
	uint stateBits; // GLS_* stage bits + stage index
	uint next;
	uint shaderTrace; // shader index: 14 - enable: 1
	uint depthFadeDistOffset; // offset: fp16 - distance: fp16
	uint depthFadeScaleBiasPO; // polygon offset: 1 - enable: 1 - color bias: 4 - color scale: 4
	// @TODO: move the 10 bits from depthFadeScaleBiasPO into shaderTrace
};

#if defined(__cplusplus)
	#pragma pack(pop)
	static_assert(sizeof(OIT_Counter) == 12, "sizeof(OIT_Counter) is wrong");
	static_assert(sizeof(OIT_Fragment) == 28, "sizeof(OIT_Fragment) is wrong");
#endif
