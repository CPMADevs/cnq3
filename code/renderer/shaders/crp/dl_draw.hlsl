/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// direct lighting from dynamic lights


#include "common.hlsli"
#include "fullscreen.hlsli"
#include "raytracing.h.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	DynamicLight light;
	uint blueNoiseTextureIndex;
};

float2 MapSquareToDisk(float2 square01)
{
	float radius = sqrt(square01.x);
	float angle = square01.y * 2.0 * 3.14159265359;
	float2 sinCos;
	sincos(angle, sinCos.x, sinCos.y);
	float2 result = radius * sinCos;
	
	return result;
}

float3 GetRayDirectionForSphereLight(float2 square01, float3 surfacePos, float3 lightPos, float worldRadius)
{
	float3 direction = normalize(lightPos - surfacePos);
	float  radius = worldRadius / length(lightPos - surfacePos);
	float2 pointInDisk = MapSquareToDisk(square01) * radius;
	float3 tangent = normalize(cross(direction, float3(0, 1, 0)));
	float3 bitangent = normalize(cross(tangent, direction));
	float3 result = normalize(direction + pointInDisk.x * tangent + pointInDisk.y * bitangent);

	return result;
}

float4 ps(VOut input) : SV_Target
{
	SceneView scene = GetSceneView();
	Texture2D shadingPositionTexture = ResourceDescriptorHeap[scene.shadingPositionTextureIndex];
	uint3 tc = uint3(input.position.xy, 0);
	float3 positionWS = shadingPositionTexture.Load(tc).xyz;
	float3 lightPosition = light.position;
	float dist = distance(positionWS, lightPosition);
	float radius = light.radius;
	if(dist >= radius)
	{
		return float4(0, 0, 0, 0);
	}

	RTAS rtas = ResourceDescriptorHeap[scene.tlasBufferIndex];
	Texture2D<float2> normalTexture = ResourceDescriptorHeap[scene.normalTextureIndex];
	StructuredBuffer<TLASInstance> tlasInstanceBuffer = ResourceDescriptorHeap[scene.tlasInstanceBufferIndex];
	Texture2D blueNoiseTexture = ResourceDescriptorHeap[blueNoiseTextureIndex];

	uint2 blueNoiseTextureSize = GetTextureSize(blueNoiseTexture);
	float3 normalWS = normalize(OctDecode(normalTexture.Load(tc)));
	float innerRadius = radius / 100.0;
	float intensity = saturate(1.0 - dist / radius);
	float3 lightDir = normalize(lightPosition - positionWS);
	float3 lightRaw = light.color * intensity * max(dot(normalWS, lightDir), 0.0);
	const uint SampleCount = 4;

	float3 lightAccum = float3(0, 0, 0);
	float error = 0.0;
	for(uint r = 0; r < SampleCount; r++)
	{
		float3 light = lightRaw;
		uint2 pos = uint2(input.position.xy) + uint2(r * 17, r * 13 + 7);
		uint2 tc = pos % blueNoiseTextureSize;
		float2 square01 = blueNoiseTexture.Load(uint3(tc, 0)).xy;
		float3 dir = GetRayDirectionForSphereLight(square01, positionWS, lightPosition, innerRadius);
		float t;
		float vis = TraceVisibilityWithATt(t, rtas, tlasInstanceBuffer, positionWS, dir, dist);
		error = max(error, t / radius);
		lightAccum += light * vis;
	}
	lightAccum /= float(SampleCount);

	float4 result = float4(lightAccum, saturate(error));

	return result;
}
