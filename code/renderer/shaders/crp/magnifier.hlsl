/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// magnifier for pixel peeping


#include "common.hlsli"
#include "fullscreen.hlsli"


cbuffer RootConstants
{
	uint colorTextureIndex;
	int cursorIndexX;
	int cursorIndexY;
	int magnifierWidth;
	int magnifierScale;
	int edgeWidth;
	uint edgeColor;
};

float4 ps(VOut input) : SV_Target
{
	Texture2D colorTexture = ResourceDescriptorHeap[colorTextureIndex];
	int2 tc = int2(input.position.xy);
	int2 tcCursor = int2(cursorIndexX, cursorIndexY);
	int2 diff = tc - tcCursor;
	int2 dist = abs(diff);
	int distInner = magnifierWidth / 2;
	int distOuter = distInner + edgeWidth;
	if(all(dist <= distInner))
	{
		// we need to map diff values -N..-1 to -1 and 0..N-1 to 0 after division
		// hence the N-1 offset for negative diff values
		int2 negOffset = int2(magnifierScale - 1, magnifierScale - 1);
		diff -= select(diff < int2(0, 0), negOffset, int2(0, 0));
		tc = tcCursor + diff / magnifierScale;
	}
	float3 color = colorTexture.Load(int3(tc.x, tc.y, 0)).rgb;
	if(any(dist > distInner) && all(dist <= distOuter))
	{
		color = UnpackColor(edgeColor).rgb;
	}
	float4 result = float4(color, 1.0);

	return result;
}
