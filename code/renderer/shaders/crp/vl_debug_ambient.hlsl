/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: visualize the bsp light grid as a bunch of raytraced spheres


#include "common.hlsli"
#include "scene_view.h.hlsli"
#include "light_grid.h.hlsli"


cbuffer RootConstants
{
	LightGridRC lightGridRC;
	float sphereScale;
}

struct VOut
{
	float4 position : SV_Position;
	float3 positionWS : POSITIONWS;
	float4 sphere : SPHERE;
	int3 voxelIndex : VOXELINDEX;
};

VOut vs(uint vertexId : SV_VertexID)
{
	SceneView scene = GetSceneView();
	LightGrid lightGrid = GetLightGrid(lightGridRC);

	uint flatVoxelIndex = vertexId / 6;
	uint vertexIndex = vertexId % 6;
	int3 voxelIndex = int3(UnflattenIndex(flatVoxelIndex, lightGrid.textureSize));
	float3 voxelCenter = lightGrid.IndexToWorldSpace(voxelIndex);
	float2 quadPosition = QuadFromVertexID(vertexIndex);
	float radius = 0.5 * sphereScale * min3(lightGrid.worldScale.x, lightGrid.worldScale.y, lightGrid.worldScale.z);
	float3 up = scene.cameraUp;
	float3 forward = normalize(voxelCenter - scene.cameraPosition);
	float3 right = normalize(cross(forward, up));
	up = cross(right, forward);
	float3x3 rotMat = float3x3(right, up, forward);
	float distToSphere = length(scene.cameraPosition - voxelCenter);
	float sinAngle = radius / distToSphere;
	float cosAngle = sqrt(1.0 - sinAngle * sinAngle);
	float tanAngle = sinAngle / cosAngle;
	float quadScale = tanAngle * distToSphere * 2.0;
	float3 positionWS = voxelCenter + quadScale * mul(float3(quadPosition, 0), rotMat);
	float4 positionVS = mul(scene.viewMatrix, float4(positionWS, 1));
	float4 position = mul(scene.projectionMatrix, positionVS);

	VOut output;
	output.position = position;
	output.positionWS = positionWS;
	output.sphere = float4(voxelCenter, radius);
	output.voxelIndex = voxelIndex;

	return output;
}

float4 ps(VOut input) : SV_Target
{
	SceneView scene = GetSceneView();
	LightGrid lightGrid = GetLightGrid(lightGridRC);

	float3 rayDir = normalize(input.positionWS - scene.cameraPosition);
	float t = RaytraceSphere(scene.cameraPosition, rayDir, input.sphere.xyz, input.sphere.w);
	if(t < 0.0)
	{
		discard;
	}

	LightGridSample ambient = lightGrid.SampleAtIndex(input.voxelIndex);
	float3 hitPosition = scene.cameraPosition + rayDir * t;
	float3 normal = normalize(hitPosition - input.sphere.xyz);
	float3 color = ambient.GetAmbientColor(normal, scene.ambientColor, 1, 1);
	float4 result = float4(color * 0.5, 1);

	return result;
}
