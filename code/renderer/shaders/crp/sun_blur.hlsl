/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// sunlight soft shadows


#include "common.hlsli"
#include "fullscreen.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint visibilityTextureIndex;
	uint penumbraTextureIndex;
};

static const int Radius = 7;
static const float PenumbraRadiusWS = 64.0;

float4 ps(VOut input) : SV_Target
{
	SceneView scene = GetSceneView();
	Texture2D shadingPositionTexture = ResourceDescriptorHeap[scene.shadingPositionTextureIndex];
	Texture2D<float2> normalTexture = ResourceDescriptorHeap[scene.normalTextureIndex];
	Texture2D<float> visibilityTexture = ResourceDescriptorHeap[visibilityTextureIndex];
	Texture2D<float> penumbraTexture = ResourceDescriptorHeap[penumbraTextureIndex];
	SunVShadowCascade cascade = scene.GetSunVShadowCascade(scene.sunVShadowWorldScale.x);

	int2 fragTC = int2(input.position.xy);
	uint3 tc = uint3(input.position.xy, 0);
	float3 positionWS = shadingPositionTexture.Load(tc).xyz;
	float3 normalWS = normalize(OctDecode(normalTexture.Load(tc)));
	float penumbraSize = penumbraTexture.Load(tc) * PenumbraRadiusWS;
	int2 textureSize = int2(GetTextureSize(visibilityTexture));

	// run a PCSS-style blocker search
	if(penumbraSize == 0.0)
	{
		float count = 0.0;
		for(int y = -Radius; y <= Radius; y++)
		{
			for(int x = -Radius; x <= Radius; x++)
			{
				int2 TCs2 = fragTC + int2(x, y);
				if(!IsInTexture(TCs2, textureSize))
				{
					continue;
				}

				uint3 TCs = uint3(TCs2, 0);
				float size = penumbraTexture.Load(TCs) * PenumbraRadiusWS;
				penumbraSize += size;
				count += 1.0;
			}
		}

		if(count > 0.0)
		{
			penumbraSize /= count;
		}
	}
	if(penumbraSize == 0.0)
	{
		penumbraSize = PenumbraRadiusWS;
	}

	float visSum = 0.0;
	float weightSum = 0.0;
	for(int y = -Radius; y <= Radius; y++)
	{
		for(int x = -Radius; x <= Radius; x++)
		{
			int2 TCs2 = fragTC + int2(x, y);
			if(!IsInTexture(TCs2, textureSize))
			{
				continue;
			}

			uint3 TCs = uint3(TCs2, 0);
			float Vs = visibilityTexture.Load(TCs);
			float3 Ns = normalize(OctDecode(normalTexture.Load(TCs)));
			float3 Ps = shadingPositionTexture.Load(TCs).xyz;
			float normalWeight = max(dot(normalWS, Ns), 0.0);
			float distanceWeight = max(1.0 - distance(positionWS, Ps) / penumbraSize, 0.0);
			float weight = normalWeight * distanceWeight;
			weightSum += weight;
			visSum += weight * Vs;
		}
	}

	float visOpaque = weightSum > 0.0 ? visSum / weightSum : 0.0;
	float visVolume = cascade.TransmittanceAt(positionWS);
	float vis = visOpaque * visVolume;
	float lambert = max(dot(normalWS, scene.sunDirection), 0.0);
	float3 color = vis * scene.sunColor * scene.sunIntensityDL * lambert;
	float4 result = float4(color, 1);

	return result;
}
