/*
===========================================================================
Copyright (C) 2023-2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// shared alpha test symbols and functions


#pragma once


#define ATEST_NONE    0u
#define ATEST_GT_0    1u
#define ATEST_LT_HALF 2u
#define ATEST_GE_HALF 3u

#if !defined(__cplusplus)

bool FailsAlphaTest(float alpha, uint alphaTest)
{
	if(alphaTest == ATEST_GT_0)
		return alpha == 0.0;
	else if(alphaTest == ATEST_LT_HALF)
		return alpha >= 0.5;
	else if(alphaTest == ATEST_GE_HALF)
		return alpha < 0.5;
	else // ATEST_NONE
		return false;
}

bool PassesAlphaTest(float alpha, uint alphaTest)
{
	return !FailsAlphaTest(alpha, alphaTest);
}

#endif
