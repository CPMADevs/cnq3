/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: inject fog into the extinction volume


#include "common.hlsli"
#include "scene_view.h.hlsli"
#define VOXEL_SUPERSAMPLING_1X
#include "vl_common.h.hlsli"


cbuffer RootConstants
{
	FogVolume fog;
	float time;
	uint extinctionTextureIndex;
	float worldScale;
}

[numthreads(4, 4, 4)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<float> extinctionTexture = ResourceDescriptorHeap[extinctionTextureIndex];
	uint3 textureSize = GetTextureSize(extinctionTexture);
	if(any(id >= textureSize))
	{
		return;
	}

	SceneView scene = GetSceneView();

	float3 textureSizeF = float3(textureSize);
	float3 tcBase = (float3(id) + float3(0.5, 0.5, 0.5)) / textureSizeF;

	float scale = 0.0;
	float counter = 0.0;
	for(int s = 0; s < VoxelSampleCount; s++)
	{
		float3 tcOffset = VoxelSamples[s] / textureSizeF;
		float3 tc = tcBase + tcOffset;
		float3 position = scene.ExtinctionIndexToWorldSpace(id, textureSizeF, worldScale);
		if(fog.IsPointInside(position))
		{
			scale += fog.DensityAt(position, time);
			counter += 1.0;
		}
	}

	if(scale > 0.0 && counter > 0.0)
	{
		scale /= counter;
		float extinction = (Brightness(fog.scatter) + fog.absorption) * scale;
		extinctionTexture[id] += extinction;
	}
}
