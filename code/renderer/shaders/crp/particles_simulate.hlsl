/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// GPU particle system: particle simulation step


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint particleBufferIndex;
	uint liveSrcBufferIndex;
	uint liveDstBufferIndex;
	uint deadBufferIndex;
	uint emitterBufferIndex;
	uint emitterIndex;
}

[numthreads(64, 1, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWStructuredBuffer<ParticleEmitter> emitterBuffer = ResourceDescriptorHeap[emitterBufferIndex];
	if(id.x >= emitterBuffer[emitterIndex].liveCount)
	{
		return;
	}

	RWStructuredBuffer<Particle> particleBuffer = ResourceDescriptorHeap[particleBufferIndex];
	RWStructuredBuffer<uint> liveSrcBuffer = ResourceDescriptorHeap[liveSrcBufferIndex];

	uint firstIndex = emitterBuffer[emitterIndex].firstIndex;
	uint particleIndex = liveSrcBuffer[firstIndex + id.x];
	Particle p = particleBuffer[firstIndex + particleIndex];
	if(p.lifeTime >= emitterBuffer[emitterIndex].maxSeconds)
	{
		RWStructuredBuffer<uint> deadBuffer = ResourceDescriptorHeap[deadBufferIndex];
		uint oldDeadCount;
		InterlockedAdd(emitterBuffer[emitterIndex].deadCount, 1, oldDeadCount);
		deadBuffer[firstIndex + oldDeadCount] = particleIndex;
		return;
	}

	RWStructuredBuffer<uint> liveDstBuffer = ResourceDescriptorHeap[liveDstBufferIndex];
#if 1 // @TODO: insert proper logic here
	float dt = 1.0 / 60.0;
	float t = p.lifeTime / emitterBuffer[emitterIndex].maxSeconds;
	float fadeOut = 1.0 - EaseOutQuad(t);
	float3 velocityNoise = Hash3To3(p.position); // want Curl
	float scatterNoise = Hash2To1(float2(particleIndex * 1337.0, p.lifeTime)); // want Simplex
	float3 color0 = float3(1, 1, 1);
	float3 color1 = float3(0.25, 0.25, 0.25);
	float3 color = lerp(color0, color1, t);
	p.lifeTime += dt;
	p.position += p.velocity * dt;
	p.velocity *= 0.85;
	p.velocity += velocityNoise * (1.0 + 0.077 * 100.0);
	p.velocity += float3(0, 0, 1) * (1.0 + 0.210 * 100.0);
	p.absorption = fadeOut * 0.02;
	p.scattering = fadeOut * (color + float(0.5 + scatterNoise).xxx) * 0.02;
	p.radius = 5.0 + 2.0 * EaseInCubic(t);
#endif
	particleBuffer[firstIndex + particleIndex] = p;

	uint oldLiveCount;
	InterlockedAdd(emitterBuffer[emitterIndex].liveCount2, 1, oldLiveCount);
	liveDstBuffer[firstIndex + oldLiveCount] = particleIndex;
}
