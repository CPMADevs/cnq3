/*
===========================================================================
Copyright (C) 2023 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// post-processing: moves from gamma to linear space


#include "common.hlsli"
#include "fullscreen.hlsli"


cbuffer RootConstants
{
	uint textureIndex;
	uint samplerIndex;
	float gamma;
	float invBrightness;
};

VOut vs(uint id : SV_VertexID)
{
	VOut output;
	output.position = FSTrianglePosFromVertexId(id);
	output.texCoords = FSTriangleTCFromVertexId(id);

	return output;
}

// X3571: pow(f, e) won't work if f is negative
#pragma warning(disable : 3571)

float4 ps(VOut input) : SV_Target
{
	Texture2D texture0 = ResourceDescriptorHeap[textureIndex];
	SamplerState sampler0 = SamplerDescriptorHeap[samplerIndex];
	float3 base = texture0.Sample(sampler0, input.texCoords).rgb;
	float3 linearSpace = pow(base * invBrightness, gamma);
	float4 result = float4(linearSpace, 1.0);

	return result;
}
