/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: normalize the Henyey-Greenstein anisotropy factor g


#include "common.hlsli"


cbuffer RootConstants
{
	uint materialTextureBIndex;
	uint materialTextureCIndex;
}

[numthreads(4, 4, 4)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<float4> materialTextureB = ResourceDescriptorHeap[materialTextureBIndex];
	uint3 textureSize = GetTextureSize(materialTextureB);
	if(any(id >= textureSize))
	{
		return;
	}

	RWTexture3D<float> materialTextureC = ResourceDescriptorHeap[materialTextureCIndex];
	float weightSum = materialTextureC[id];
	if(weightSum > 0.0)
	{
		materialTextureB[id].a /= weightSum;
	}
}
