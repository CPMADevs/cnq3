/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// draws the sun's position


#include "common.hlsli"
#include "fullscreen.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	float3 direction;
	float angle; // @TODO:
	float3 color;
	float padding;
	float2 textureSize;
};

float4 ps(VOut input) : SV_Target
{
	SceneView scene = GetSceneView();
	float3 positionWS = scene.cameraPosition + scene.zFar * direction;
	float4 positionCS = mul(scene.projectionMatrix, mul(scene.viewMatrix, float4(positionWS, 1)));
	float3 positionNDC = positionCS.xyz / positionCS.w;
	if(positionNDC.z < 0.0)
	{
		return float4(0, 0, 0, 0);
	}
	
	float2 positionPx = NDCToTC(positionNDC.xy) * textureSize;
	float2 fragmentPx = input.texCoords * textureSize;
	if(distance(positionPx, fragmentPx) < 12.0)
	{
		return float4(saturate(color), 1);
	}
	if(distance(positionPx, fragmentPx) < 14.0)
	{
		return float4(0, 0, 0, 1);
	}

	return float4(0, 0, 0, 0);
}
