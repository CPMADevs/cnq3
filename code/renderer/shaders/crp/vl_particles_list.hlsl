/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting particles: create final live particle index array


#include "common.hlsli"
#include "vl_common.h.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint3 fullResolution;
	uint emitterIndex;
	uint3 tileResolution;
	uint maxParticleIndexCount;
	uint3 tileScale;
	uint tileBufferIndex;
	uint emitterBufferIndex;
	uint particleBufferIndex;
	uint liveBufferIndex;
	uint indexBufferIndex;
}

[numthreads(64, 1, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWStructuredBuffer<ParticleEmitter> emitterBuffer = ResourceDescriptorHeap[emitterBufferIndex];
	ParticleEmitter emitter = emitterBuffer[emitterIndex];
	if(id.x >= emitter.liveCount2)
	{
		return;
	}

	RWStructuredBuffer<Particle> particleBuffer = ResourceDescriptorHeap[particleBufferIndex];
	RWStructuredBuffer<Tile> tileBuffer = ResourceDescriptorHeap[tileBufferIndex];
	RWStructuredBuffer<uint> liveBuffer = ResourceDescriptorHeap[liveBufferIndex];
	RWStructuredBuffer<uint> indexBuffer = ResourceDescriptorHeap[indexBufferIndex];
	SceneView scene = GetSceneView();

	uint firstIndex = emitter.firstIndex;
	uint particleIndex = liveBuffer[firstIndex + id.x];
	Particle particle = particleBuffer[firstIndex + particleIndex];
	int3 boxMin = particle.froxelMin / int3(tileScale);
	int3 boxMax = particle.froxelMax / int3(tileScale);
	boxMin = max(boxMin, int3(0, 0, 0));
	boxMax = min(boxMax, int3(tileResolution) - int3(1, 1, 1));
	for(int x = boxMin.x; x <= boxMax.x; x++)
	{
		for(int y = boxMin.y; y <= boxMax.y; y++)
		{
			for(int z = boxMin.z; z <= boxMax.z; z++)
			{
				uint3 tileIndex = uint3(x, y, z);
				uint flatTileIndex = FlattenIndex(tileIndex, tileResolution);
				uint particleWriteOffset;
				InterlockedAdd(tileBuffer[flatTileIndex].particleIndex, 1, particleWriteOffset);
				uint particleWriteIndex = tileBuffer[flatTileIndex].firstParticle + particleWriteOffset;
				if(particleWriteOffset < tileBuffer[flatTileIndex].particleCount &&
					particleWriteIndex < maxParticleIndexCount)
				{
					indexBuffer[particleWriteIndex] = firstIndex + particleIndex;
				}
			}
		}
	}
}
