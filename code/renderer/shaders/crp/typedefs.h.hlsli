/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// type definitions shared with C++ code


#pragma once


#if defined(__cplusplus)
	typedef ivec2_t int2;
	typedef ivec3_t int3;
	typedef ivec4_t int4;
	typedef uint32_t uint;
	typedef uvec2_t uint2;
	typedef uvec3_t uint3;
	typedef uvec4_t uint4;
	typedef vec2_t float2;
	typedef vec3_t float3;
	typedef vec4_t float4;
	typedef matrix4x4_t matrix;
	typedef matrix3x3_t float3x3;
	typedef color4ub_t color4ub;
#else
	typedef uint color4ub;
#endif
