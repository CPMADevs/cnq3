/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// fetches and returns direct light data for non-lightmapped surfaces


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	matrix modelViewMatrix;
};

struct VIn
{
	float3 position : POSITION;
};

struct VOut
{
	float4 position : SV_Position;
	float clipDist : SV_ClipDistance0;
};

VOut vs(VIn input)
{
	SceneView scene = GetSceneView();
	float4 positionVS = mul(modelViewMatrix, float4(input.position.xyz, 1));

	VOut output;
	output.position = mul(scene.projectionMatrix, positionVS);
	output.clipDist = dot(positionVS, scene.clipPlane);

	return output;
}

[earlydepthstencil]
float4 ps(VOut input) : SV_Target
{
	SceneView scene = GetSceneView();
	Texture2D lightTexture = ResourceDescriptorHeap[scene.lightTextureIndex];
	Texture2D sunlightTexture = ResourceDescriptorHeap[scene.sunlightTextureIndex];
	
	uint3 tc = uint3(input.position.xy, 0);
	float3 color = lightTexture.Load(tc).rgb + sunlightTexture.Load(tc).rgb;
	float4 result = float4(color, 0);

	return result;
}
