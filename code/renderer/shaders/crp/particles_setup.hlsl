/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// GPU particle system: sets up buffers for 1 emission step and 1 simulation step


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint emitterBufferIndex;
	uint indirectBufferIndex;
	uint emitterIndex;
	uint emitCountRequest;
}

[numthreads(1, 1, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWStructuredBuffer<ParticleEmitter> emitterBuffer = ResourceDescriptorHeap[emitterBufferIndex];
	RWByteAddressBuffer indirectBuffer = ResourceDescriptorHeap[indirectBufferIndex];

	uint deadCount = emitterBuffer[emitterIndex].deadCount;
	uint liveCount = emitterBuffer[emitterIndex].liveCount2;
	uint emitCount = min(deadCount, emitCountRequest);
	indirectBuffer.Store(0, (emitCount + 63) / 64); // emit.x
	indirectBuffer.Store(12, (liveCount + emitCount + 63) / 64); // simulate.x
	emitterBuffer[emitterIndex].liveCount = liveCount;
	emitterBuffer[emitterIndex].liveCount2 = 0;
	emitterBuffer[emitterIndex].emitCount = emitCount;
}
