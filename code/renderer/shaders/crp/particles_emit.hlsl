/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// GPU particle system: particle emission step


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint particleBufferIndex;
	uint liveBufferIndex;
	uint deadBufferIndex;
	uint emitterBufferIndex;
	uint emitterIndex;
}

[numthreads(64, 1, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWStructuredBuffer<ParticleEmitter> emitterBuffer = ResourceDescriptorHeap[emitterBufferIndex];
	uint emitCount = emitterBuffer[emitterIndex].emitCount;
	if(id.x >= emitCount)
	{
		return;
	}

	RWStructuredBuffer<Particle> particleBuffer = ResourceDescriptorHeap[particleBufferIndex];
	RWStructuredBuffer<uint> deadBuffer = ResourceDescriptorHeap[deadBufferIndex];
	RWStructuredBuffer<uint> liveBuffer = ResourceDescriptorHeap[liveBufferIndex];
	SceneView scene = GetSceneView();
	
	uint firstIndex = emitterBuffer[emitterIndex].firstIndex;
	uint oldDeadCount;
	InterlockedAdd(emitterBuffer[emitterIndex].deadCount, -1, oldDeadCount);
	uint newIndex = deadBuffer[firstIndex + oldDeadCount - 1];
	uint oldLiveCount;
	InterlockedAdd(emitterBuffer[emitterIndex].liveCount, 1, oldLiveCount);
	liveBuffer[firstIndex + oldLiveCount] = newIndex;

	Particle p;
#if 1 // @TODO: insert proper logic here
	p.absorption = 1.0;
	p.anisotropy = 0.5;
	p.isEmissive = 0;
	p.position = float3(694, -42, -300);
	p.velocity = (Hash1To3(scene.frameSeed + 17.69 * float(id.x)) * 2.0 - 1.0) * 20.0;
	p.radius = 10.0;
	p.scattering = float3(1.0, 0.5, 0.0);
	p.lifeTime = 0.0;
	p.froxelMin = int3(0, 0, 0);
	p.froxelMax = int3(-1, -1, -1);
#endif
	particleBuffer[firstIndex + newIndex] = p;
}
