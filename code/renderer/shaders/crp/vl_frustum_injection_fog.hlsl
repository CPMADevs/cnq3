/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: inject fog into the material textures


#include "common.hlsli"
#include "scene_view.h.hlsli"
#define VOXEL_SUPERSAMPLING_1X
#include "vl_common.h.hlsli"


cbuffer RootConstants
{
	FogVolume fog;
	float time;
	uint materialTextureAIndex;
	uint materialTextureBIndex;
	uint materialTextureCIndex;
}

[numthreads(4, 4, 4)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<float4> materialTextureA = ResourceDescriptorHeap[materialTextureAIndex];
	uint3 textureSize = GetTextureSize(materialTextureA);
	if(any(id >= textureSize))
	{
		return;
	}

	RWTexture3D<float4> materialTextureB = ResourceDescriptorHeap[materialTextureBIndex];
	RWTexture3D<float> materialTextureC = ResourceDescriptorHeap[materialTextureCIndex];
	SceneView scene = GetSceneView();

	float3 textureSizeF = float3(textureSize);
	float3 tcBase = (float3(id) + float3(0.5, 0.5, 0.5)) / textureSizeF;

	float scale = 0.0;
	float counter = 0.0;
	for(int s = 0; s < VoxelSampleCount; s++)
	{
		float3 tcOffset = VoxelSamples[s] / textureSizeF;
		float3 tc = tcBase + tcOffset;
		float3 position = scene.FroxelTCToWorldSpace(tc, textureSizeF);
		if(fog.IsPointInside(position))
		{
			scale += fog.DensityAt(position, time);
			counter += 1.0;
		}
	}

	if(scale > 0.0 && counter > 0.0)
	{
		scale /= counter;
		materialTextureA[id] += float4(fog.scatter * scale, fog.absorption * scale);
		materialTextureB[id] += float4(fog.emissive * scale, fog.anisotropy);
		materialTextureC[id] += 1.0;
	}
}
