/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// sunlight visibility and penumbra size


#include "common.hlsli"
#include "fullscreen.hlsli"
#include "scene_view.h.hlsli"
#include "raytracing.h.hlsli"


cbuffer RootConstants
{
};

static const float PenumbraRadiusWS = 64.0;

struct POut
{
	float visible : SV_Target0;
	float penumbra : SV_Target1;
};

POut ps(VOut input)
{
	SceneView scene = GetSceneView();
	RTAS rtas = ResourceDescriptorHeap[scene.tlasBufferIndex];
	Texture2D shadingPositionTexture = ResourceDescriptorHeap[scene.shadingPositionTextureIndex];
	StructuredBuffer<TLASInstance> tlasInstanceBuffer = ResourceDescriptorHeap[scene.tlasInstanceBufferIndex];

	uint3 tc = uint3(input.position.xy, 0);
	float3 positionWS = shadingPositionTexture.Load(tc).xyz;
	float t;
	float vis = TraceVisibilityWithATt(t, rtas, tlasInstanceBuffer, positionWS, scene.sunDirection, 10000.0);

	POut output;
	if(vis == 0.0) // in shadow
	{
		output.visible = 0.0;
		output.penumbra = saturate((t * 0.01) / PenumbraRadiusWS);
	}
	else
	{
		output.visible = 1.0;
		output.penumbra = 0.0;
	}

	return output;
}
