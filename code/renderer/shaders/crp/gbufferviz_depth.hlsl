/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// linearizes raw depth buffer values for visualization


#include "common.hlsli"
#include "fullscreen.hlsli"
#include "scene_view.h.hlsli"


float4 ps(VOut input) : SV_Target
{
	SceneView scene = GetSceneView();
	Texture2D<float> depthTexture = ResourceDescriptorHeap[scene.depthTextureIndex];
	uint3 tc = uint3(input.position.xy, 0);
	float depthZW = depthTexture.Load(tc);
	float depth = scene.LinearDepth(depthZW);
	float depth01 = depth / scene.zFar;
	float4 result = float4(depth01.xxx, 1);

	return result;
}
