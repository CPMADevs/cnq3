/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting particles: create compacted froxel tile array and compute per-tile particle offset


#include "common.hlsli"
#include "vl_common.h.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint counterBufferIndex;
	uint tileBufferIndex;
	uint tileIndexBufferIndex;
	uint tileCount;
}

[numthreads(64, 1, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	uint tileIndex = id.x;
	if(tileIndex >= tileCount)
	{
		return;
	}

	RWStructuredBuffer<Tile> tileBuffer = ResourceDescriptorHeap[tileBufferIndex];
	if(tileBuffer[tileIndex].particleCount == 0)
	{
		return;
	}
	
	RWStructuredBuffer<Counters> counterBuffer = ResourceDescriptorHeap[counterBufferIndex];
	RWStructuredBuffer<uint> tileIndexBuffer = ResourceDescriptorHeap[tileIndexBufferIndex];

	InterlockedAdd(counterBuffer[0].particleCount, tileBuffer[tileIndex].particleCount, tileBuffer[tileIndex].firstParticle);
	uint tileIndexIndex;
	InterlockedAdd(counterBuffer[0].tileCount, 1, tileIndexIndex);
	tileIndexBuffer[tileIndexIndex] = tileIndex;
}
