/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: raymarch froxels


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint scatterTextureIndex;
	uint resolveTextureIndex;
	uint materialTextureBIndex;
}

[numthreads(8, 8, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<float4> scatterTexture = ResourceDescriptorHeap[scatterTextureIndex];
	uint3 textureSize = GetTextureSize(scatterTexture);
	if(any(id.xy >= textureSize.xy))
	{
		return;
	}

	// Even if the extinction coefficient is constant all along a given line segment of length Z,
	// the transmittance is different at every point.
	// We compute the final scatter/emissive using an analytical solution like Frostbite does.
	// Integral(T(x) * dx) == Integral(e^(-extinction*x) * dZ) [0 to Z]
	//                     == [e^(-extinction*x) / (-extinction)] [0 to Z]
	//                     == (-1 / extinction) * [e^(-Z*extinction)] [0 to Z]
	//                     == (-1 / extinction) * [T(Z)] [0 to Z]
	//                     == (-1 / extinction) * (T(Z) - T(0))
	//                     == (-1 / extinction) * (T(Z) - 1)
	//                     == (1 / extinction) * (1 - T(Z))
	//                     == (1 - T(Z)) / extinction
	// The scatter/emissive coefficients are considered uniform in each froxel.
	// They are therefore constants that can be pulled out of the integral, hence their omission.

	SceneView scene = GetSceneView();
	RWTexture3D<float4> resolveTexture = ResourceDescriptorHeap[resolveTextureIndex];
	RWTexture3D<float4> materialTextureB = ResourceDescriptorHeap[materialTextureBIndex];
	uint3 index0 = uint3(id.xy, 0);
	float3 tc0 = (float3(index0) + float3(0.5, 0.5, 0)) / textureSize; // near edge of first voxel
	float3 prevPosition = scene.FroxelTCToWorldSpace(tc0, float3(textureSize));
	float3 accumScatter = float3(0, 0, 0);
	float accumTrans = 1.0;
	for(uint d = 0; d < textureSize.z; d++)
	{
		uint3 index = uint3(id.xy, d);
		float3 tc = (float3(index) + float3(0.5, 0.5, 1)) / textureSize; // far edge of current voxel
		float4 froxelScatterExt = scatterTexture[index];
		float3 froxelEmissive = materialTextureB[index].rgb;
		float3 froxelScatter = froxelScatterExt.rgb;
		float froxelExtinction = froxelScatterExt.a;
		float3 currPosition = scene.FroxelTCToWorldSpace(tc, float3(textureSize));
		float depthStep = distance(currPosition, prevPosition);
		float froxelTrans = Transmittance(depthStep, froxelExtinction);
		float froxelTransInteg = (1.0 - froxelTrans) / (froxelExtinction == 0.0 ? 1.0 : froxelExtinction);
		accumScatter += (accumTrans * froxelTransInteg) * (froxelScatter + froxelEmissive);
		accumTrans *= froxelTrans;
		resolveTexture[index] = float4(accumScatter, accumTrans);
		prevPosition = currPosition;
	}
}
