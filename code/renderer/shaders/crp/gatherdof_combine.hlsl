/*
===========================================================================
Copyright (C) 2023-2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// gather depth of field: final blend pass


#include "common.hlsli"
#include "fullscreen.hlsli"
#include "gatherdof.hlsli"


cbuffer RootConstants : register(b0)
{
	uint nearTextureIndex;
	uint farTextureIndex;
	uint nearCocTextureIndex; // @TODO: remove
	uint farCocTextureIndex;
	uint sharpTextureIndex;
	uint samplerIndex; // point/clamp
};

float4 ps(VOut input) : SV_Target
{
	SamplerState samplerState = SamplerDescriptorHeap[samplerIndex];
	Texture2D nearColorTexture = ResourceDescriptorHeap[nearTextureIndex];
	Texture2D farColorTexture = ResourceDescriptorHeap[farTextureIndex];
	Texture2D<float> farCocTexture = ResourceDescriptorHeap[farCocTextureIndex];
	Texture2D sharpTexture = ResourceDescriptorHeap[sharpTextureIndex];

	float4 nearColor = nearColorTexture.Sample(samplerState, input.texCoords);
	float4 farColor = farColorTexture.Sample(samplerState, input.texCoords);
	float nearCoc = saturate(nearColor.a);
	float farCoc = farCocTexture.Sample(samplerState, input.texCoords);
	float4 sharp = sharpTexture.Sample(samplerState, input.texCoords);

	float3 color = lerp(sharp.rgb, farColor.rgb, farCoc);
	color = lerp(color, nearColor.rgb, nearCoc);
	float4 result = float4(color, 1.0);

	return result;
}
