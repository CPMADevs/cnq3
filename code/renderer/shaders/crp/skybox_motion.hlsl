/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// outputs motion vectors for objects infinitely far away


#include "common.hlsli"
#include "fullscreen.hlsli"
#include "scene_view.h.hlsli"


float2 ps(VOut input) : SV_Target
{
	SceneView scene = GetSceneView();

	// we need the position to match for perfect translation invariance
	matrix prevViewMatrix = scene.prevViewMatrix;
	prevViewMatrix[0][3] = scene.viewMatrix[0][3];
	prevViewMatrix[1][3] = scene.viewMatrix[1][3];
	prevViewMatrix[2][3] = scene.viewMatrix[2][3];

	float2 currNDC = TCToNDC(input.texCoords.xy);
	float4 currPointNDC = float4(currNDC, 0, 1);
	float4 pointWSw = mul(scene.invViewMatrix, mul(scene.invProjectionMatrix, currPointNDC));
	float4 pointWS = pointWSw / pointWSw.w;
	float4 prevPointNDC = mul(scene.prevProjectionMatrix, mul(prevViewMatrix, pointWS));
	float2 prevNDC = prevPointNDC.xy / prevPointNDC.w;
	float2 motionTC = NDCToTC(currNDC) - NDCToTC(prevNDC);

	return motionTC;
}
