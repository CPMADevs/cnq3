/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// raytracing structures shared with C++ code


#pragma once


#include "typedefs.h.hlsli"
#if defined(__cplusplus)
#	pragma pack(push, 4)
#endif

struct BLASVertex
{
	float2 texCoords;
	color4ub color;
};

struct BLASMesh
{
	uint firstVertex;
	uint firstIndex;
	uint textureIndex;
	uint samplerIndex;
	uint alphaTestMode;
	uint blendBits;
};

struct TLASInstance
{
	uint vertexBufferIndex;
	uint indexBufferIndex;
	uint meshBufferIndex;
	uint cullMode;
};

#if defined(__cplusplus)
#	pragma pack(pop)
#endif

#if !defined(__cplusplus)

#include "common.hlsli"
#include "alpha_test.h.hlsli"

bool IsTriangleHitOpaque(StructuredBuffer<TLASInstance> tlasInstanceBuffer, uint instanceId, uint meshId, uint triangleId, float2 bary2, bool frontFace)
{
	TLASInstance instance = tlasInstanceBuffer[instanceId];
	StructuredBuffer<BLASMesh> meshBuffer = ResourceDescriptorHeap[instance.meshBufferIndex];
	BLASMesh mesh = meshBuffer[meshId];
	[branch] if(mesh.alphaTestMode == 0)
	{
		return false; // translucent -> ignored
	}

	float3 barycentrics = float3(1.0 - bary2.x - bary2.y, bary2.x, bary2.y);
	StructuredBuffer<BLASVertex> vertexBuffer = ResourceDescriptorHeap[instance.vertexBufferIndex];
	StructuredBuffer<uint> indexBuffer = ResourceDescriptorHeap[instance.indexBufferIndex];
	uint firstIndex = mesh.firstIndex + triangleId * 3;
	uint vtxIdx0 = mesh.firstVertex + indexBuffer[firstIndex + 0];
	uint vtxIdx1 = mesh.firstVertex + indexBuffer[firstIndex + 1];
	uint vtxIdx2 = mesh.firstVertex + indexBuffer[firstIndex + 2];
	BLASVertex v0 = vertexBuffer[vtxIdx0];
	BLASVertex v1 = vertexBuffer[vtxIdx1];
	BLASVertex v2 = vertexBuffer[vtxIdx2];
	float2 texCoords = trilerp(v0.texCoords, v1.texCoords, v2.texCoords, barycentrics);
	float4 vertexColor = trilerp(UnpackColor(v0.color), UnpackColor(v1.color), UnpackColor(v2.color), barycentrics);
	Texture2D texture0 = ResourceDescriptorHeap[mesh.textureIndex];
	SamplerState sampler0 = SamplerDescriptorHeap[mesh.samplerIndex];
	float4 textureColor = texture0.SampleLevel(sampler0, texCoords, 0);
	float4 hitColor = vertexColor * textureColor;
	bool isOpaque = PassesAlphaTest(hitColor.a, mesh.alphaTestMode);

	return isOpaque;
}

float TraceVisibilityWithATt(
	out float t, RTAS rtas, StructuredBuffer<TLASInstance> instBuffer,
	float3 position, float3 direction, float dist)
{
	RayDesc ray;
	ray.Origin = position;
	ray.Direction = direction;
	ray.TMin = 0.0;
	ray.TMax = dist;

	t = 0.0;
	RayQuery<RAY_FLAG_NONE> q;
	q.TraceRayInline(rtas, RAY_FLAG_NONE, 0xFF, ray);
	while(q.Proceed())
	{
		if(q.CandidateType() == CANDIDATE_NON_OPAQUE_TRIANGLE)
		{
			bool isOpaque = IsTriangleHitOpaque(
				instBuffer,
				q.CandidateInstanceIndex(),
				q.CandidateGeometryIndex(),
				q.CandidatePrimitiveIndex(),
				q.CandidateTriangleBarycentrics(),
				q.CandidateTriangleFrontFace());
			if(isOpaque)
			{
				q.CommitNonOpaqueTriangleHit();
			}
		}
	}

	if(q.CommittedStatus() == COMMITTED_TRIANGLE_HIT)
	{
		t = q.CommittedRayT();
		return 0.0;
	}

	return 1.0;
}

float TraceVisibilityWithAT(
	RTAS rtas, StructuredBuffer<TLASInstance> instBuffer,
	float3 position, float3 direction, float dist)
{
	float t;
	return TraceVisibilityWithATt(t, rtas, instBuffer, position, direction, dist);
}

float TraceVisibilityWithoutATt(
	out float t, RTAS rtas,
	float3 position, float3 direction, float dist)
{
	RayDesc ray;
	ray.Origin = position;
	ray.Direction = direction;
	ray.TMin = 0.0;
	ray.TMax = dist;

	t = 0.0;
	RayQuery<RAY_FLAG_CULL_NON_OPAQUE> q;
	q.TraceRayInline(rtas, RAY_FLAG_NONE, 0xFF, ray);
	q.Proceed();
	if(q.CommittedStatus() == COMMITTED_TRIANGLE_HIT)
	{
		t = q.CommittedRayT();
		return 0.0;
	}

	return 1.0;
}

float TraceVisibilityWithoutAT(RTAS rtas, float3 position, float3 direction, float dist)
{
	float t;
	return TraceVisibilityWithoutATt(t, rtas, position, direction, dist);
}

#endif
