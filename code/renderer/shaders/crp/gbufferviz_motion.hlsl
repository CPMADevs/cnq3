/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// colorizes motion vectors for debugging


#include "common.hlsli"
#include "fullscreen.hlsli"


cbuffer RootConstants
{
	uint textureIndex;
};

float4 ps(VOut input) : SV_Target
{
	Texture2D<float2> motionTexture = ResourceDescriptorHeap[textureIndex];
	float2 size = float2(GetTextureSize(motionTexture));
	float scale = size.y / size.x; // correct for aspect ratio
	uint3 tc = uint3(input.position.xy, 0);
	float2 raw = motionTexture.Load(tc);
	float2 display = abs(raw) * 50.0 * scale;
	float4 result = float4(display, 0, 1);

	return result;
}
