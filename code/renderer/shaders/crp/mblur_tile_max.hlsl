/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// motion blur velocity tile dilation


#include "common.hlsli"


cbuffer RootConstants : register(b0)
{
	uint inputTextureIndex;
	uint outputTextureIndex;
	uint samplerIndex; // point/clamp
};

[numthreads(8, 8, 1)]
void cs(uint3 dtid : SV_DispatchThreadID)
{
	uint2 tc = dtid.xy;
	RWTexture2D<float2> outputTexture = ResourceDescriptorHeap[outputTextureIndex];
	uint2 textureSize = GetTextureSize(outputTexture);
	if(any(tc >= textureSize))
	{
		return;
	}

	Texture2D<float2> inputTexture = ResourceDescriptorHeap[inputTextureIndex];
	SamplerState samplerState = SamplerDescriptorHeap[samplerIndex];

	float2 tcShifted = float2(tc) + float2(0.5, 0.5);
	float2 pixelSize = float2(1, 1) / float2(textureSize);
	float2 maxVelocity = float2(0, 0);
	for(int y = -1; y <= 1; y++)
	{
		for(int x = -1; x <= 1; x++)
		{
			float2 tc01 = (tcShifted + float2(x, y)) * pixelSize;
			float2 velocity = inputTexture.SampleLevel(samplerState, tc01, 0);
			maxVelocity = vmax(maxVelocity, velocity);
		}
	}

	outputTexture[tc] = maxVelocity;
}
