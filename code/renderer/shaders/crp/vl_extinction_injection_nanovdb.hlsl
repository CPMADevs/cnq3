/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: inject a NanoVDB volume into the extinction volume


#include "common.hlsli"
#include "scene_view.h.hlsli"
#include "vl_common.h.hlsli"
#include "vl_nanovdb.hlsli"


#define LOW_QUALITY_INJECTION_MODE 1


cbuffer RootConstants
{
	float4 packedTransform[4];
	float2 packedTransform2;
	uint nanovdbBufferIndex;
	uint extinctionTextureIndex;
	uint densityGridByteOffset;
	uint densityGridByteOffset2;
	uint linearInterpolation;
	float worldScale;
	float densityExtinctionScale;
	float t;
}

[numthreads(4, 4, 4)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<float> extinctionTexture = ResourceDescriptorHeap[extinctionTextureIndex];
	uint3 textureSize = GetTextureSize(extinctionTexture);
	if(any(id >= textureSize))
	{
		return;
	}

	pnanovdb_buf_t nanovdbBuffer = ResourceDescriptorHeap[nanovdbBufferIndex];
	SceneView scene = GetSceneView();

	float3 textureSizeF = float3(textureSize);
	float3 tcBase = (float3(id) + float3(0.5, 0.5, 0.5)) / textureSizeF;
	float3 voxelPosition = scene.ExtinctionIndexToWorldSpace(id, textureSizeF, worldScale);
	Transform transform = DecodeTransform(packedTransform, packedTransform2);
#if LOW_QUALITY_INJECTION_MODE
	// big perf boost, tiny visual impact
	transform.stepSize = 0.5 * worldScale.xxx;
#endif

	if(linearInterpolation != 0u)
	{
		SampleResult extResult1 = CreateSampleResult();
		if(densityGridByteOffset)
		{
			Grid grid1 = GetGrid(nanovdbBuffer, densityGridByteOffset);
			if(grid1.OverlapsAxisAlignedBox(scene, id, textureSizeF, worldScale, transform))
			{
				extResult1 = grid1.GetAxisAlignedBoxAverage(scene, voxelPosition, worldScale.xxx, transform);
			}
		}

		SampleResult extResult2 = CreateSampleResult();
		if(densityGridByteOffset2 > 0)
		{
			Grid grid2 = GetGrid(nanovdbBuffer, densityGridByteOffset2);
			if(grid2.OverlapsAxisAlignedBox(scene, id, textureSizeF, worldScale, transform))
			{
				extResult2 = grid2.GetAxisAlignedBoxAverage(scene, voxelPosition, worldScale.xxx, transform);
			}
		}

		if(extResult1.sum > 0.0 || extResult2.sum > 0.0)
		{
			float extinction1 = (extResult1.sum / float(extResult1.maxSampleCount));
			float extinction2 = (extResult2.sum / float(extResult2.maxSampleCount));
			float extinction = lerp(extinction1, extinction2, t) * densityExtinctionScale;
			extinctionTexture[id] += extinction;
		}
	}
	else if(densityGridByteOffset > 0)
	{
		Grid grid = GetGrid(nanovdbBuffer, densityGridByteOffset);
		if(grid.OverlapsAxisAlignedBox(scene, id, textureSizeF, worldScale, transform))
		{
			SampleResult extResult = CreateSampleResult();
			extResult = grid.GetAxisAlignedBoxAverage(scene, voxelPosition, worldScale.xxx, transform);
			if(extResult.sum > 0.0)
			{
				float extinction = (extResult.sum / float(extResult.maxSampleCount)) * densityExtinctionScale;
				extinctionTexture[id] += extinction;
			}
		}
	}
}
