/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: raymarch froxels on the X/Y axis to propagate light from emissive froxels


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint materialTextureAIndex;
	uint materialTextureBIndex;
	float emissiveScatter; // how much of the light emitted sideways is reflected towards the camera
}

[numthreads(8, 8, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<float4> materialTextureA = ResourceDescriptorHeap[materialTextureAIndex];
	uint3 textureSize = GetTextureSize(materialTextureA);
#if DIRECTION_NX || DIRECTION_PX
	if(any(id.xy >= textureSize.yz))
#else
	if(any(id.xy >= textureSize.xz))
#endif
	{
		return;
	}

	SceneView scene = GetSceneView();
	RWTexture3D<float4> materialTextureB = ResourceDescriptorHeap[materialTextureBIndex];

#if DIRECTION_PX
	uint3 coords = uint3(0, id.x, id.y);
	uint3 index0 = coords;
	float3 tc0 = (float3(index0) + float3(0, 0.5, 0.5)) / textureSize;
	float3 prevPosition = scene.FroxelTCToWorldSpace(tc0, float3(textureSize));
	float3 accumEmit = float3(0, 0, 0);
	for(uint x = 1; x < textureSize.x; x++)
	{
		uint3 prevIndex = uint3(x - 1, coords.yz);
		uint3 currIndex = uint3(x, coords.yz);
		float3 tc = (float3(currIndex) + float3(0, 0.5, 0.5)) / textureSize;
		float3 currPosition = scene.FroxelTCToWorldSpace(tc, float3(textureSize));
		float4 scatterAbs = materialTextureA[prevIndex];
		float3 emissive = materialTextureB[prevIndex].rgb;
		float scatter = Brightness(scatterAbs.xyz);
		float extinction = scatter + scatterAbs.w;
		float dist = distance(currPosition, prevPosition);
		float froxelTrans = Transmittance(dist, extinction);
		accumEmit = (accumEmit + emissive) * froxelTrans;
		float3 extraEmit = accumEmit * emissiveScatter * scatter;
		if(any(extraEmit > 0.0))
		{
			materialTextureB[currIndex].rgb += extraEmit;
		}
		prevPosition = currPosition;
	}
#elif DIRECTION_NX
	uint3 coords = uint3(textureSize.x - 1, id.x, id.y);
	uint3 index0 = coords;
	float3 tc0 = (float3(index0) + float3(1, 0.5, 0.5)) / textureSize;
	float3 prevPosition = scene.FroxelTCToWorldSpace(tc0, float3(textureSize));
	float3 accumEmit = float3(0, 0, 0);
	for(int x = textureSize.x - 2; x >= 0; x--)
	{
		uint3 prevIndex = uint3(x + 1, coords.yz);
		uint3 currIndex = uint3(x, coords.yz);
		float3 tc = (float3(currIndex) + float3(1, 0.5, 0.5)) / textureSize;
		float3 currPosition = scene.FroxelTCToWorldSpace(tc, float3(textureSize));
		float4 scatterAbs = materialTextureA[prevIndex];
		float3 emissive = materialTextureB[prevIndex].rgb;
		float scatter = Brightness(scatterAbs.xyz);
		float extinction = scatter + scatterAbs.w;
		float dist = distance(currPosition, prevPosition);
		float froxelTrans = Transmittance(dist, extinction);
		accumEmit = (accumEmit + emissive) * froxelTrans;
		float3 extraEmit = accumEmit * emissiveScatter * scatter;
		if(any(extraEmit > 0.0))
		{
			materialTextureB[currIndex].rgb += extraEmit;
		}
		prevPosition = currPosition;
	}
#elif DIRECTION_PY
	uint3 index0 = uint3(id.x, 0, id.y);
	float3 tc0 = (float3(index0) + float3(0.5, 0, 0.5)) / textureSize;
	float3 prevPosition = scene.FroxelTCToWorldSpace(tc0, float3(textureSize));
	float3 accumEmit = float3(0, 0, 0);
	for(uint y = 1; y < textureSize.y; y++)
	{
		uint3 prevIndex = uint3(id.x, y - 1, id.y);
		uint3 currIndex = uint3(id.x, y + 0, id.y);
		float3 tc = (float3(currIndex) + float3(0.5, 0, 0.5)) / textureSize;
		float3 currPosition = scene.FroxelTCToWorldSpace(tc, float3(textureSize));
		float4 scatterAbs = materialTextureA[prevIndex];
		float3 emissive = materialTextureB[prevIndex].rgb;
		float scatter = Brightness(scatterAbs.xyz);
		float extinction = scatter + scatterAbs.w;
		float dist = distance(currPosition, prevPosition);
		float froxelTrans = Transmittance(dist, extinction);
		accumEmit = (accumEmit + emissive) * froxelTrans;
		float3 extraEmit = accumEmit * emissiveScatter * scatter;
		if(any(extraEmit > 0.0))
		{
			materialTextureB[currIndex].rgb += extraEmit;
		}
		prevPosition = currPosition;
	}
#elif DIRECTION_NY
	uint3 index0 = uint3(id.x, textureSize.y - 1, id.y);
	float3 tc0 = (float3(index0) + float3(0.5, 1, 0.5)) / textureSize;
	float3 prevPosition = scene.FroxelTCToWorldSpace(tc0, float3(textureSize));
	float3 accumEmit = float3(0, 0, 0);
	for(int y = textureSize.y - 2; y >= 0; y--)
	{
		uint3 prevIndex = uint3(id.x, y + 1, id.y);
		uint3 currIndex = uint3(id.x, y + 0, id.y);
		float3 tc = (float3(currIndex) + float3(0.5, 1, 0.5)) / textureSize;
		float3 currPosition = scene.FroxelTCToWorldSpace(tc, float3(textureSize));
		float4 scatterAbs = materialTextureA[prevIndex];
		float3 emissive = materialTextureB[prevIndex].rgb;
		float scatter = Brightness(scatterAbs.xyz);
		float extinction = scatter + scatterAbs.w;
		float dist = distance(currPosition, prevPosition);
		float froxelTrans = Transmittance(dist, extinction);
		accumEmit = (accumEmit + emissive) * froxelTrans;
		float3 extraEmit = accumEmit * emissiveScatter * scatter;
		if(any(extraEmit > 0.0))
		{
			materialTextureB[currIndex].rgb += extraEmit;
		}
		prevPosition = currPosition;
	}
#else
	float MissingDirectionMacro[-1];
#endif
}
