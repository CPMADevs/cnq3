/*
===========================================================================
Copyright (C) 2023-2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// gather depth of field: debug overlay


#include "common.hlsli"
#include "fullscreen.hlsli"
#include "gatherdof.hlsli"
#include "dof.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants : register(b0)
{
	uint colorTextureIndex;
	uint depthTextureIndex;
	uint debugMode;
	float focusNearMin;
	float focusNearMax;
	float focusFarMin;
	float focusFarMax;
	float focusDist;
};

float4 ps(VOut input) : SV_Target
{
	SceneView scene = GetSceneView();
	Texture2D colorTexture = ResourceDescriptorHeap[colorTextureIndex];
	Texture2D<float> depthTexture = ResourceDescriptorHeap[depthTextureIndex];
	uint3 tc = uint3(input.position.x, input.position.y, 0);
	float3 color = colorTexture.Load(tc).rgb;
	float depthZW = depthTexture.Load(tc);
	float depth = scene.LinearDepth(depthZW);
	float coc = CircleOfConfusion(depth, focusNearMin, focusNearMax, focusFarMin, focusFarMax);
	float nearField = coc < 0.0;
	float4 result;
	if(debugMode == 1)
	{
		result = DOF_DebugCoc(color, nearField, saturate(-coc), saturate(coc));
	}
	else if(debugMode == 2)
	{
		result = DOF_DebugFocusPlane(color, nearField);
	}
	else
	{
		result = float4(color, 1);
	}

	return result;
}
