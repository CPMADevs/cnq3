/*
===========================================================================
Copyright (C) 2023-2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// gather depth of field: max blur post-filter to combat undersampling


#include "common.hlsli"
#include "gatherdof.hlsli"


cbuffer RootConstants : register(b0)
{
	uint nearInputTextureIndex;
	uint nearOutputTextureIndex;
	uint farInputTextureIndex;
	uint farOutputTextureIndex;
	uint samplerIndex; // point/clamp
};

[numthreads(8, 8, 1)]
void cs(uint3 dtid : SV_DispatchThreadID)
{
	uint2 tc = dtid.xy;
	RWTexture2D<float4> nearOutputTexture = ResourceDescriptorHeap[nearOutputTextureIndex];
	RWTexture2D<float4> farOutputTexture = ResourceDescriptorHeap[farOutputTextureIndex];
	uint width, height, levels;
	nearOutputTexture.GetDimensions(width, height);
	if(any(dtid.xy >= uint2(width, height)))
	{
		return;
	}

	SamplerState samplerState = SamplerDescriptorHeap[samplerIndex];
	Texture2D nearInputTexture = ResourceDescriptorHeap[nearInputTextureIndex];
	Texture2D farInputTexture = ResourceDescriptorHeap[farInputTextureIndex];

	float2 tc01 = (float2(tc) + float2(0.5, 0.5)) / float2(width, height);
	float2 pixelSize = float2(1, 1) / float2(width, height);
	float4 nearFilled = float4(0, 0, 0, 0);
	float4 farFilled = float4(0, 0, 0, 0);
	for(int y = -1; y <= 1; y++)
	{
		for(int x = -1; x <= 1; x++)
		{
			float2 tcSample01 = tc01 + float2(x, y) * pixelSize;
			float4 nearSample = nearInputTexture.SampleLevel(samplerState, tcSample01, 0);
			float4 farSample = farInputTexture.SampleLevel(samplerState, tcSample01, 0);
			nearFilled = max(nearFilled, nearSample);
			farFilled = max(farFilled, farSample);
		}
	}

	// make sure to keep the original blend factors
	nearFilled.a = nearInputTexture.Load(uint3(tc.x, tc.y, 0)).a;
	farFilled.a = farInputTexture.Load(uint3(tc.x, tc.y, 0)).a;

	nearOutputTexture[tc] = nearFilled;
	farOutputTexture[tc] = farFilled;
}
