/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// decodes the per-axis differences between geometry and shading positions


#include "common.hlsli"
#include "fullscreen.hlsli"


cbuffer RootConstants
{
	uint textureIndex;
	uint coloredDelta;
};

float4 ps(VOut input) : SV_Target
{
	Texture2D positionTexture = ResourceDescriptorHeap[textureIndex];
	uint3 tc = uint3(input.position.xy, 0);
	float delta = positionTexture.Load(tc).w;
	float4 unpacked = UnpackColor(asuint(delta));
	float3 color = coloredDelta > 0 ? unpacked.rgb : unpacked.aaa;
	float4 result = float4(color, 1);

	return result;
}
