/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting particles: count the number of particles in each froxel tile


#include "common.hlsli"
#include "vl_common.h.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint3 fullResolution;
	uint tileBufferIndex;
	uint3 tileResolution;
	uint pad0;
	uint3 tileScale;
	uint pad1;
	uint particleBufferIndex;
	uint emitterBufferIndex;
	uint liveBufferIndex;
	uint emitterIndex;
}

[numthreads(64, 1, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWStructuredBuffer<ParticleEmitter> emitterBuffer = ResourceDescriptorHeap[emitterBufferIndex];
	ParticleEmitter emitter = emitterBuffer[emitterIndex];
	if(id.x >= emitter.liveCount2)
	{
		return;
	}

	RWStructuredBuffer<Particle> particleBuffer = ResourceDescriptorHeap[particleBufferIndex];
	RWStructuredBuffer<Tile> tileBuffer= ResourceDescriptorHeap[tileBufferIndex];
	RWStructuredBuffer<uint> liveBuffer = ResourceDescriptorHeap[liveBufferIndex];
	SceneView scene = GetSceneView();

	uint firstIndex = emitter.firstIndex;
	uint particleIndex = liveBuffer[firstIndex + id.x];
	Particle particle = particleBuffer[firstIndex + particleIndex];
	float3 P = particle.position;
	float r = particle.radius;
	float3 fwd = scene.cameraForward;
	float4 extentsXYNDC = ProjectedSphereExtentsNDC(particle.position, particle.radius, scene.viewMatrix, scene.projectionMatrix);
	float4 extentsXYFroxel = (extentsXYNDC * 0.5 + float(0.5).xxxx) * float4(fullResolution.xy, fullResolution.xy);
	int2 extentsZFroxel = scene.FroxelSphereZExtents(particle.position, particle.radius, fullResolution);
	int3 boxMin = int3(extentsXYFroxel.x, extentsXYFroxel.y, extentsZFroxel.x);
	int3 boxMax = int3(ceil(extentsXYFroxel.z), ceil(extentsXYFroxel.w), extentsZFroxel.y);
	particleBuffer[firstIndex + particleIndex].froxelMin = max(boxMin, int3(0, 0, 0));
	particleBuffer[firstIndex + particleIndex].froxelMax = min(boxMax, int3(fullResolution) - int3(1, 1, 1));
	boxMin /= int3(tileScale);
	boxMax /= int3(tileScale);
	boxMin = max(boxMin, int3(0, 0, 0));
	boxMax = min(boxMax, int3(tileResolution) - int3(1, 1, 1));

	for(int x = boxMin.x; x <= boxMax.x; x++)
	{
		for(int y = boxMin.y; y <= boxMax.y; y++)
		{
			for(int z = boxMin.z; z <= boxMax.z; z++)
			{
				uint3 tileIndex = uint3(x, y, z);
				uint flatTileIndex = FlattenIndex(tileIndex, tileResolution);
				InterlockedAdd(tileBuffer[flatTileIndex].particleCount, 1);
			}
		}
	}
}
