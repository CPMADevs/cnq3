/*
===========================================================================
Copyright (C) 2023 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// mip-map generation: linear-space to gamma-space transform


#include "../common/mip_gen.hlsli"


cbuffer RootConstants
{
	float4 blendColor;
	float intensity;
	float invGamma; // 1.0 / gamma
	uint srcMip;
	uint dstMip;
	uint srcTexture;
	uint dstTexture;
}

[numthreads(8, 8, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture2D<float4> src = ResourceDescriptorHeap[srcTexture];
	RWTexture2D<float4> dst = ResourceDescriptorHeap[dstTexture];
	MipGen_LinearToGamma(dst, src, id, blendColor, intensity, invGamma);
}
