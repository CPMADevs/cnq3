/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// wireframe normals for debugging


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	matrix modelViewMatrix;
};

struct VIn
{
	float3 position : POSITION;
	float4 color : COLOR0;
};

struct VOut
{
	float4 position : SV_Position;
	float4 color : COLOR0;
	float clipDist : SV_ClipDistance0;
};

VOut vs(VIn input)
{
	SceneView scene = GetSceneView();
	float4 positionVS = mul(modelViewMatrix, float4(input.position.xyz, 1));

	VOut output;
	output.position = mul(scene.projectionMatrix, positionVS);
	output.clipDist = dot(positionVS, scene.clipPlane);
	output.color = input.color;

	return output;
}

[earlydepthstencil]
float4 ps(VOut input) : SV_Target
{
	float4 result = float4(input.color.rgb, 1);

	return result;
}
