/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: depth test screen tiles


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint3 frustumTextureSize;
	uint frustumVisTextureIndex;
	uint depthMip;
}

[numthreads(8, 8, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	SceneView scene = GetSceneView();
	RWTexture2D<uint> frustumVisTexture = ResourceDescriptorHeap[frustumVisTextureIndex];
	uint2 visTextureSize = GetTextureSize(frustumVisTexture);
	uint2 tileIndex = id.xy;
	if(any(tileIndex >= visTextureSize.xy))
	{
		return;
	}

	Texture2D<float2> depthMinMaxTexture = ResourceDescriptorHeap[scene.depthMinMaxTextureIndex];
	float3 frustumTextureSizeF = float3(frustumTextureSize);
	// x=min is furthest with reverse Z
	float tileDepthZW = depthMinMaxTexture.mips[depthMip][id.xy].x;
	float tileDepth = scene.LinearDepth(tileDepthZW);
	uint furthestVisibleIndex = 0;
	for(uint d = 1; d < frustumTextureSize.z; d++)
	{
		// Z offset is 0 because we want the closest part of the froxel
		float3 froxelTC = (float3(tileIndex.xy, d) + float3(0.5, 0.5, 0)) / frustumTextureSizeF;
		float froxelDepth = scene.FroxelZ01ToViewDepth(froxelTC.z, frustumTextureSizeF.z);
		if(froxelDepth < tileDepth)
		{
			furthestVisibleIndex = d;
		}
		else
		{
			break;
		}
	}
	frustumVisTexture[tileIndex] = furthestVisibleIndex;
}
