/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// motion blur packing of scaled velocities and linear depth into a full-screen texture


#include "common.hlsli"
#include "fullscreen.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	float motionScale;
	float maxRadiusPx;
};

uint2 ps(VOut input) : SV_Target
{
	SceneView scene = GetSceneView();

	Texture2D<float> depthTexture = ResourceDescriptorHeap[scene.depthTextureIndex];
	float2 textureSize = float2(GetTextureSize(depthTexture));
	uint3 tc = uint3(input.texCoords * textureSize, 0);
	float depthZW = depthTexture.Load(tc);
	float linearDepth01 = scene.LinearDepth(depthZW) / scene.zFar;

	Texture2D<float2> motionTexture = ResourceDescriptorHeap[scene.motionVectorMBTextureIndex];
	float2 motionTC = motionTexture.Load(tc) * motionScale;
	float2 motionPx = motionTC * textureSize;
	float motionLengthPx = length(motionPx);
	if(motionLengthPx > maxRadiusPx)
	{
		motionTC *= maxRadiusPx / motionLengthPx;
	}
	uint motionPacked = PackHalf2(motionTC);
	uint2 result = uint2(motionPacked, asuint(linearDepth01));

	return result;
}
