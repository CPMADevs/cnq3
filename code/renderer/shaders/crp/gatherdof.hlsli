/*
===========================================================================
Copyright (C) 2023-2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// gather depth of field: support functions


#pragma once


#include "common.hlsli"


#define MAX_BLUR_DIAMETER 32.0
#define MAX_COC 16.0


float CircleOfConfusion(float depth, float focusNearMin, float focusNearMax, float focusFarMin, float focusFarMax)
{
	if(depth <= focusNearMin)
	{
		return -1.0;
	}

	if(depth > focusNearMin && depth < focusNearMax)
	{
		float t = 1.0 - (depth - focusNearMin) / (focusNearMax - focusNearMin);

		return -t;
	}

	if(depth > focusFarMin && depth < focusFarMax)
	{
		float t = (depth - focusFarMin) / (focusFarMax - focusFarMin);

		return t;
	}

	if(depth >= focusFarMax)
	{
		return 1.0;
	}

	return 0.0;
}
