/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// shared structure for the Quake 3 light grid


#pragma once


#include "typedefs.h.hlsli"
#if !defined(__cplusplus)
#	include "common.hlsli"
#endif

#if defined(__cplusplus)
#	pragma pack(push, 4)
#endif

struct LightGridRC
{
	float3 centerPosition;
	uint textureAIndex;
	float3 worldScale;
	uint textureBIndex;
	uint samplerIndex;
	uint isAvailable;
};

#if defined(__cplusplus)
#	pragma pack(pop)
#endif

#if defined(__cplusplus)
	static_assert(sizeof(LightGridRC) == 40, "sizeof(LightGridRC) is wrong");
#endif

#if !defined(__cplusplus)

struct LightGridSample
{
	float4 a;
	float4 b;

	float3 GetLightDirection()
	{
		return DirectionFromLongLat(b.z, b.w);
	}

	float3 GetLocalColor()
	{
		return float3(a.w, b.xy);
	}

	float3 GetGlobalColor()
	{
		return a.xyz;
	}

	float3 GetAmbientColor(float3 normal, float3 fallbackColor, float ambColorScale, float localColorScale)
	{
		float3 ambColor = GetGlobalColor();
		float3 localColor = GetLocalColor();
		float3 localDir = GetLightDirection();
		float localScale = dot(localDir, normal) * 0.5 + 0.5; // wraps around
		float3 interpColor = ambColor * ambColorScale + localColor * localScale * localColorScale;
		float brightNew = Brightness(interpColor);
		float brightFall = Brightness(fallbackColor);
		float t = saturate(brightNew / max(brightFall, 0.001));
		float3 color = lerp(fallbackColor, interpColor, t);

		return color;
	}
};

struct LightGrid
{
	float3 centerPosition;
	float3 worldScale;
	float3 textureSize;
	Texture3D textureA;
	Texture3D textureB;
	SamplerState sampler0;

	LightGridSample SampleAtPosition(float3 positionWS)
	{
		float3 ambientTC = AABoxWorldSpaceToTC(positionWS, centerPosition, textureSize, worldScale);
		LightGridSample sample;
		sample.a = textureA.SampleLevel(sampler0, ambientTC, 0);
		sample.b = textureB.SampleLevel(sampler0, ambientTC, 0);

		return sample;
	}

	LightGridSample SampleAtIndex(int3 index)
	{
		LightGridSample sample;
		sample.a = textureA[index];
		sample.b = textureB[index];

		return sample;
	}

	float3 IndexToWorldSpace(int3 voxelIndex)
	{
		float3 voxelCenter = AABoxIndexToWorldSpace(voxelIndex, centerPosition, textureSize, worldScale);

		return voxelCenter;
	}
};

LightGrid GetLightGrid(LightGridRC rc)
{
	LightGrid grid;
	grid.textureA = ResourceDescriptorHeap[rc.textureAIndex];
	grid.textureB = ResourceDescriptorHeap[rc.textureBIndex];
	grid.sampler0 = SamplerDescriptorHeap[rc.samplerIndex];
	grid.centerPosition = rc.centerPosition;
	grid.worldScale = rc.worldScale;
	grid.textureSize = float3(GetTextureSize(grid.textureA));

	return grid;
}

#endif
