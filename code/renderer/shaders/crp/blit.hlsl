/*
===========================================================================
Copyright (C) 2023-2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// blit shader - unlike texture copies, it doesn't care about the specific formats used


#include "fullscreen.hlsli"


cbuffer RootConstants : register(b0)
{
	uint textureIndex;
	uint samplerIndex;
	float2 tcScale;
	float2 tcBias;
};

float4 ps(VOut input) : SV_Target
{
	Texture2D texture0 = ResourceDescriptorHeap[textureIndex];
	SamplerState sampler0 = SamplerDescriptorHeap[samplerIndex];
	float2 tc = input.texCoords * tcScale + tcBias;
	float3 base = texture0.Sample(sampler0, tc).rgb;
	float4 result = float4(base, 1.0);

	return result;
}
