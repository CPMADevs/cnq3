/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// depth of field: debug overlay support functions


#pragma once


float4 DOF_DebugCoc(float3 color, bool nearField, float nearCoc, float farCoc)
{
	float blendFactor;
	float3 target;
	if(nearField)
	{
		blendFactor = 0.5 * nearCoc;
		target = float3(0, 1, 0);
	}
	else
	{
		blendFactor = 0.5 * farCoc;
		target = float3(0, 0, 1);
	}
	float4 result = float4(lerp(color, target, blendFactor), 1);

	return result;
}

float4 DOF_DebugFocusPlane(float3 color, bool nearField)
{
	float farFieldFactor = nearField ? 0.0 : 0.25;
	float3 farFieldColor = float3(0.5, 0, 0.5);
	float3 mixed = lerp(color, farFieldColor, farFieldFactor);
	float4 result = float4(mixed, 1);

	return result;
}
