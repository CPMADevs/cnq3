/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// volumetric lighting: accumulates in-scattered ambient light


#include "common.hlsli"
#include "scene_view.h.hlsli"
#include "light_grid.h.hlsli"


cbuffer RootConstants
{
	LightGridRC lightGridRC;
	uint materialTextureAIndex;
	uint scatterExtTextureIndex;
}

[numthreads(4, 4, 4)]
void cs(uint3 id : SV_DispatchThreadID)
{
	RWTexture3D<float4> scatterExtTexture = ResourceDescriptorHeap[scatterExtTextureIndex];
	uint3 textureSize = GetTextureSize(scatterExtTexture);
	if(any(id >= textureSize))
	{
		return;
	}

	SceneView scene = GetSceneView();
	RWTexture3D<float4> materialTextureA = ResourceDescriptorHeap[materialTextureAIndex];
	ExtinctionCascade cascade = scene.GetExtinctionCascade(scene.extinctionWorldScale.y);
	float3 positionWS = scene.FroxelIndexToWorldSpace(id, textureSize);
	float3 normalWS = normalize(scene.cameraPosition - positionWS);
	float4 scatterAbs = materialTextureA[id];
	float3 scattering = scatterAbs.rgb;
	float extinction = Brightness(scattering) + scatterAbs.a;

	if(lightGridRC.isAvailable != 0)
	{
		LightGrid lightGrid = GetLightGrid(lightGridRC);
		LightGridSample ambient = lightGrid.SampleAtPosition(positionWS);
		float3 ambientColor = ambient.GetAmbientColor(normalWS, scene.ambientColor, 1, 1);
		float3 inScattering = scattering * ambientColor * scene.ambientIntensity;

		scatterExtTexture[id] += float4(inScattering, extinction);
	}
	else
	{
		float3 inScattering = scattering * scene.ambientColor * scene.ambientIntensity;

		scatterExtTexture[id] += float4(inScattering, extinction);
	}
}
