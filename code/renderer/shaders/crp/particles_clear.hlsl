/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// GPU particle system: emitter and particle free list initialization


#include "common.hlsli"
#include "scene_view.h.hlsli"


cbuffer RootConstants
{
	uint emitterBufferIndex;
	uint deadBufferIndex;
	uint emitterIndex;
	uint firstParticle;
	uint particleCount;
	float maxSeconds;
}

[numthreads(64, 1, 1)]
void cs(uint3 id : SV_DispatchThreadID)
{
	if(id.x >= particleCount)
	{
		return;
	}

	if(id.x == 0)
	{
		RWStructuredBuffer<ParticleEmitter> emitterBuffer = ResourceDescriptorHeap[emitterBufferIndex];
		ParticleEmitter e;
		e.deadCount = particleCount;
		e.firstIndex = firstParticle;
		e.liveCount = 0;
		e.liveCount2 = 0;
		e.totalCount = particleCount;
		e.emitCount = 0;
		e.maxSeconds = maxSeconds;
		emitterBuffer[emitterIndex] = e;
	}

	RWStructuredBuffer<uint> deadBuffer = ResourceDescriptorHeap[deadBufferIndex];
	deadBuffer[firstParticle + id.x] = particleCount - 1 - id.x;
}
