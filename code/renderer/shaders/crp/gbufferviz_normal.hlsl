/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// decodes octahedron-encoded normals for visualization


#include "common.hlsli"
#include "fullscreen.hlsli"


cbuffer RootConstants
{
	uint normalTextureIndex;
};

float4 ps(VOut input) : SV_Target
{
	Texture2D<float2> normalTexture = ResourceDescriptorHeap[normalTextureIndex];
	uint3 tc = uint3(input.position.xy, 0);
	float3 normalWS = OctDecode(normalTexture.Load(tc));
	float3 normal = normalize(normalWS) * 0.5 + 0.5;
	float4 result = float4(normal, 1);

	return result;
}
