/*
===========================================================================
Copyright (C) 2024 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// shader stage state constants


#if !defined(DISABLE_PRAGMA_ONCE)
#pragma once
#endif


#define GLS_SRCBLEND_ZERO						0x00000001u
#define GLS_SRCBLEND_ONE						0x00000002u
#define GLS_SRCBLEND_DST_COLOR					0x00000003u
#define GLS_SRCBLEND_ONE_MINUS_DST_COLOR		0x00000004u
#define GLS_SRCBLEND_SRC_ALPHA					0x00000005u
#define GLS_SRCBLEND_ONE_MINUS_SRC_ALPHA		0x00000006u
#define GLS_SRCBLEND_DST_ALPHA					0x00000007u
#define GLS_SRCBLEND_ONE_MINUS_DST_ALPHA		0x00000008u
#define GLS_SRCBLEND_ALPHA_SATURATE				0x00000009u
#define		GLS_SRCBLEND_BITS					0x0000000fu

#define GLS_DSTBLEND_ZERO						0x00000010u
#define GLS_DSTBLEND_ONE						0x00000020u
#define GLS_DSTBLEND_SRC_COLOR					0x00000030u
#define GLS_DSTBLEND_ONE_MINUS_SRC_COLOR		0x00000040u
#define GLS_DSTBLEND_SRC_ALPHA					0x00000050u
#define GLS_DSTBLEND_ONE_MINUS_SRC_ALPHA		0x00000060u
#define GLS_DSTBLEND_DST_ALPHA					0x00000070u
#define GLS_DSTBLEND_ONE_MINUS_DST_ALPHA		0x00000080u
#define		GLS_DSTBLEND_BITS					0x000000f0u

#define		GLS_BLEND_BITS						0x000000ffu
#define		GLS_BLEND_ADDITIVE					(GLS_SRCBLEND_ONE | GLS_DSTBLEND_ONE)
#define		GLS_BLEND_STD_ALPHA					(GLS_SRCBLEND_SRC_ALPHA | GLS_DSTBLEND_ONE_MINUS_SRC_ALPHA)
#define		GLS_BLEND_PMUL_ALPHA				(GLS_SRCBLEND_ONE | GLS_DSTBLEND_ONE_MINUS_SRC_ALPHA)
#define		GLS_BLEND_FILTER					(GLS_SRCBLEND_DST_COLOR | GLS_DSTBLEND_ZERO)
#define		GLS_BLEND_FILTER_V2					(GLS_SRCBLEND_ZERO | GLS_DSTBLEND_SRC_COLOR)

#define GLS_DEPTHMASK_TRUE						0x00000100u	// enable depth writes

#define GLS_POLYMODE_LINE						0x00001000u	// wireframe polygon filling, not line rendering

#define GLS_DEPTHTEST_DISABLE					0x00010000u	// disable depth tests
#define GLS_DEPTHFUNC_EQUAL						0x00020000u

#define GLS_STAGEINDEX_BITS						0x00700000u
#define GLS_STAGEINDEX_SHIFT					20u

#define GLS_ATEST_GT_0							0x10000000u
#define GLS_ATEST_LT_80							0x20000000u
#define GLS_ATEST_GE_80							0x40000000u
#define		GLS_ATEST_BITS						0x70000000u
