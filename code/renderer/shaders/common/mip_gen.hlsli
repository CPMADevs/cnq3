/*
===========================================================================
Copyright (C) 2023 Gian 'myT' Schellenbaum

This file is part of Challenge Quake 3 (CNQ3).

Challenge Quake 3 is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

Challenge Quake 3 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Challenge Quake 3. If not, see <https://www.gnu.org/licenses/>.
===========================================================================
*/
// mip-map generation


#pragma once


uint2 MipGen_FixCoords(int2 tc, int2 maxSize, uint clampCoords)
{
	if(clampCoords > 0)
	{
		// clamp
		return uint2(clamp(tc, int2(0, 0), maxSize));
	}

	// repeat
	return uint2(tc & maxSize);
}

void MipGen_GammaToLinear(RWTexture2D<float4> dst, RWTexture2D<float4> src, uint3 dtid, float gamma)
{
	uint w, h;
	dst.GetDimensions(w, h);
	if(any(dtid.xy >= uint2(w, h)))
	{
		return;
	}

	float4 v = src[dtid.xy];
	dst[dtid.xy] = float4(pow(v.xyz, gamma), v.a);
}

void MipGen_LinearToGamma(RWTexture2D<float4> dst, RWTexture2D<float4> src, uint3 dtid, float4 blendColor, float intensity, float invGamma)
{
	uint w, h;
	dst.GetDimensions(w, h);
	if(any(dtid.xy >= uint2(w, h)))
	{
		return;
	}

	// yes, intensity *should* be done in light-linear space
	// but we keep the old behavior for consistency...
	float4 in0 = src[dtid.xy];
	float3 in1 = 0.5 * (in0.rgb + blendColor.rgb);
	float3 inV = lerp(in0.rgb, in1.rgb, blendColor.a);
	float3 out0 = pow(max(inV, 0.0), invGamma);
	float3 out1 = out0 * intensity;
	float4 outV = saturate(float4(out1, in0.a));
	dst[dtid.xy] = outV;
}

void MipGen_DownSample(RWTexture2D<float4> dst, RWTexture2D<float4> src, uint3 dtid, int2 maxSize, uint clampCoords, int2 scale, int2 offset, float4 weights)
{
	uint w, h;
	dst.GetDimensions(w, h);
	if(any(dtid.xy >= uint2(w, h)))
	{
		return;
	}

#define FixCoords(tc) MipGen_FixCoords(tc, maxSize, clampCoords)

	int2 base = int2(dtid.xy) * scale;
	float4 r = float4(0, 0, 0, 0);
	r += src[FixCoords(base - offset * 3)] * weights.x;
	r += src[FixCoords(base - offset * 2)] * weights.y;
	r += src[FixCoords(base - offset * 1)] * weights.z;
	r += src[base] * weights.w;
	r += src[base + offset] * weights.w;
	r += src[FixCoords(base + offset * 2)] * weights.z;
	r += src[FixCoords(base + offset * 3)] * weights.y;
	r += src[FixCoords(base + offset * 4)] * weights.x;
	dst[dtid.xy] = r;

#undef FixCoords
}
